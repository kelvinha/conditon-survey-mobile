import 'package:condition_survey/keys.dart';
import 'package:condition_survey/pages/dashboard/dashboard_page.dart';
import 'package:condition_survey/pages/auth/login_page.dart';
import 'package:condition_survey/pages/splash_screen_page.dart';
import 'package:condition_survey/providers/auth_provider.dart';
import 'package:condition_survey/providers/dashboard/detail_object_provider.dart';
import 'package:condition_survey/providers/dashboard/dashboard_survey_provider.dart';
import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/providers/list/survey_provider.dart';
import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:condition_survey/providers/media_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // Dashboard provider
        ChangeNotifierProvider(create: (context) => DashboardSurveyProvider()),
        ChangeNotifierProvider(create: (context) => DetailObjectProvider()),
        ChangeNotifierProvider(create: (context) => DashboardProvider()),
        // List provider
        ChangeNotifierProvider(create: (context) => SurveyProvider()),
        // Auth Provider
        ChangeNotifierProvider(create: (context) => AuthProvider()),
        // Media Provider
        ChangeNotifierProvider(create: (context) => MediaProvider()),
        // Inspection Certificate
        ChangeNotifierProvider(
            create: (context) => InspectionCertificateProvider()),
        // Connection
        ChangeNotifierProvider(
            create: (context) => ConnectivityStatusProvider()),
      ],
      child: MaterialApp(
        localizationsDelegates: const [
          DefaultMaterialLocalizations.delegate,
          DefaultCupertinoLocalizations.delegate,
          DefaultWidgetsLocalizations.delegate,
        ],
        debugShowCheckedModeBanner: false,
        title: 'Condition Survey',
        home: const SplashScreenPage(),
        navigatorKey: navigatorKey,
        routes: {
          "/login": (context) => const LoginPage(),
          "/dashboard": (context) => const DashboardPage(),
        },
      ),
    );
  }
}
