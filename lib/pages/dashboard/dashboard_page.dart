import 'package:condition_survey/models/dashboard/survey_model.dart';
import 'package:condition_survey/pages/dashboard/detail_survey_page.dart';
import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/not_found_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../providers/dashboard_provider.dart';
import '../../widgets/loading_indicator_widget.dart';
import '../../widgets/navbar_widget.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({super.key});

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  TextEditingController searchSurveyController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    String? messageAlert;
    bool isSuccess = false;
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);
    // ignore: unused_local_variable
    final ConnectivityStatusProvider connStatusProvider =
        Provider.of<ConnectivityStatusProvider>(context, listen: true);

    Future<bool> handleUpdateStatusSurvey(
        Map<String, dynamic> requestBody, int idTitle) async {
      isSuccess = await dashboardProvider.updateStatusSurvey(
        bodyRequest: requestBody,
        titleId: idTitle.toString(),
      );

      if (isSuccess) {
        messageAlert = 'Selamat mengerjakan!';
      } else {
        messageAlert = 'Oops, terjadi kesalahan';
      }

      // ignore: avoid_print
      print(requestBody);

      return isSuccess;
    }

    void showAlertDialog(
        BuildContext context, Map<String, dynamic> requestBody, int idTitle) {
      showCupertinoModalPopup<bool>(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(
            'Alert',
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          content: Text(
            'Apakah kamu yakin ?',
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          actions: <CupertinoDialogAction>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'No',
                style: regularPoppins.copyWith(
                  color: redColor,
                ),
              ),
            ),
            CupertinoDialogAction(
              isDestructiveAction: true,
              onPressed: () async {
                bool isSuccess =
                    await handleUpdateStatusSurvey(requestBody, idTitle);
                // ignore: use_build_context_synchronously
                SnackbarCustom.alertMessage(
                  context,
                  messageAlert!,
                  2,
                  isSuccess,
                );

                setState(() {});
                // ignore: use_build_context_synchronously
                Navigator.pop(context);
              },
              child: Text(
                'Yes',
                style: regularPoppins.copyWith(
                  color: Colors.blue,
                ),
              ),
            ),
          ],
        ),
      );
    }

    void showAlertDialogDownload(BuildContext context, int idTitle) {
      showCupertinoModalPopup<bool>(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(
            'Alert',
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          content: Text(
            'Apakah kamu yakin ingin mengunduh survey ini?',
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          actions: <CupertinoDialogAction>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'No',
                style: regularPoppins.copyWith(
                  color: redColor,
                ),
              ),
            ),
            CupertinoDialogAction(
              isDestructiveAction: true,
              onPressed: () async {
                if (dashboardProvider.survey
                    .any((element) => element.id == idTitle)) {
                  var matchingElement = dashboardProvider.survey.firstWhere(
                    (element) => element.id == idTitle,
                    orElse: () => ListSurvey(
                      id: 0,
                      inspectorId: 0,
                      title: "",
                      location: "",
                      date: "",
                      inspectorName: "",
                      status: 0,
                      createdAt: "",
                      updatedAt: "",
                    ),
                  );
                  // ignore: use_build_context_synchronously
                  Navigator.pop(context);
                  showDialog(
                    context: context,
                    builder: (context) {
                      return ContentDownload(widget: matchingElement);
                    },
                  );
                }
              },
              child: Text(
                'Yes',
                style: regularPoppins.copyWith(
                  color: Colors.blue,
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: backgroundColor1,
      body: RefreshIndicator(
        edgeOffset: 230,
        backgroundColor: secondaryColor,
        color: whiteColor,
        onRefresh: () async {
          await dashboardProvider.getSummary();
          setState(() {});
        },
        child: Column(
          children: [
            // ignore: prefer_const_constructors
            Navbar(),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 10,
                      horizontal: 20,
                    ),
                    child: searchSurvey(),
                  ),
                  Expanded(
                    child: FutureBuilder(
                      future: dashboardProvider.getSurvey(
                        title: searchSurveyController.text,
                      ),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const LoadingIndicator();
                        } else if (dashboardProvider.survey.isEmpty) {
                          return const NotFound();
                        } else {
                          return ListView.builder(
                            padding: const EdgeInsets.only(
                              top: 10,
                              left: 20,
                              right: 20,
                            ),
                            itemCount: dashboardProvider.survey.length,
                            itemBuilder: (context, index) {
                              var data = dashboardProvider.survey[index];
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    CupertinoPageRoute(
                                      builder: (context) => DetailSurveyPage(
                                        surveyTitle: data.title,
                                        surveyTitleId: data.id.toString(),
                                      ),
                                    ),
                                  );
                                },
                                child: Container(
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  margin: const EdgeInsets.only(
                                    bottom: 15,
                                  ),
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 20,
                                    vertical: 10,
                                  ),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: greyColor.withOpacity(0.5),
                                        offset: const Offset(
                                          5,
                                          8,
                                        ),
                                        blurRadius: 2.0,
                                      )
                                    ],
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(8),
                                    ),
                                    color: whiteColor,
                                  ),
                                  child: Row(
                                    children: [
                                      const ShipLogo(),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              data.title.toUpperCase(),
                                              style: mediumPoppins.copyWith(
                                                fontSize: 18,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.75,
                                              child: Text(
                                                data.location,
                                                style: lightPoppins.copyWith(
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                            Text(
                                              data.date!,
                                              style: lightPoppins.copyWith(
                                                fontSize: 12,
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  height: 30,
                                                  width: data.status == 0
                                                      ? MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                          5.0
                                                      : connStatusProvider
                                                              .hasConnected
                                                          ? MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width /
                                                              2.1
                                                          : MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width /
                                                              1.75,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        const BorderRadius.all(
                                                      Radius.circular(8),
                                                    ),
                                                    color: _statusColor(
                                                        data.status),
                                                  ),
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      _status(data.status),
                                                      style: data.status == 1
                                                          ? mediumPoppins
                                                              .copyWith(
                                                              fontSize: 14,
                                                              color: whiteColor,
                                                            )
                                                          : mediumPoppins
                                                              .copyWith(
                                                                  fontSize: 14),
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: 10,
                                                ),
                                                Visibility(
                                                  visible: data.status == 0,
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      Map<String, dynamic>
                                                          bodyRequest = {
                                                        'title': data.title,
                                                        'location':
                                                            data.location,
                                                        'date': data.date,
                                                        'inspector_name':
                                                            data.inspectorName,
                                                      };

                                                      showAlertDialog(context,
                                                          bodyRequest, data.id);
                                                    },
                                                    child: Container(
                                                      height: 30,
                                                      width: connStatusProvider
                                                              .hasConnected
                                                          ? MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width /
                                                              3.5
                                                          : MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width /
                                                              2.6,
                                                      decoration: BoxDecoration(
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.5),
                                                            blurRadius: 6,
                                                            offset:
                                                                const Offset(
                                                              2,
                                                              4,
                                                            ),
                                                          )
                                                        ],
                                                        borderRadius:
                                                            const BorderRadius
                                                                .all(
                                                          Radius.circular(8),
                                                        ),
                                                        color: primaryColor,
                                                      ),
                                                      child: Align(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Text(
                                                          "Start Survey",
                                                          style: mediumPoppins
                                                              .copyWith(
                                                            fontSize: 14,
                                                            color: whiteColor,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Visibility(
                                                  visible: connStatusProvider
                                                      .hasConnected,
                                                  child: Row(
                                                    children: [
                                                      const SizedBox(
                                                        width: 10,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          showAlertDialogDownload(
                                                              context, data.id);
                                                        },
                                                        child: Container(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(6.0),
                                                          decoration:
                                                              BoxDecoration(
                                                            boxShadow: [
                                                              BoxShadow(
                                                                color: Colors
                                                                    .black
                                                                    .withOpacity(
                                                                        0.2),
                                                                blurRadius: 6,
                                                                offset:
                                                                    const Offset(
                                                                  2,
                                                                  4,
                                                                ),
                                                              )
                                                            ],
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              Radius.circular(
                                                                  6),
                                                            ),
                                                            color:
                                                                secondaryColor,
                                                          ),
                                                          child: const Icon(
                                                            Icons.download,
                                                            color: whiteColor,
                                                            size: 18,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                              // ListSurveyWidget(
                              //   titleId: dashboardProvider.survey[index].id,
                              //   titleShip:
                              //       dashboardProvider.survey[index].title,
                              //   location:
                              //       dashboardProvider.survey[index].location,
                              //   dateRange: dashboardProvider.survey[index].date,
                              //   status: dashboardProvider.survey[index].status,
                              //   inspectorName: dashboardProvider
                              //       .survey[index].inspectorName,
                              // );
                            },
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget searchSurvey() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: greyColor.withOpacity(0.5),
            offset: const Offset(
              5,
              8,
            ),
            blurRadius: 2.0,
          )
        ],
        color: whiteColor,
        borderRadius: const BorderRadius.all(
          Radius.circular(
            10,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          left: 14,
          right: 14,
        ),
        child: Align(
          child: TextField(
            controller: searchSurveyController,
            onSubmitted: (value) {
              setState(() {
                searchSurveyController.text = value;
              });
            },
            autocorrect: false,
            style: regularPoppins,
            cursorColor: primaryColor,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Search survey",
              hintStyle: regularPoppins.copyWith(
                fontSize: 12,
                color: Colors.black.withOpacity(0.5),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ContentDownload extends StatefulWidget {
  final ListSurvey widget;
  // final ListSurveyWidget widget;
  const ContentDownload({
    super.key,
    required this.widget,
  });

  @override
  State<ContentDownload> createState() => _ContentDownloadState();
}

class _ContentDownloadState extends State<ContentDownload> {
  bool isSuccessDetailMenu = false;
  bool isSuccessListMenu = false;

  @override
  void initState() {
    super.initState();
    // downloadDetailMenu();
    downloadListMenu();
  }

  Future<void> downloadDetailMenu() async {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    await inspectionCertificateProvider.getSurveyMenu(isDownload: true);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? checkMenu = prefs.getString('survey_menu');

    if (checkMenu!.isNotEmpty) {
      setState(() {
        isSuccessDetailMenu = true;
      });
    } else {
      setState(() {
        isSuccessDetailMenu = false;
      });
    }
  }

  Future<void> downloadListMenu() async {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);
    await dashboardProvider.getSurvey(
      isDownload: true,
      listSurveyDownload: widget.widget,
    );

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? checkMenu = prefs.getString('list_menu');

    if (checkMenu!.isNotEmpty) {
      setState(() {
        isSuccessListMenu = true;
      });
    } else {
      setState(() {
        isSuccessListMenu = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Download | ${widget.widget.title}",
            style: regularPoppins,
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "List Menu",
                style: regularPoppins,
              ),
              isSuccessDetailMenu
                  ? const Icon(
                      Icons.verified,
                      color: greenColor,
                    )
                  : const Icon(
                      Icons.verified,
                      color: greyColor,
                    ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Detail Menu",
                style: regularPoppins,
              ),
              isSuccessListMenu
                  ? const Icon(
                      Icons.verified,
                      color: greenColor,
                    )
                  : const Icon(
                      Icons.verified,
                      color: greyColor,
                    ),
            ],
          ),
        ],
      ),
    );
  }
}

class ShipLogo extends StatelessWidget {
  const ShipLogo({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Row(
        children: [
          Image.asset(
            "${pathImages}ship.png",
            width: 60,
          ),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }
}

String _status(int status) {
  switch (status) {
    case 0:
      return todo;
    case 2:
      return inProgress;
    case 1:
      return done;
    default:
      return 'Unknown';
  }
}

Color _statusColor(int status) {
  switch (status) {
    case 0:
      return backgroundColor1;
    case 2:
      return thirdColor;
    case 1:
      return secondaryColor;
    default:
      return backgroundColor1;
  }
}
