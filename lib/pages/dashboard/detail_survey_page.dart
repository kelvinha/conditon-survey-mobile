// ignore_for_file: unused_import
import 'package:condition_survey/models/dashboard/survey_menu_model.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/ship_legalities/detail_ship_legalities_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/inspection_certificate_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_physicals/inspection_physicals_page.dart';
import 'package:condition_survey/pages/dashboard/object/detail_object_page.dart';
import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:condition_survey/widgets/not_found_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailSurveyPage extends StatefulWidget {
  final String surveyTitle;
  final String surveyTitleId;

  const DetailSurveyPage({
    required this.surveyTitle,
    required this.surveyTitleId,
    super.key,
  });

  @override
  State<DetailSurveyPage> createState() => _DetailSurveyPageState();
}

class _DetailSurveyPageState extends State<DetailSurveyPage> {
  late DashboardProvider dashboardProvider;
  late InspectionCertificateProvider inspectionCertificateProvider;
  @override
  void initState() {
    getDetailSurvey();
    super.initState();
  }

  void getDetailSurvey() async {
    inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    inspectionCertificateProvider.getDetailSurveyByID(id: widget.surveyTitleId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        foregroundColor: primaryColor,
        backgroundColor: whiteColor,
        title: Text(
          widget.surveyTitle,
          style: regularPoppins,
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FutureBuilder(
              future: inspectionCertificateProvider.getSurveyMenu(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const LoadingIndicator();
                } else if (inspectionCertificateProvider.surveyMenu.isEmpty) {
                  return const NotFound();
                } else {
                  return Expanded(
                    child: ListView.builder(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      shrinkWrap: true,
                      itemCount:
                          inspectionCertificateProvider.surveyMenu.length,
                      itemBuilder: (context, index) {
                        SurveyMenu surveyMenu =
                            inspectionCertificateProvider.surveyMenu[index];
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 70,
                            vertical: 20,
                          ),
                          child: Menus(
                            titleMenu: surveyMenu.title,
                            formCode: surveyMenu.formCode!,
                            titleId: widget.surveyTitleId,
                            titleSurvey: widget.surveyTitle,
                          ),
                        );
                      },
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

class Menus extends StatelessWidget {
  final String titleId;
  final String titleSurvey;
  final String titleMenu;
  final String formCode;

  const Menus({
    required this.titleSurvey,
    required this.titleId,
    required this.titleMenu,
    required this.formCode,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          CupertinoPageRoute(
            builder: ((context) => formCode == inspectionObjects
                ? DetailObjectPage(
                    titleMenu: titleMenu,
                    titleApi: formCode,
                    titleId: titleId,
                    titleSurvey: titleSurvey,
                  )
                : formCode == inspectionCertificate
                    ? DetailInspectionCertificate(
                        titleMenu: titleMenu,
                        titleId: titleId,
                        titleAPI: formCode,
                      )
                    : DetailInspectionPhysicals(
                        titleMenu: titleMenu,
                        titleId: titleId,
                        titleAPI: formCode,
                      )),
          ),
        );
      },
      child: Container(
        width: 250,
        // height: 250,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              offset: const Offset(4, 8),
              blurRadius: 2,
            ),
          ],
          color: whiteColor,
          borderRadius: const BorderRadius.all(
            Radius.circular(10),
          ),
          border: Border.all(
            color: primaryColor,
            width: 2,
          ),
        ),
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 15.0,
                  vertical: 35.0,
                ),
                child: Column(
                  children: [
                    Icon(
                      formCode == inspectionObjects
                          ? Icons.description_outlined
                          : formCode == inspectionCertificate
                              ? Icons.fact_check_outlined
                              : Icons.anchor,
                      color: primaryColor,
                      size: 120,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      titleMenu,
                      style: semiboldPoppins,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
