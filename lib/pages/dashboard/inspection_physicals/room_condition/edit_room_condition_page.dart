import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/button_custom_widget.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../widgets/media_attachment_image_widget.dart';
import '../../../../widgets/media_attachment_pdf_widget.dart';
import '../../../../widgets/media_attachment_voice_note_widget.dart';

class EditRoomConditionPage extends StatefulWidget {
  final String titleId;
  final String titleShip;
  final String titleMenu;
  final String formCode;

  const EditRoomConditionPage({
    super.key,
    required this.titleId,
    required this.titleShip,
    required this.titleMenu,
    required this.formCode,
  });

  @override
  State<EditRoomConditionPage> createState() => _EditRoomConditionPageState();
}

class _EditRoomConditionPageState extends State<EditRoomConditionPage> {
  List<List<TextEditingController>> findingControllers = [];
  List<List<TextEditingController>> remarkControllers = [];
  List<List<TextEditingController>> gradeLControllers = [];
  List<List<TextEditingController>> gradeCControllers = [];
  List<List<TextEditingController>> gradeFLControllers = [];
  List<List<TextEditingController>> gradeFUControllers = [];
  List<List<String>> gradingL = [];
  List<List<String>> gradingC = [];
  List<List<String>> gradingFL = [];
  List<List<String>> gradingFU = [];
  Map<int, List<String>> listFilesPdf = {};
  Map<int, List<String>> listFilesImage = {};
  Map<int, List<String>> listFilesAudio = {};

  bool isLoading = false;
  String messageAlert = '';

  @override
  void initState() {
    super.initState();
    final inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    for (var i = 0;
        i < inspectionCertificateProvider.roomCondition.length;
        i++) {
      // finding
      var findingControllersRow = List.generate(
        inspectionCertificateProvider.roomCondition[i].subItems.length,
        (index) {
          var subItem =
              inspectionCertificateProvider.roomCondition[i].subItems[index];
          return TextEditingController(
            text: subItem.finding,
          );
        },
      );
      findingControllers.add(findingControllersRow);
      // remark
      var remarkControllersRow = List.generate(
        inspectionCertificateProvider.roomCondition[i].subItems.length,
        (index) {
          var subItem =
              inspectionCertificateProvider.roomCondition[i].subItems[index];
          return TextEditingController(
            text: subItem.remark,
          );
        },
      );
      remarkControllers.add(remarkControllersRow);

      // grade
      var gradeLControllersRow = List.generate(
        inspectionCertificateProvider.roomCondition[i].subItems.length,
        (index) {
          var subItem =
              inspectionCertificateProvider.roomCondition[i].subItems[index];
          return TextEditingController(
            text: subItem.gradeL.toString(),
          );
        },
      );
      gradeLControllers.add(gradeLControllersRow);
      gradingL = List.generate(
        inspectionCertificateProvider.roomCondition.length,
        (index) {
          var subItems =
              inspectionCertificateProvider.roomCondition[index].subItems;
          return List.generate(subItems.length, (subIndex) => '');
        },
      );

      var gradeCControllersRow = List.generate(
        inspectionCertificateProvider.roomCondition[i].subItems.length,
        (index) {
          var subItem =
              inspectionCertificateProvider.roomCondition[i].subItems[index];
          return TextEditingController(
            text: subItem.gradeC.toString(),
          );
        },
      );
      gradeCControllers.add(gradeCControllersRow);
      gradingC = List.generate(
        inspectionCertificateProvider.roomCondition.length,
        (index) {
          var subItems =
              inspectionCertificateProvider.roomCondition[index].subItems;
          return List.generate(subItems.length, (subIndex) => '');
        },
      );

      var gradeFLControllersRow = List.generate(
        inspectionCertificateProvider.roomCondition[i].subItems.length,
        (index) {
          var subItem =
              inspectionCertificateProvider.roomCondition[i].subItems[index];
          return TextEditingController(
            text: subItem.gradeFl.toString(),
          );
        },
      );
      gradeFLControllers.add(gradeFLControllersRow);
      gradingFL = List.generate(
        inspectionCertificateProvider.roomCondition.length,
        (index) {
          var subItems =
              inspectionCertificateProvider.roomCondition[index].subItems;
          return List.generate(subItems.length, (subIndex) => '');
        },
      );

      var gradeFUControllersRow = List.generate(
        inspectionCertificateProvider.roomCondition[i].subItems.length,
        (index) {
          var subItem =
              inspectionCertificateProvider.roomCondition[i].subItems[index];
          return TextEditingController(
            text: subItem.gradeFu.toString(),
          );
        },
      );
      gradeFUControllers.add(gradeFUControllersRow);
      gradingFU = List.generate(
        inspectionCertificateProvider.roomCondition.length,
        (index) {
          var subItems =
              inspectionCertificateProvider.roomCondition[index].subItems;
          return List.generate(subItems.length, (subIndex) => '');
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    // ignore: unused_element
    String mergeFinding(int index, int indexSubItem) {
      var findingText =
          '- ${gradingL[index][indexSubItem]}\n- ${gradingC[index][indexSubItem]}\n- ${gradingFL[index][indexSubItem]}\n- ${gradingFU[index][indexSubItem]}';
      return findingText;
    }

    Future<bool> handleUpdateRoomCondition() async {
      List<Map<String, dynamic>> listItems = [];
      for (var i = 0;
          i < inspectionCertificateProvider.roomCondition.length;
          i++) {
        var item = inspectionCertificateProvider.roomCondition[i];
        List<Map<String, dynamic>> subItems = [];
        for (var j = 0; j < item.subItems.length; j++) {
          var subItem = item.subItems[j];
          subItems.add({
            'id': subItem.id,
            'survey_title_id': subItem.surveyTitleId,
            'item': subItem.item,
            'grade_cat_l': subItem.gradeCatL,
            'grade_cat_c': subItem.gradeCatC,
            'grade_cat_fl': subItem.gradeCatFl,
            'grade_cat_fu': subItem.gradeCatFu,
            'grade_l': gradeLControllers[i][j].text.isNotEmpty
                ? int.parse(gradeLControllers[i][j].text)
                : 0,
            'grade_c': gradeLControllers[i][j].text.isNotEmpty
                ? int.parse(gradeLControllers[i][j].text)
                : 0,
            'grade_fl': gradeLControllers[i][j].text.isNotEmpty
                ? int.parse(gradeLControllers[i][j].text)
                : 0,
            'grade_fu': gradeLControllers[i][j].text.isNotEmpty
                ? int.parse(gradeLControllers[i][j].text)
                : 0,
            'finding': findingControllers[i][j].text,
            'remark': remarkControllers[i][j].text,
            'voice_note_path': listFilesAudio[int.parse("$i$j")] ?? [],
            'images_path': listFilesImage[int.parse("$i$j")] ?? [],
            'file_path': listFilesPdf[int.parse("$i$j")] ?? [],
          });
        }

        listItems.add({
          'id': item.id,
          'item': item.item,
          'sub_items': subItems,
        });
      }

      Map<String, dynamic> requestBody = {'items': listItems};
      var isSuccess = await inspectionCertificateProvider.updateItem(
        bodyRequest: requestBody,
        titleId: widget.titleId,
        titleApi: widget.formCode,
      );

      setState(() {
        isLoading = true;
      });

      if (isSuccess) {
        messageAlert = "Berhasil di simpan";
      } else {
        messageAlert = "Data tidak berhasil di simpan";
        setState(() {
          isLoading = false;
        });
      }
      return isSuccess;
    }

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.titleShip,
          style: semiboldPoppins,
        ),
        backgroundColor: whiteColor,
        foregroundColor: primaryColor,
      ),
      body: Stack(
        children: [
          FutureBuilder(
            future: inspectionCertificateProvider.roomConditionExam(
              titleApi: widget.formCode,
              titleId: widget.titleId,
            ),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const LoadingIndicator();
              } else if (snapshot.hasError) {
                return Text('Error : ${snapshot.error}');
              } else {
                if (inspectionCertificateProvider.roomCondition.isEmpty) {
                  return Center(
                    child: Text(
                      "Menu Belum tersedia",
                      style: regularPoppins,
                    ),
                  );
                }
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 0,
                      ),
                      child: ListTile(
                        leading: const Icon(
                          Icons.radio_button_checked_outlined,
                          color: primaryColor,
                        ),
                        title: Text(
                          widget.titleMenu,
                          style: semiboldPoppins,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          bottom: 20,
                          left: 20,
                          right: 20,
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              ...inspectionCertificateProvider.roomCondition
                                  .asMap()
                                  .entries
                                  .map(
                                (entry) {
                                  var data = entry.value;
                                  var index = entry.key;
                                  return Column(
                                    children: [
                                      ExpansionTile(
                                        shape: RoundedRectangleBorder(
                                          side: BorderSide(
                                            color:
                                                secondaryColor.withOpacity(0.6),
                                            width: 2,
                                          ),
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        collapsedShape:
                                            const RoundedRectangleBorder(
                                          side: BorderSide(
                                            color: greyColor,
                                          ),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        collapsedIconColor: primaryColor,
                                        collapsedTextColor: primaryColor,
                                        iconColor: greyColor,
                                        textColor: secondaryColor,
                                        tilePadding: const EdgeInsets.symmetric(
                                          horizontal: 10,
                                        ),
                                        title: Text(
                                          data.item,
                                          style: const TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        children: [
                                          Divider(
                                            color:
                                                secondaryColor.withOpacity(0.6),
                                          ),
                                          ...data.subItems.asMap().entries.map(
                                            (entry2) {
                                              var dataSubItem = entry2.value;
                                              var indexSubItem = entry2.key;

                                              listFilesPdf[int.parse(
                                                      "$index$indexSubItem")] =
                                                  dataSubItem.filePath;
                                              listFilesImage[int.parse(
                                                      "$index$indexSubItem")] =
                                                  dataSubItem.imagesPath;
                                              listFilesAudio[int.parse(
                                                      "$index$indexSubItem")] =
                                                  dataSubItem.voiceNotePath;

                                              return Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  children: [
                                                    ExpansionTile(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        side: BorderSide(
                                                          color: secondaryColor
                                                              .withOpacity(0.6),
                                                          width: 2,
                                                        ),
                                                        borderRadius:
                                                            const BorderRadius
                                                                .all(
                                                          Radius.circular(8),
                                                        ),
                                                      ),
                                                      collapsedShape:
                                                          const RoundedRectangleBorder(
                                                        side: BorderSide(
                                                          color: greyColor,
                                                        ),
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(8),
                                                        ),
                                                      ),
                                                      collapsedIconColor:
                                                          primaryColor,
                                                      collapsedTextColor:
                                                          primaryColor,
                                                      iconColor: greyColor,
                                                      textColor: secondaryColor,
                                                      tilePadding:
                                                          const EdgeInsets
                                                              .symmetric(
                                                        horizontal: 10,
                                                      ),
                                                      title: Text(
                                                        dataSubItem.item,
                                                        style: const TextStyle(
                                                          fontFamily: 'Poppins',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                      ),
                                                      children: [
                                                        Divider(
                                                          color: secondaryColor
                                                              .withOpacity(0.6),
                                                        ),
                                                        RatingWidget(
                                                          currentRating:
                                                              dataSubItem
                                                                  .gradeL,
                                                          idCategoryGrading:
                                                              dataSubItem
                                                                  .gradeCatL,
                                                          label:
                                                              "Kategori Grade L",
                                                          onChanged: (value) {
                                                            gradeLControllers[
                                                                            index]
                                                                        [
                                                                        indexSubItem]
                                                                    .text =
                                                                value
                                                                    .toString();

                                                            gradingL[index][
                                                                    indexSubItem] =
                                                                dashboardProvider
                                                                    .grading
                                                                    .description;

                                                            findingControllers[
                                                                            index]
                                                                        [
                                                                        indexSubItem]
                                                                    .text =
                                                                mergeFinding(
                                                                    index,
                                                                    indexSubItem);
                                                          },
                                                        ),
                                                        RatingWidget(
                                                          currentRating:
                                                              dataSubItem
                                                                  .gradeC,
                                                          idCategoryGrading:
                                                              dataSubItem
                                                                  .gradeCatC,
                                                          label:
                                                              "Kategori Grade C",
                                                          onChanged: (value) {
                                                            gradeCControllers[
                                                                            index]
                                                                        [
                                                                        indexSubItem]
                                                                    .text =
                                                                value
                                                                    .toString();

                                                            gradingC[index][
                                                                    indexSubItem] =
                                                                dashboardProvider
                                                                    .grading
                                                                    .description;

                                                            findingControllers[
                                                                            index]
                                                                        [
                                                                        indexSubItem]
                                                                    .text =
                                                                mergeFinding(
                                                                    index,
                                                                    indexSubItem);
                                                          },
                                                        ),
                                                        RatingWidget(
                                                          currentRating:
                                                              dataSubItem
                                                                  .gradeFl,
                                                          idCategoryGrading:
                                                              dataSubItem
                                                                  .gradeCatFl,
                                                          label:
                                                              "Kategori Grade FL",
                                                          onChanged: (value) {
                                                            gradeFLControllers[
                                                                            index]
                                                                        [
                                                                        indexSubItem]
                                                                    .text =
                                                                value
                                                                    .toString();

                                                            gradingFL[index][
                                                                    indexSubItem] =
                                                                dashboardProvider
                                                                    .grading
                                                                    .description;

                                                            findingControllers[
                                                                            index]
                                                                        [
                                                                        indexSubItem]
                                                                    .text =
                                                                mergeFinding(
                                                                    index,
                                                                    indexSubItem);
                                                          },
                                                        ),
                                                        RatingWidget(
                                                          currentRating:
                                                              dataSubItem
                                                                  .gradeFu,
                                                          idCategoryGrading:
                                                              dataSubItem
                                                                  .gradeCatFu,
                                                          label:
                                                              "Kategori Grade FU",
                                                          onChanged: (value) {
                                                            gradeFUControllers[
                                                                            index]
                                                                        [
                                                                        indexSubItem]
                                                                    .text =
                                                                value
                                                                    .toString();

                                                            gradingFU[index][
                                                                    indexSubItem] =
                                                                dashboardProvider
                                                                    .grading
                                                                    .description;

                                                            findingControllers[
                                                                            index]
                                                                        [
                                                                        indexSubItem]
                                                                    .text =
                                                                mergeFinding(
                                                                    index,
                                                                    indexSubItem);
                                                          },
                                                        ),
                                                        ItemWidget(
                                                          controller:
                                                              findingControllers[
                                                                      index][
                                                                  indexSubItem],
                                                          label: "Temuan",
                                                        ),
                                                        ItemWidget(
                                                          controller:
                                                              remarkControllers[
                                                                      index][
                                                                  indexSubItem],
                                                          label: "Keterangan",
                                                        ),
                                                        const Divider(),
                                                        MediaAttachmentPDF(
                                                          onMediaSelectedForDeleted:
                                                              (pdfsDeleted) {
                                                            if (listFilesPdf[
                                                                    int.parse(
                                                                        "$index$indexSubItem")] !=
                                                                null) {
                                                              listFilesPdf[
                                                                      int.parse(
                                                                          "$index$indexSubItem")]!
                                                                  .remove(
                                                                      pdfsDeleted![
                                                                          0]);
                                                            } else {
                                                              listFilesPdf[
                                                                      int.parse(
                                                                          "$index$indexSubItem")] =
                                                                  pdfsDeleted!;
                                                            }
                                                          },
                                                          itemId:
                                                              dataSubItem.id,
                                                          totalCurrentFile:
                                                              dataSubItem
                                                                  .filePath
                                                                  .length,
                                                          currentFile:
                                                              dataSubItem
                                                                  .filePath,
                                                          titleId:
                                                              widget.titleId,
                                                          formCode:
                                                              widget.formCode,
                                                          onMediaSelected:
                                                              (pdfs) {
                                                            if (listFilesPdf[
                                                                    int.parse(
                                                                        "$index$indexSubItem")] !=
                                                                null) {
                                                              listFilesPdf[
                                                                      int.parse(
                                                                          "$index$indexSubItem")]!
                                                                  .addAll(
                                                                      pdfs!);
                                                            } else {
                                                              listFilesPdf[
                                                                      int.parse(
                                                                          "$index$indexSubItem")] =
                                                                  pdfs!;
                                                            }
                                                          },
                                                        ),
                                                        MediaAttachmentImage(
                                                          totalCurrentImage:
                                                              dataSubItem
                                                                  .imagesPath
                                                                  .length,
                                                          currentImage:
                                                              dataSubItem
                                                                  .imagesPath,
                                                          formCode:
                                                              widget.formCode,
                                                          titleId:
                                                              widget.titleId,
                                                          onMediaSelected:
                                                              (images) {
                                                            if (listFilesImage[
                                                                    int.parse(
                                                                        "$index$indexSubItem")] !=
                                                                null) {
                                                              listFilesImage[
                                                                      int.parse(
                                                                          "$index$indexSubItem")]!
                                                                  .addAll(
                                                                      images!);
                                                            } else {
                                                              listFilesImage[
                                                                      int.parse(
                                                                          "$index$indexSubItem")] =
                                                                  images!;
                                                            }
                                                          },
                                                        ),
                                                        MediaAttachmentVoiceNote(
                                                          currentAudio:
                                                              dataSubItem
                                                                  .voiceNotePath,
                                                          formCode:
                                                              widget.formCode,
                                                          titleId:
                                                              widget.titleId,
                                                          onMediaSelected:
                                                              (vn) {
                                                            if (listFilesAudio[
                                                                    int.parse(
                                                                        "$index$indexSubItem")] !=
                                                                null) {
                                                              listFilesAudio[
                                                                      int.parse(
                                                                          "$index$indexSubItem")]!
                                                                  .addAll(vn!);
                                                            } else {
                                                              listFilesAudio[
                                                                      int.parse(
                                                                          "$index$indexSubItem")] =
                                                                  vn!;
                                                            }
                                                          },
                                                        )
                                                      ],
                                                    ),
                                                    const SizedBox(
                                                      height: 5,
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  );
                                },
                              ),
                              const SizedBox(
                                height: 50,
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                );
              }
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10,
              ),
              // height: 80,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(8),
                ),
                color: whiteColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 10,
                    offset: const Offset(0, -5),
                  ),
                ],
              ),
              child: isLoading
                  ? const LoadingIndicator()
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonCustom(
                          backgroundColor: whiteColor,
                          label: "Cancel",
                          outlineBorderColor: redColor,
                          textColor: redColor,
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                        ),
                        ButtonCustom(
                          backgroundColor: primaryColor,
                          label: "Save",
                          outlineBorderColor: primaryColor,
                          textColor: whiteColor,
                          onPressed: () async {
                            var isSuccess = await handleUpdateRoomCondition();

                            // ignore: use_build_context_synchronously
                            SnackbarCustom.alertMessage(
                              context,
                              messageAlert,
                              2,
                              isSuccess,
                            );

                            Future.delayed(
                              const Duration(seconds: 1),
                              () {
                                Navigator.pop(context, isSuccess);
                              },
                            );
                          },
                        ),
                      ],
                    ),
            ),
          ),
        ],
      ),
    );
  }
}

class RatingWidget extends StatefulWidget {
  final ValueChanged<int> onChanged;
  final int currentRating;
  final int idCategoryGrading;
  final String label;

  const RatingWidget({
    Key? key,
    required this.onChanged,
    required this.currentRating,
    required this.idCategoryGrading,
    required this.label,
  }) : super(key: key);

  @override
  RatingWidgetState createState() => RatingWidgetState();
}

class RatingWidgetState extends State<RatingWidget> {
  late List<bool> isFilled;

  @override
  void initState() {
    super.initState();
    isFilled = List.generate(4, (index) => false);
    for (int i = 0; i < widget.currentRating; i++) {
      isFilled[i] = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);

    Future<bool> getGrading(int grade, String idCategoryGrading) async {
      var isSuccess = await dashboardProvider.getGrading(
          grade: grade.toString(), idCategoryGrading: idCategoryGrading);
      if (isSuccess) {
        return isSuccess;
      } else {
        return false;
      }
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Row(
            children: List.generate(4, (index) {
              return InkWell(
                onTap: () async {
                  setState(() {
                    for (int i = 0; i <= index; i++) {
                      isFilled[i] = true;
                    }
                    for (int i = index + 1; i < 4; i++) {
                      isFilled[i] = false;
                    }
                  });
                  // Mengirim nilai rating ke parent widget
                  await getGrading(
                      index + 1, widget.idCategoryGrading.toString());
                  widget.onChanged(index + 1);
                },
                child: Icon(
                  Icons.star,
                  size: 30,
                  color:
                      isFilled[index] ? thirdColor : greyColor.withOpacity(0.6),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
}

class ItemWidget extends StatefulWidget {
  const ItemWidget({
    super.key,
    required this.controller,
    required this.label,
  });

  final String label;
  final TextEditingController? controller;

  @override
  State<ItemWidget> createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<ItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Container(
            constraints: const BoxConstraints(
              minHeight: 50,
              minWidth: 200,
              maxHeight: 80,
              maxWidth: 200,
            ),
            child: TextField(
              minLines: 2,
              maxLines: 5,
              controller: widget.controller,
              style: regularPoppins.copyWith(
                fontSize: 14,
              ),
              cursorColor: primaryColor,
              keyboardType: TextInputType.multiline,
              autofocus: false,
              onTap: () async {},
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(
                  10.0,
                ),
                labelStyle: regularPoppins,
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                ),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
