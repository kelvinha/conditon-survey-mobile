import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/media_attachment_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailDeckEquipmentPage extends StatefulWidget {
  const DetailDeckEquipmentPage({super.key});

  @override
  State<DetailDeckEquipmentPage> createState() =>
      DetailDeckEquipmentPageState();
}

class DetailDeckEquipmentPageState extends State<DetailDeckEquipmentPage> {
  @override
  Widget build(BuildContext context) {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(
          bottom: 20,
          left: 20,
          right: 20,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ...inspectionCertificateProvider.deckEquipment.map(
                (data) {
                  return Column(
                    children: [
                      ExpansionTile(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                            color: secondaryColor.withOpacity(0.6),
                            width: 2,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        collapsedShape: const RoundedRectangleBorder(
                          side: BorderSide(
                            color: greyColor,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        collapsedIconColor: primaryColor,
                        collapsedTextColor: primaryColor,
                        iconColor: greyColor,
                        textColor: secondaryColor,
                        tilePadding: const EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        title: Text(
                          data.item,
                          style: const TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        children: [
                          Divider(
                            color: secondaryColor.withOpacity(0.6),
                          ),
                          ...data.subItems.map(
                            (dataSubItem) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    ExpansionTile(
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                          color:
                                              secondaryColor.withOpacity(0.6),
                                          width: 2,
                                        ),
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(8),
                                        ),
                                      ),
                                      collapsedShape:
                                          const RoundedRectangleBorder(
                                        side: BorderSide(
                                          color: greyColor,
                                        ),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(8),
                                        ),
                                      ),
                                      collapsedIconColor: primaryColor,
                                      collapsedTextColor: primaryColor,
                                      iconColor: greyColor,
                                      textColor: secondaryColor,
                                      tilePadding: const EdgeInsets.symmetric(
                                        horizontal: 10,
                                      ),
                                      title: Text(
                                        dataSubItem.item,
                                        style: const TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      children: [
                                        Divider(
                                          color:
                                              secondaryColor.withOpacity(0.6),
                                        ),
                                        RatingWidget(
                                          label: "Kategori Grade C",
                                          grade: dataSubItem.gradeC,
                                        ),
                                        RatingWidget(
                                          label: "Kategori Grade V",
                                          grade: dataSubItem.gradeV,
                                        ),
                                        RatingWidget(
                                          label: "Kategori Grade M",
                                          grade: dataSubItem.gradeM,
                                        ),
                                        ItemWidget(
                                          label: "Temuan",
                                          value: dataSubItem.finding,
                                        ),
                                        ItemWidget(
                                          label: "Keterangan",
                                          value: dataSubItem.remark,
                                        ),
                                        ContentMediaAttachmentWidget(
                                          inspectionCertificateProvider:
                                              inspectionCertificateProvider,
                                          data: dataSubItem,
                                        )
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  );
                },
              ),
              const SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ItemWidget extends StatelessWidget {
  const ItemWidget({
    super.key,
    required this.label,
    required this.value,
  });

  final String label;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            width: 100,
          ),
          Flexible(
            child: Text(
              value,
              style: lightPoppins.copyWith(
                fontSize: 14,
                color: primaryColor,
              ),
              textAlign: TextAlign.right,
              maxLines: 3,
            ),
          ),
        ],
      ),
    );
  }
}

class RatingWidget extends StatelessWidget {
  final String label;
  final int grade;

  const RatingWidget({
    super.key,
    required this.label,
    required this.grade,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Row(
            children: List.generate(4, (index) {
              return Icon(
                Icons.star,
                size: 20,
                color: index < grade
                    ? thirdColor
                    : greyColor.withOpacity(
                        0.6,
                      ),
              );
            }),
          ),
        ],
      ),
    );
  }
}

class ContentMediaAttachmentWidget extends StatelessWidget {
  const ContentMediaAttachmentWidget({
    super.key,
    required this.inspectionCertificateProvider,
    required this.data,
  });

  final InspectionCertificateProvider inspectionCertificateProvider;
  final dynamic data;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Media Attachment",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          MediaAttachment(
            formCode: inspectionCertificateProvider.subMenuActive,
            data: data,
          ),
        ],
      ),
    );
  }
}
