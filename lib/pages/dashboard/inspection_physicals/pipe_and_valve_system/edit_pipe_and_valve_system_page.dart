// ignore_for_file: avoid_print
import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/button_custom_widget.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../widgets/media_attachment_image_widget.dart';
import '../../../../widgets/media_attachment_pdf_widget.dart';
import '../../../../widgets/media_attachment_voice_note_widget.dart';

class EditPipeAndValveSystemPage extends StatefulWidget {
  final String titleId;
  final String titleShip;
  final String titleMenu;
  final String formCode;

  const EditPipeAndValveSystemPage({
    super.key,
    required this.titleId,
    required this.titleShip,
    required this.titleMenu,
    required this.formCode,
  });

  @override
  State<EditPipeAndValveSystemPage> createState() =>
      EditPipeAndValveSystemPageState();
}

class EditPipeAndValveSystemPageState
    extends State<EditPipeAndValveSystemPage> {
  List<TextEditingController> remarkControllers = [];
  List<TextEditingController> gradeVPControllers = [];
  List<TextEditingController> gradeVVControllers = [];
  List<TextEditingController> findingControllers = [];
  List<String> gradingVP = [];
  List<String> gradingVV = [];
  Map<int, List<String>> listFilesPdf = {};
  Map<int, List<String>> listFilesImage = {};
  Map<int, List<String>> listFilesAudio = {};

  bool isLoading = false;
  String messageAlert = '';

  @override
  void initState() {
    super.initState();

    // Inisialisasi controller sesuai jumlah data yang ada
    final inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    gradeVPControllers = List.generate(
      inspectionCertificateProvider.pipeValveSystemCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data =
            inspectionCertificateProvider.pipeValveSystemCertificate[index];
        controller.text = data.gradeVp.toString();
        return controller;
      },
    );

    gradeVVControllers = List.generate(
      inspectionCertificateProvider.pipeValveSystemCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data =
            inspectionCertificateProvider.pipeValveSystemCertificate[index];
        controller.text = data.gradeCatVv.toString();
        return controller;
      },
    );

    gradingVP = List.generate(3, (index) => '');
    gradingVV = List.generate(3, (index) => '');

    remarkControllers = List.generate(
      inspectionCertificateProvider.pipeValveSystemCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data =
            inspectionCertificateProvider.pipeValveSystemCertificate[index];
        controller.text = data.remark!;
        return controller;
      },
    );

    findingControllers = List.generate(
      inspectionCertificateProvider.pipeValveSystemCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data =
            inspectionCertificateProvider.pipeValveSystemCertificate[index];
        controller.text = data.finding!;
        return controller;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    // ignore: unused_element
    String mergeFinding(int index) {
      var findingText = '- ${gradingVP[index]}\n- ${gradingVV[index]}';
      return findingText;
    }

    Future<bool> handleUpdatePipeAndValveSystem() async {
      List<Map<String, dynamic>> itemsList = [];

      for (var i = 0;
          i < inspectionCertificateProvider.pipeValveSystemCertificate.length;
          i++) {
        var data = inspectionCertificateProvider.pipeValveSystemCertificate[i];
        Map<String, dynamic> itemMap = {
          'id': data.id,
          'item': data.item,
          'grade_cat_vp': data.gradeCatVp,
          'grade_cat_vv': data.gradeCatVv,
          'grade_vp': gradeVPControllers[i].text.isNotEmpty
              ? int.parse(gradeVPControllers[i].text)
              : 0,
          'grade_vv': gradeVVControllers[i].text.isNotEmpty
              ? int.parse(gradeVVControllers[i].text)
              : 0,
          'finding': findingControllers[i].text,
          'remark': remarkControllers[i].text,
          'voice_note_path': listFilesAudio[i],
          'images_path': listFilesImage[i],
          'file_path': listFilesPdf[i],
        };

        itemsList.add(itemMap);
      }

      Map<String, dynamic> requestBody = {'items': itemsList};
      var isSuccess =
          await inspectionCertificateProvider.updatePipeValveSystemItem(
        bodyRequest: requestBody,
        titleId: widget.titleId,
        titleApi: widget.formCode,
      );

      setState(() {
        isLoading = true;
      });

      if (isSuccess) {
        messageAlert = "Berhasil di simpan";
      } else {
        messageAlert = "Data tidak berhasil di simpan";
        setState(() {
          isLoading = false;
        });
      }
      return isSuccess;
    }

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.titleShip,
          style: semiboldPoppins,
        ),
        backgroundColor: whiteColor,
        foregroundColor: primaryColor,
      ),
      body: Stack(
        children: [
          FutureBuilder(
            future: inspectionCertificateProvider.pipeValveSystemExam(
              titleApi: widget.formCode,
              titleId: widget.titleId,
            ),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const LoadingIndicator();
              } else if (snapshot.hasError) {
                return Text('Error : ${snapshot.error}');
              } else {
                if (inspectionCertificateProvider
                    .pipeValveSystemCertificate.isEmpty) {
                  return Center(
                    child: Text(
                      "Menu Belum tersedia",
                      style: regularPoppins,
                    ),
                  );
                }
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 0,
                      ),
                      child: ListTile(
                        leading: const Icon(
                          Icons.radio_button_checked_outlined,
                          color: primaryColor,
                        ),
                        title: Text(
                          widget.titleMenu,
                          style: semiboldPoppins,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          bottom: 20,
                          left: 20,
                          right: 20,
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              ...inspectionCertificateProvider
                                  .pipeValveSystemCertificate
                                  .asMap()
                                  .entries
                                  .map(
                                (entry) {
                                  var data = entry.value;
                                  var index = entry.key;

                                  listFilesPdf[index] = data.filePath;
                                  listFilesImage[index] = data.imagesPath;
                                  return Column(
                                    children: [
                                      ExpansionTile(
                                        shape: RoundedRectangleBorder(
                                          side: BorderSide(
                                            color:
                                                secondaryColor.withOpacity(0.6),
                                            width: 2,
                                          ),
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        collapsedShape:
                                            const RoundedRectangleBorder(
                                          side: BorderSide(
                                            color: greyColor,
                                          ),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        collapsedIconColor: primaryColor,
                                        collapsedTextColor: primaryColor,
                                        iconColor: greyColor,
                                        textColor: secondaryColor,
                                        tilePadding: const EdgeInsets.symmetric(
                                          horizontal: 10,
                                        ),
                                        title: Text(
                                          data.item,
                                          style: const TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        children: [
                                          Divider(
                                            color:
                                                secondaryColor.withOpacity(0.6),
                                          ),
                                          RatingWidget(
                                            currentRating: data.gradeVp,
                                            idCategoryGrading: data.gradeCatVp,
                                            label: "Kategori Grade VP",
                                            onChanged: (value) {
                                              gradeVPControllers[index].text =
                                                  value.toString();

                                              gradingVP[index] =
                                                  dashboardProvider
                                                      .grading.description;

                                              findingControllers[index].text =
                                                  mergeFinding(index);
                                            },
                                          ),
                                          RatingWidget(
                                            currentRating: data.gradeCatVv,
                                            idCategoryGrading: data.gradeCatVv,
                                            label: "Ketagori Grade VV",
                                            onChanged: (value) {
                                              gradeVVControllers[index].text =
                                                  value.toString();

                                              gradingVV[index] =
                                                  dashboardProvider
                                                      .grading.description;

                                              findingControllers[index].text =
                                                  mergeFinding(index);
                                            },
                                          ),
                                          ItemWidget(
                                            controller:
                                                findingControllers[index],
                                            label: "Temuan",
                                          ),
                                          ItemWidget(
                                            controller:
                                                remarkControllers[index],
                                            label: "Keterangan",
                                          ),
                                          const Divider(),
                                          MediaAttachmentPDF(
                                            onMediaSelectedForDeleted:
                                                (pdfsDeleted) {
                                              if (listFilesPdf[index] != null) {
                                                listFilesPdf[index]!
                                                    .remove(pdfsDeleted![0]);
                                              } else {
                                                listFilesPdf[index] =
                                                    pdfsDeleted!;
                                              }
                                            },
                                            itemId: data.id,
                                            totalCurrentFile:
                                                listFilesPdf[index]!.length,
                                            currentFile: data.filePath,
                                            titleId: widget.titleId,
                                            formCode: widget.formCode,
                                            onMediaSelected: (pdfs) {
                                              if (listFilesPdf[index] != null) {
                                                listFilesPdf[index]!
                                                    .addAll(pdfs!);
                                              } else {
                                                listFilesPdf[index] = pdfs!;
                                              }
                                            },
                                          ),
                                          MediaAttachmentImage(
                                            totalCurrentImage:
                                                listFilesImage[index]!.length,
                                            currentImage: data.imagesPath,
                                            formCode: widget.formCode,
                                            titleId: widget.titleId,
                                            onMediaSelected: (images) {
                                              if (listFilesImage[index] !=
                                                  null) {
                                                listFilesImage[index]!
                                                    .addAll(images!);
                                              } else {
                                                listFilesImage[index] = images!;
                                              }
                                            },
                                          ),
                                          MediaAttachmentVoiceNote(
                                            currentAudio: data.voiceNotePath,
                                            formCode: widget.formCode,
                                            titleId: widget.titleId,
                                            onMediaSelected: (vn) {
                                              if (listFilesAudio[index] !=
                                                  null) {
                                                listFilesAudio[index]!
                                                    .addAll(vn!);
                                              } else {
                                                listFilesAudio[index] = vn!;
                                              }
                                            },
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  );
                                },
                              ),
                              const SizedBox(
                                height: 50,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              }
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10,
              ),
              // height: 80,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(8),
                ),
                color: whiteColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 10,
                    offset: const Offset(0, -5),
                  ),
                ],
              ),
              child: isLoading
                  ? const LoadingIndicator()
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonCustom(
                          backgroundColor: whiteColor,
                          label: "Cancel",
                          outlineBorderColor: redColor,
                          textColor: redColor,
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                        ),
                        ButtonCustom(
                          backgroundColor: primaryColor,
                          label: "Save",
                          outlineBorderColor: primaryColor,
                          textColor: whiteColor,
                          onPressed: () async {
                            var isSuccess =
                                await handleUpdatePipeAndValveSystem();

                            // ignore: use_build_context_synchronously
                            SnackbarCustom.alertMessage(
                              context,
                              messageAlert,
                              2,
                              isSuccess,
                            );

                            Future.delayed(
                              const Duration(seconds: 1),
                              () {
                                Navigator.pop(context, isSuccess);
                              },
                            );
                          },
                        ),
                      ],
                    ),
            ),
          ),
        ],
      ),
    );
  }
}

class ItemWidget extends StatefulWidget {
  const ItemWidget({
    super.key,
    required this.controller,
    required this.label,
  });

  final String label;
  final TextEditingController? controller;

  @override
  State<ItemWidget> createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<ItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Container(
            constraints: const BoxConstraints(
              minHeight: 50,
              minWidth: 200,
              maxHeight: 80,
              maxWidth: 200,
            ),
            child: TextField(
              minLines: 2,
              maxLines: 5,
              controller: widget.controller,
              style: regularPoppins.copyWith(
                fontSize: 14,
              ),
              cursorColor: primaryColor,
              keyboardType: TextInputType.multiline,
              autofocus: false,
              onTap: () async {},
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(
                  10.0,
                ),
                labelStyle: regularPoppins,
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                ),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class RatingWidget extends StatefulWidget {
  final ValueChanged<int> onChanged;
  final int currentRating;
  final int idCategoryGrading;
  final String label;

  const RatingWidget({
    Key? key,
    required this.onChanged,
    required this.currentRating,
    required this.idCategoryGrading,
    required this.label,
  }) : super(key: key);

  @override
  RatingWidgetState createState() => RatingWidgetState();
}

class RatingWidgetState extends State<RatingWidget> {
  late List<bool> isFilled;

  @override
  void initState() {
    super.initState();
    isFilled = List.generate(4, (index) => false);
    for (int i = 0; i < widget.currentRating; i++) {
      isFilled[i] = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);

    Future<bool> getGrading(int grade, String idCategoryGrading) async {
      var isSuccess = await dashboardProvider.getGrading(
          grade: grade.toString(), idCategoryGrading: idCategoryGrading);
      if (isSuccess) {
        return isSuccess;
      } else {
        return false;
      }
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Row(
            children: List.generate(4, (index) {
              return InkWell(
                onTap: () async {
                  setState(() {
                    for (int i = 0; i <= index; i++) {
                      isFilled[i] = true;
                    }
                    for (int i = index + 1; i < 4; i++) {
                      isFilled[i] = false;
                    }
                  });
                  // Mengirim nilai rating ke parent widget
                  await getGrading(
                      index + 1, widget.idCategoryGrading.toString());
                  widget.onChanged(index + 1);
                },
                child: Icon(
                  Icons.star,
                  size: 45,
                  color:
                      isFilled[index] ? thirdColor : greyColor.withOpacity(0.6),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
}
