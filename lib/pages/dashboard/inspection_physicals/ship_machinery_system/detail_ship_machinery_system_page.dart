import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/media_attachment_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailShipMachinerySystemPage extends StatefulWidget {
  const DetailShipMachinerySystemPage({super.key});

  @override
  State<DetailShipMachinerySystemPage> createState() =>
      _DetailShipMachinerySystemPageState();
}

class _DetailShipMachinerySystemPageState
    extends State<DetailShipMachinerySystemPage> {
  @override
  Widget build(BuildContext context) {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(
          bottom: 20,
          left: 20,
          right: 20,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ...inspectionCertificateProvider.shipMachinerySystemCertificate
                  .map(
                (data) {
                  return Column(
                    children: [
                      ExpansionTile(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                            color: secondaryColor.withOpacity(0.6),
                            width: 2,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        collapsedShape: const RoundedRectangleBorder(
                          side: BorderSide(
                            color: greyColor,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        collapsedIconColor: primaryColor,
                        collapsedTextColor: primaryColor,
                        iconColor: greyColor,
                        textColor: secondaryColor,
                        tilePadding: const EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        title: Text(
                          data.item,
                          style: const TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        children: [
                          Divider(
                            color: secondaryColor.withOpacity(0.6),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  "Item Detail",
                                  style: mediumPoppins.copyWith(
                                    fontSize: 14,
                                    color: primaryColor,
                                  ),
                                ),
                                Text(
                                  data.itemDetail,
                                  style: lightPoppins.copyWith(
                                    fontSize: 14,
                                    color: primaryColor,
                                  ),
                                  textAlign: TextAlign.right,
                                ),
                              ],
                            ),
                          ),
                          RatingWidget(
                              title: "Kategori Grade V", value: data.gradeV),
                          RatingWidget(
                              title: "Kategori Grade F", value: data.gradeF),
                          RatingWidget(
                              title: "Kategori Grade P",
                              value: data.gradeP ?? 0),
                          RatingWidget(
                              title: "Kategori Grade M",
                              value: data.gradeM ?? 0),
                          ItemWidget(
                              title: "Temuan", value: data.finding ?? "-"),
                          ItemWidget(
                              title: "Keterangan", value: data.remark ?? "-"),
                          ContentMediaAttachmentWidget(
                              inspectionCertificateProvider:
                                  inspectionCertificateProvider,
                              data: data)
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  );
                },
              ),
              const SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ItemWidget extends StatelessWidget {
  const ItemWidget({super.key, required this.value, required this.title});

  final String value;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            width: 100,
          ),
          Flexible(
            child: Text(
              value,
              style: lightPoppins.copyWith(
                fontSize: 14,
                color: primaryColor,
              ),
              textAlign: TextAlign.right,
              maxLines: 3,
            ),
          ),
        ],
      ),
    );
  }
}

class RatingWidget extends StatelessWidget {
  const RatingWidget({super.key, required this.value, required this.title});

  final int value;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            width: 100,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: List.generate(4, (index) {
                    return Icon(
                      Icons.star,
                      size: 20,
                      color: index < value
                          ? thirdColor
                          : greyColor.withOpacity(
                              0.6,
                            ),
                    );
                  }),
                ),
              ],
            ),
          )
          // Flexible(
          //   child: Text(
          //     data.gradeCatC ?? "-",
          //     style: lightPoppins.copyWith(
          //       fontSize: 14,
          //       color: primaryColor,
          //     ),
          //     textAlign: TextAlign.right,
          //     maxLines: 3,
          //   ),
          // ),
        ],
      ),
    );
  }
}

class ContentMediaAttachmentWidget extends StatelessWidget {
  const ContentMediaAttachmentWidget({
    super.key,
    required this.inspectionCertificateProvider,
    required this.data,
  });

  final InspectionCertificateProvider inspectionCertificateProvider;
  final dynamic data;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Media Attachment",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          MediaAttachment(
            formCode: inspectionCertificateProvider.subMenuActive,
            data: data,
          ),
        ],
      ),
    );
  }
}
