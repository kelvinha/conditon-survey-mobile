import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/media_attachment_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailShipLegalitiesPage extends StatefulWidget {
  const DetailShipLegalitiesPage({
    super.key,
  });

  @override
  State<DetailShipLegalitiesPage> createState() =>
      _DetailShipLegalitiesPageState();
}

class _DetailShipLegalitiesPageState extends State<DetailShipLegalitiesPage> {
  @override
  Widget build(BuildContext context) {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(
          bottom: 20,
          left: 20,
          right: 20,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ...inspectionCertificateProvider.surveyCertificate.map(
                (data) {
                  return Column(
                    children: [
                      ListItemSurvey(
                        inspectionCertificateProvider: inspectionCertificateProvider,
                        data: data,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  );
                },
              ),
              const SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ListItemSurvey extends StatelessWidget {
  const ListItemSurvey({
    super.key,
    required this.inspectionCertificateProvider,
    required this.data,
  });

  final InspectionCertificateProvider inspectionCertificateProvider;
  final dynamic data;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: secondaryColor.withOpacity(0.6),
          width: 2,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(8),
        ),
      ),
      collapsedShape: const RoundedRectangleBorder(
        side: BorderSide(
          color: greyColor,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
      ),
      collapsedIconColor: primaryColor,
      collapsedTextColor: primaryColor,
      iconColor: greyColor,
      textColor: secondaryColor,
      tilePadding: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      title: Text(
        data.certificateName,
        style: const TextStyle(
          fontFamily: 'Poppins',
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
      ),
      children: [
        Divider(
          color: secondaryColor.withOpacity(0.6),
        ),
        ItemDataWidget(
          label: "No. Sertifikat",
          data: data.certificateNumber,
        ),
        ItemDataWidget(
          label: "Masa Berlaku",
          data: data.expiryDate,
        ),
        ItemDataWidget(
          label: "Temuan",
          data: data.finding,
        ),
        RatingWidget(data: data),
        ItemDataWidget(
          label: "Keterangan",
          data: data.remark,
        ),
        ContentMediaAttachmentWidget(
          inspectionCertificateProvider: inspectionCertificateProvider,
          data: data,
        ),
      ],
    );
  }
}

class ContentMediaAttachmentWidget extends StatelessWidget {
  const ContentMediaAttachmentWidget({
    super.key,
    required this.inspectionCertificateProvider,
    required this.data,
  });

  final InspectionCertificateProvider inspectionCertificateProvider;
  final dynamic data;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Media Attachment",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          MediaAttachment(
            formCode: inspectionCertificateProvider.subMenuActive,
            data: data,
          ),
        ],
      ),
    );
  }
}

class RatingWidget extends StatelessWidget {
  final dynamic data;

  const RatingWidget({
    super.key,
    required this.data,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Rating",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Row(
            children: List.generate(4, (index) {
              return Icon(
                Icons.star,
                size: 20,
                color: index < data.gradeL
                    ? thirdColor
                    : greyColor.withOpacity(
                        0.6,
                      ),
              );
            }),
          ),
        ],
      ),
    );
  }
}

class ItemDataWidget extends StatelessWidget {
  final String label;
  final String? data;

  const ItemDataWidget({
    super.key,
    required this.label,
    required this.data,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            width: 100,
          ),
          Flexible(
            child: Text(
              data ?? "-",
              style: lightPoppins.copyWith(
                fontSize: 14,
                color: primaryColor,
              ),
              textAlign: TextAlign.right,
              maxLines: 3,
            ),
          ),
        ],
      ),
    );
  }
}
