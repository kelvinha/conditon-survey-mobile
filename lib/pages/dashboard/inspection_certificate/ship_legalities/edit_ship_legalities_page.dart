// ignore_for_file: avoid_print

import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/providers/media_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/button_custom_widget.dart';
import 'package:condition_survey/widgets/list_audio_preview_widget.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sound/public/flutter_sound_recorder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../../../../widgets/media_attachment_image_widget.dart';
import '../../../../widgets/media_attachment_pdf_widget.dart';

class EditShipLegalitiesPage extends StatefulWidget {
  final String titleId;
  final String titleShip;
  final String titleMenu;
  final String formCode;

  const EditShipLegalitiesPage({
    required this.titleId,
    required this.titleShip,
    required this.titleMenu,
    required this.formCode,
    super.key,
  });

  @override
  State<EditShipLegalitiesPage> createState() => _EditShipLegalitiesPageState();
}

class _EditShipLegalitiesPageState extends State<EditShipLegalitiesPage> {
  final recorder = FlutterSoundRecorder();
  Map<String, int> listIdItem = {};
  Map<String, TextEditingController> controllers = {};
  Map<String, int> rating = {};
  Map<String, String> listCertificateName = {};
  bool isRecorderReady = false;

  List<TextEditingController> certificateControllers = [];
  List<TextEditingController> expiryDateControllers = [];
  List<TextEditingController> remarkControllers = [];
  List<TextEditingController> findingControllers = [];
  List<TextEditingController> gradeControllers = [];
  List<String> grading = [];
  Map<int, List<String>> listFilesPdf = {};
  Map<int, List<String>> listFilesImage = {};
  Map<int, List<String>> listFilesAudio = {};

  FilePickerResult? resultPDF;
  List<XFile>? resultImage;
  List<String>? listFilePdf;
  int total = 0;
  bool isLoading = false;

  List<bool> isFilled = List.generate(4, (index) => false);
  TextEditingController certificateNumberController = TextEditingController();

  late int itemID;
  late String messageAlert;

  @override
  void initState() {
    super.initState();
    initRecorder();
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    gradeControllers = List.generate(
      inspectionCertificateProvider.surveyCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data = inspectionCertificateProvider.surveyCertificate[index];
        controller.text = data.gradeL.toString();
        return controller;
      },
    );

    grading = List.generate(3, (index) => '');

    remarkControllers = List.generate(
      inspectionCertificateProvider.surveyCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data = inspectionCertificateProvider.surveyCertificate[index];
        if (data.remark != null) {
          controller.text = data.remark!;
        }
        return controller;
      },
    );

    findingControllers = List.generate(
      inspectionCertificateProvider.surveyCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data = inspectionCertificateProvider.surveyCertificate[index];
        if (data.finding != null) {
          controller.text = data.finding!;
        }
        return controller;
      },
    );

    certificateControllers = List.generate(
      inspectionCertificateProvider.surveyCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data = inspectionCertificateProvider.surveyCertificate[index];
        if (data.certificateNumber != null) {
          controller.text = data.certificateNumber!;
        }
        return controller;
      },
    );

    expiryDateControllers = List.generate(
      inspectionCertificateProvider.surveyCertificate.length,
      (index) {
        var controller = TextEditingController();
        var data = inspectionCertificateProvider.surveyCertificate[index];
        if (data.expiryDate != null) {
          controller.text = data.expiryDate!;
        }
        return controller;
      },
    );
  }

  Future initRecorder() async {
    final status = await Permission.microphone.status;
    if (status != PermissionStatus.granted) {
      final result = await Permission.microphone.request();
      if (result != PermissionStatus.granted) {
        print('debug Microphone permission not granted');
        return;
      }
    }

    // await recorder.openRecorder();
    setState(() {
      isRecorderReady = true;
    });
    // recorder.setSubscriptionDuration(const Duration(milliseconds: 500));
  }

  @override
  void dispose() {
    // Hapus semua controllers yang telah diinisialisasi
    // ignore: avoid_function_literals_in_foreach_calls
    controllers.values.forEach((controller) {
      controller.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    Future<int> validateTotalFile({
      int? totalPdf,
      int? totalImage,
      int? totalAudio,
    }) async {
      total = totalPdf! + totalImage! + totalAudio!;
      if (total > 20) {
        return 1;
      }
      return 0;
    }

    Future<bool> handleUpdateShipLegalites() async {
      List<Map<String, dynamic>> itemsList = [];

      for (var i = 0;
          i < inspectionCertificateProvider.surveyCertificate.length;
          i++) {
        var data = inspectionCertificateProvider.surveyCertificate[i];
        Map<String, dynamic> itemMap = {
          'id': data.id,
          'survey_title_id': widget.titleId,
          'grade_cat_l': data.gradeCatL,
          'grade_l': gradeControllers[i].text,
          'certificate_name': data.certificateName,
          'certificate_number': certificateControllers[i].text,
          'expiry_date': expiryDateControllers[i].text,
          'finding': findingControllers[i].text,
          'remark': remarkControllers[i].text,
          'voice_note_path': listFilesAudio[i],
          'images_path': listFilesImage[i],
          'file_path': listFilesPdf[i],
        };

        itemsList.add(itemMap);
      }

      Map<String, dynamic> requestBody = {'items': itemsList};
      var isSuccess =
          await inspectionCertificateProvider.updateSurveyShipLegalities(
        bodyRequest: requestBody,
        titleId: widget.titleId,
      );

      setState(() {
        isLoading = true;
      });

      if (isSuccess) {
        messageAlert = "Berhasil di simpan";
      } else {
        messageAlert = "Data tidak berhasil di simpan";
        setState(() {
          isLoading = false;
        });
      }
      return isSuccess;
    }

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.titleShip,
          style: semiboldPoppins,
        ),
        backgroundColor: whiteColor,
        foregroundColor: primaryColor,
      ),
      body: Stack(
        children: [
          FutureBuilder(
            future: inspectionCertificateProvider.getSurveyItemCertificate(
              titleApi: widget.formCode,
              titleId: widget.titleId,
            ),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const LoadingIndicator();
              } else if (snapshot.hasError) {
                return Text('Error : ${snapshot.error}');
              } else {
                if (inspectionCertificateProvider.surveyCertificate.isEmpty) {
                  return Center(
                    child: Text(
                      "Menu Belum tersedia",
                      style: regularPoppins,
                    ),
                  );
                }
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 0,
                      ),
                      child: ListTile(
                        leading: const Icon(
                          Icons.radio_button_checked_outlined,
                          color: primaryColor,
                        ),
                        title: Text(
                          widget.titleMenu,
                          style: semiboldPoppins,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          bottom: 20,
                          left: 20,
                          right: 20,
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              ...inspectionCertificateProvider.surveyCertificate
                                  .asMap()
                                  .entries
                                  .map(
                                (entry) {
                                  var data = entry.value;
                                  var index = entry.key;

                                  listFilesPdf[index] = data.filePath!;
                                  listFilesImage[index] = data.imagesPath!;
                                  listFilesAudio[index] = data.voiceNotePath!;

                                  return Column(
                                    children: [
                                      ExpansionTile(
                                        maintainState: true,
                                        shape: RoundedRectangleBorder(
                                          side: BorderSide(
                                            color:
                                                secondaryColor.withOpacity(0.6),
                                            width: 2,
                                          ),
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        collapsedShape:
                                            const RoundedRectangleBorder(
                                          side: BorderSide(
                                            color: greyColor,
                                          ),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        collapsedIconColor: primaryColor,
                                        collapsedTextColor: primaryColor,
                                        iconColor: greyColor,
                                        textColor: secondaryColor,
                                        tilePadding: const EdgeInsets.symmetric(
                                          horizontal: 10,
                                        ),
                                        title: Text(
                                          data.certificateName,
                                          style: const TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        children: [
                                          Divider(
                                            color:
                                                secondaryColor.withOpacity(0.6),
                                          ),
                                          CustomInputField(
                                            controller:
                                                certificateControllers[index],
                                            label: "No. Sertifikat",
                                            data: data.certificateNumber ??
                                                "Data Kosong",
                                            isRating: false,
                                          ),
                                          CustomInputField(
                                            controller:
                                                expiryDateControllers[index],
                                            label: "Masa Berlaku",
                                            data: data.expiryDate ??
                                                "Data Kosong",
                                            isRating: false,
                                            isDate: true,
                                          ),
                                          CustomInputField(
                                            controller:
                                                findingControllers[index],
                                            label: "Temuan",
                                            data: data.finding ?? "Data Kosong",
                                            isRating: false,
                                          ),
                                          RatingWidget(
                                            idCategoryGrading: data.gradeCatL,
                                            currentRating: data.gradeL,
                                            onChanged: (value) {
                                              gradeControllers[index].text =
                                                  value.toString();

                                              findingControllers[index].text =
                                                  dashboardProvider
                                                      .grading.description;
                                            },
                                          ),
                                          CustomInputField(
                                            controller:
                                                remarkControllers[index],
                                            label: "Keterangan",
                                            data: data.remark ?? "Data Kosong",
                                            isRating: false,
                                          ),
                                          const Divider(),
                                          MediaAttachmentPDF(
                                            itemId: data.id,
                                            totalCurrentFile:
                                                listFilesPdf[index]!.length,
                                            currentFile: data.filePath,
                                            titleId: widget.titleId,
                                            formCode: widget.formCode,
                                            onMediaSelectedForDeleted:
                                                (pdfsDeleted) {
                                              if (pdfsDeleted!.isNotEmpty) {
                                                if (listFilesPdf[index] !=
                                                    null) {
                                                  listFilesPdf[index]!
                                                      .remove(pdfsDeleted[0]);
                                                } else {
                                                  listFilesPdf[index] =
                                                      pdfsDeleted;
                                                }
                                              }
                                            },
                                            onMediaSelected: (pdfs) {
                                              if (pdfs!.isNotEmpty) {
                                                if (listFilesPdf[index] !=
                                                    null) {
                                                  listFilesPdf[index]!
                                                      .addAll(pdfs);
                                                } else {
                                                  listFilesPdf[index] = pdfs;
                                                }
                                              }
                                            },
                                          ),
                                          MediaAttachmentImage(
                                            totalCurrentImage:
                                                listFilesImage[index]!.length,
                                            currentImage: data.imagesPath!,
                                            formCode: widget.formCode,
                                            titleId: widget.titleId,
                                            onMediaSelected: (images) {
                                              if (listFilesImage[index] !=
                                                  null) {
                                                listFilesImage[index]!
                                                    .addAll(images!);
                                              } else {
                                                listFilesImage[index] = images!;
                                              }
                                            },
                                          ),
                                          MediaAttachmentVoiceNote(
                                            isRecorderReady: isRecorderReady,
                                            currentAudio: data.voiceNotePath!,
                                            formCode: widget.formCode,
                                            titleId: widget.titleId,
                                            onMediaSelected: (vn) {
                                              if (listFilesAudio[index] !=
                                                  null) {
                                                listFilesAudio[index]!
                                                    .addAll(vn!);
                                              } else {
                                                listFilesAudio[index] = vn!;
                                              }
                                            },
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  );
                                },
                              ),
                              const SizedBox(
                                height: 50,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              }
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10,
              ),
              // height: 80,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(8),
                ),
                color: whiteColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 10,
                    offset: const Offset(0, -5),
                  ),
                ],
              ),
              child: isLoading
                  ? const LoadingIndicator()
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonCustom(
                          backgroundColor: whiteColor,
                          label: "Cancel",
                          outlineBorderColor: redColor,
                          textColor: redColor,
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                        ),
                        ButtonCustom(
                          backgroundColor: primaryColor,
                          label: "Save",
                          outlineBorderColor: primaryColor,
                          textColor: whiteColor,
                          onPressed: () async {
                            var isSuccess = await handleUpdateShipLegalites();
                            // ignore: use_build_context_synchronously
                            SnackbarCustom.alertMessage(
                              context,
                              messageAlert,
                              2,
                              isSuccess,
                            );

                            Future.delayed(
                              const Duration(seconds: 1),
                              () {
                                Navigator.pop(context, isSuccess);
                              },
                            );
                          },
                        ),
                      ],
                    ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomInputField extends StatefulWidget {
  final String label;
  final String data;
  final bool isRating;
  final TextEditingController? controller;
  final bool? isDate;

  const CustomInputField({
    super.key,
    required this.label,
    required this.data,
    required this.isRating,
    this.controller,
    this.isDate,
  });

  @override
  State<CustomInputField> createState() => _CustomInputFieldState();
}

class _CustomInputFieldState extends State<CustomInputField> {
  List<bool> isFilled = List.generate(4, (index) => false);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Container(
            constraints: const BoxConstraints(
              minHeight: 50,
              minWidth: 200,
              maxHeight: 80,
              maxWidth: 200,
            ),
            child: TextField(
              minLines: 2,
              maxLines: 5,
              controller: widget.controller,
              style: regularPoppins.copyWith(
                fontSize: 14,
              ),
              cursorColor: primaryColor,
              keyboardType: TextInputType.multiline,
              autofocus: false,
              readOnly: widget.isDate != null ? true : false,
              onTap: () async {
                if (widget.isDate != null) {
                  DateTime? pickedDate = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(
                      2000,
                    ),
                    lastDate: DateTime(2101),
                    builder: (context, child) {
                      return Theme(
                        data: Theme.of(context).copyWith(
                          colorScheme: const ColorScheme.light(
                            primary: secondaryColor,
                            onPrimary: whiteColor,
                            onSurface: primaryColor,
                          ),
                        ),
                        child: child!,
                      );
                    },
                  );

                  if (pickedDate != null) {
                    var onlyDate = pickedDate.toString().split(" ");
                    print("debug $onlyDate");
                    setState(() {
                      widget.controller!.text = onlyDate[0];
                    });
                  } else {
                    print("Date is not selected");
                  }
                }
              },
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(
                  10.0,
                ),
                labelStyle: regularPoppins,
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                ),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class RatingWidget extends StatefulWidget {
  final ValueChanged<int> onChanged;
  final int currentRating;
  final int idCategoryGrading;

  const RatingWidget({
    Key? key,
    required this.onChanged,
    required this.currentRating,
    required this.idCategoryGrading,
  }) : super(key: key);

  @override
  RatingWidgetState createState() => RatingWidgetState();
}

class RatingWidgetState extends State<RatingWidget> {
  late List<bool> isFilled;

  @override
  void initState() {
    super.initState();
    isFilled = List.generate(4, (index) => false);
    for (int i = 0; i < widget.currentRating; i++) {
      isFilled[i] = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);

    Future<bool> getGrading(int grade, String idCategoryGrading) async {
      var isSuccess = await dashboardProvider.getGrading(
          grade: grade.toString(), idCategoryGrading: idCategoryGrading);
      if (isSuccess) {
        return isSuccess;
      } else {
        return false;
      }
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Rating",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Row(
            children: List.generate(4, (index) {
              return InkWell(
                onTap: () async {
                  setState(() {
                    for (int i = 0; i <= index; i++) {
                      isFilled[i] = true;
                    }
                    for (int i = index + 1; i < 4; i++) {
                      isFilled[i] = false;
                    }
                  });
                  // Mengirim nilai rating ke parent widget
                  await getGrading(
                      index + 1, widget.idCategoryGrading.toString());
                  widget.onChanged(index + 1);
                },
                child: Icon(
                  Icons.star,
                  size: 45,
                  color:
                      isFilled[index] ? thirdColor : greyColor.withOpacity(0.6),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
}

class MediaAttachmentVoiceNote extends StatefulWidget {
  final Function(List<String>?) onMediaSelected;
  final String titleId;
  final String formCode;
  final bool isRecorderReady;
  final List<String>? currentAudio;

  const MediaAttachmentVoiceNote(
      {super.key,
      required this.onMediaSelected,
      required this.titleId,
      required this.formCode,
      required this.currentAudio,
      required this.isRecorderReady});

  @override
  State<MediaAttachmentVoiceNote> createState() =>
      _MediaAttachmentVoiceNoteState();
}

class _MediaAttachmentVoiceNoteState extends State<MediaAttachmentVoiceNote> {
  final recorder = FlutterSoundRecorder();
  late File audioFile;
  late AudioPlayer audioPlayer;
  bool isRecorderReady = false;
  bool isLoading = false;
  List<String> pathAudio = [];
  List<String>? pathAudiotemp = [];

  @override
  void initState() {
    initRecorder();
    super.initState();
    pathAudiotemp = widget.currentAudio;
  }

  @override
  void dispose() {
    recorder.closeRecorder();
    super.dispose();
  }

  Future initRecorder() async {
    if (widget.isRecorderReady) {
      await recorder.openRecorder();
      recorder.setSubscriptionDuration(const Duration(milliseconds: 500));
    }
  }

  Future startRecording() async {
    if (!widget.isRecorderReady) return !widget.isRecorderReady;
    await recorder.startRecorder(toFile: 'audio.aac');
  }

  Future<bool> stopRecording() async {
    if (!widget.isRecorderReady) return !widget.isRecorderReady;
    final pathRecorder = await recorder.stopRecorder();
    audioFile = File(pathRecorder!);
    // play(audioFile.path);

    return true;
  }

  Future play(String path) async {
    audioPlayer = AudioPlayer();
    await audioPlayer.play(UrlSource(path));
  }

  @override
  Widget build(BuildContext context) {
    final MediaProvider mediaProvider =
        Provider.of<MediaProvider>(context, listen: false);

    Future<bool> uploadVoiceNote() async {
      setState(() {
        isLoading = true;
      });
      var isSuccess = await mediaProvider.uploadFileAudio(
        audio: audioFile,
        formCode: widget.formCode,
        name: "audio-${widget.formCode}",
        titleId: widget.titleId,
      );

      if (isSuccess) {
        setState(() {
          isLoading = false;
        });
      } else {
        setState(() {
          isLoading = false;
        });
      }
      return isSuccess;
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Attachment Voice Note:",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          isLoading
              ? const LoadingIndicator()
              : GestureDetector(
                  // record voice note
                  onTap: () async {
                    if (recorder.isRecording) {
                      var isStopped = await stopRecording();
                      if (isStopped) {
                        var isSuccess = await uploadVoiceNote();
                        if (isSuccess) {
                          // ignore: use_build_context_synchronously
                          SnackbarCustom.alertMessage(
                            context,
                            "Sukses Uploaded Audio",
                            4,
                            isSuccess,
                          );

                          pathAudio.add(mediaProvider.voiceNote);
                          widget.onMediaSelected(pathAudio);
                        } else {
                          // ignore: use_build_context_synchronously
                          SnackbarCustom.alertMessage(
                            context,
                            mediaProvider.errorMessage,
                            4,
                            isSuccess,
                          );
                        }
                      }
                    } else {
                      await startRecording();
                    }
                    setState(() {});
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 14),
                    child: Container(
                      padding: const EdgeInsets.all(8),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: whiteColor,
                        border: Border.all(
                          color: primaryColor.withOpacity(0.7),
                          width: 2,
                        ),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(8),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            recorder.isRecording
                                ? Icons.stop
                                : Icons.mic_none_sharp,
                            size: 35,
                            color: primaryColor,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          recorder.isRecording
                              ? StreamBuilder<RecordingDisposition>(
                                  stream: recorder.onProgress,
                                  builder: (context, snapshot) {
                                    final duration = snapshot.hasData
                                        ? snapshot.data!.duration
                                        : Duration.zero;
                                    final minutes = duration.inMinutes
                                        .remainder(60)
                                        .toString()
                                        .padLeft(2, '0');
                                    final seconds = duration.inSeconds
                                        .remainder(60)
                                        .toString()
                                        .padLeft(2, '0');
                                    return Text(
                                      '$minutes:$seconds',
                                      style: regularPoppins,
                                    );
                                  },
                                )
                              : Text(
                                  "Record",
                                  style: regularPoppins.copyWith(
                                    color: primaryColor,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                        ],
                      ),
                    ),
                  ),
                ),
          const SizedBox(
            height: 10,
          ),
          Visibility(
            visible: pathAudio.isNotEmpty,
            child: InkWell(
              onTap: () {
                showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return ListAudioPreview(
                      formCode: widget.formCode,
                      audioPath: pathAudio,
                    );
                  },
                );
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 14.0),
                padding: const EdgeInsets.all(8),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: secondaryColor,
                  border: Border.all(
                    color: primaryColor.withOpacity(0.7),
                    width: 2,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "Total Audio Yang terekam (${pathAudio.length})",
                      style: regularPoppins.copyWith(
                        color: whiteColor,
                        fontSize: 14,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
