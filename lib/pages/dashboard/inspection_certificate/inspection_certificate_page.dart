import 'package:condition_survey/models/dashboard/survey_menu_model.dart';
import 'package:condition_survey/models/dashboard/survey_model.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/manual_books/detail_manual_book_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/manual_books/edit_manual_book_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/ship_legalities/detail_ship_legalities_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/ship_legalities/edit_ship_legalities_page.dart';
import 'package:condition_survey/pages/dashboard/detail_survey_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/survey_status/detail_classification_survey_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/survey_status/detail_statutory_survey_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/survey_status/edit_classification_survey_page.dart';
import 'package:condition_survey/pages/dashboard/inspection_certificate/survey_status/edit_statutory_survey_page.dart';
import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:condition_survey/widgets/not_found_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailInspectionCertificate extends StatefulWidget {
  final String titleMenu;
  final String titleId;
  final String titleAPI;

  const DetailInspectionCertificate({
    required this.titleMenu,
    required this.titleId,
    required this.titleAPI,
    super.key,
  });

  @override
  State<DetailInspectionCertificate> createState() =>
      _DetailInspectionCertificateState();
}

class _DetailInspectionCertificateState
    extends State<DetailInspectionCertificate> {
  late String subMenu;
  late List<bool> isFilled;
  bool isFormCodeInitialized = false;
  bool isButtonFABShow = true;

  @override
  void initState() {
    super.initState();
    getMenu().then((value) {
      setState(() {
        isFormCodeInitialized = true;
      });
    });
    isFilled = List.generate(4, (index) => false);
  }

  Future<String> getMenu() async {
    String firstFormCode = '';
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    await inspectionCertificateProvider.getSurveySubMenu(
        formCode: widget.titleAPI);
    await inspectionCertificateProvider.getSubMenuActive();

    final List<SurveyMenu> listMenus =
        inspectionCertificateProvider.surveySubMenu;
    if (inspectionCertificateProvider.subMenuActive.isEmpty) {
      for (var surveyMenu in listMenus) {
        inspectionCertificateProvider.subMenuActive = surveyMenu.formCode!;
        inspectionCertificateProvider.titleSubMenu = surveyMenu.title;
        break;
      }
    }
    return firstFormCode;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    ListSurvey detailSurvey = inspectionCertificateProvider.detailSurvey;
    return Scaffold(
      endDrawer: isFormCodeInitialized
          ? Drawer(
              child: Column(
                children: [
                  HeaderDrawer(detailSurvey: detailSurvey),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Menu",
                    style: semiboldPoppins,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  const Divider(),
                  FutureBuilder(
                    future: inspectionCertificateProvider.getSurveySubMenu(
                      formCode: widget.titleAPI,
                    ),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const LoadingIndicator();
                      } else if (snapshot.hasError) {
                        return Text('Error : ${snapshot.error}');
                      } else {
                        return Expanded(
                          child: ListView.builder(
                            padding: const EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemCount: inspectionCertificateProvider
                                .surveySubMenu.length,
                            itemBuilder: (context, index) {
                              final subMenu = inspectionCertificateProvider
                                  .surveySubMenu[index];
                              final totalSubMenu = subMenu.subMenu.isNotEmpty;
                              return totalSubMenu
                                  ? Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        ListTile(
                                          onTap: () {},
                                          dense: true,
                                          leading: Icon(
                                            inspectionCertificateProvider
                                                        .subMenuActive ==
                                                    subMenu.subMenu[0].formCode
                                                ? Icons
                                                    .radio_button_checked_outlined
                                                : inspectionCertificateProvider
                                                            .subMenuActive ==
                                                        subMenu
                                                            .subMenu[1].formCode
                                                    ? Icons
                                                        .radio_button_checked_outlined
                                                    : Icons
                                                        .radio_button_unchecked_outlined,
                                            color: inspectionCertificateProvider
                                                        .subMenuActive ==
                                                    subMenu.subMenu[0].formCode
                                                ? secondaryColor
                                                : inspectionCertificateProvider
                                                            .subMenuActive ==
                                                        subMenu
                                                            .subMenu[1].formCode
                                                    ? secondaryColor
                                                    : primaryColor
                                                        .withOpacity(0.6),
                                          ),
                                          title: Text(
                                            subMenu.title.capitalize(),
                                            style: regularPoppins.copyWith(
                                              fontSize: 14,
                                              color: inspectionCertificateProvider
                                                          .subMenuActive ==
                                                      subMenu
                                                          .subMenu[0].formCode
                                                  ? primaryColor
                                                  : inspectionCertificateProvider
                                                              .subMenuActive ==
                                                          subMenu.subMenu[0]
                                                              .formCode
                                                      ? primaryColor
                                                      : primaryColor
                                                          .withOpacity(0.6),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 40.0,
                                            bottom: 0,
                                            right: 0,
                                            top: 0,
                                          ),
                                          child: ListTile(
                                            onTap: () {
                                              setState(() {
                                                inspectionCertificateProvider
                                                        .subMenuActive =
                                                    subMenu
                                                        .subMenu[0].formCode!;
                                                inspectionCertificateProvider
                                                        .titleSubMenu =
                                                    subMenu.subMenu[0].title;
                                              });
                                              Navigator.pop(context);
                                            },
                                            dense: true,
                                            leading: Icon(
                                              inspectionCertificateProvider
                                                          .subMenuActive ==
                                                      subMenu
                                                          .subMenu[0].formCode
                                                  ? Icons
                                                      .radio_button_checked_outlined
                                                  : Icons
                                                      .radio_button_unchecked_outlined,
                                              color:
                                                  inspectionCertificateProvider
                                                              .subMenuActive ==
                                                          subMenu.subMenu[0]
                                                              .formCode
                                                      ? secondaryColor
                                                      : primaryColor
                                                          .withOpacity(0.6),
                                            ),
                                            title: Text(
                                              subMenu.subMenu[0].title
                                                  .capitalize(),
                                              style: regularPoppins.copyWith(
                                                fontSize: 14,
                                                color:
                                                    inspectionCertificateProvider
                                                                .subMenuActive ==
                                                            subMenu.subMenu[0]
                                                                .formCode
                                                        ? primaryColor
                                                        : primaryColor
                                                            .withOpacity(0.6),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 40.0,
                                            bottom: 0,
                                            right: 0,
                                            top: 0,
                                          ),
                                          child: ListTile(
                                            onTap: () {
                                              setState(() {
                                                inspectionCertificateProvider
                                                        .subMenuActive =
                                                    subMenu
                                                        .subMenu[1].formCode!;
                                                inspectionCertificateProvider
                                                        .titleSubMenu =
                                                    subMenu.subMenu[1].title;
                                              });
                                              Navigator.pop(context);
                                            },
                                            dense: true,
                                            leading: Icon(
                                              inspectionCertificateProvider
                                                          .subMenuActive ==
                                                      subMenu
                                                          .subMenu[1].formCode
                                                  ? Icons
                                                      .radio_button_checked_outlined
                                                  : Icons
                                                      .radio_button_unchecked_outlined,
                                              color:
                                                  inspectionCertificateProvider
                                                              .subMenuActive ==
                                                          subMenu.subMenu[1]
                                                              .formCode
                                                      ? secondaryColor
                                                      : primaryColor
                                                          .withOpacity(0.6),
                                            ),
                                            title: Text(
                                              subMenu.subMenu[1].title
                                                  .capitalize(),
                                              style: regularPoppins.copyWith(
                                                fontSize: 14,
                                                color:
                                                    inspectionCertificateProvider
                                                                .subMenuActive ==
                                                            subMenu.subMenu[1]
                                                                .formCode
                                                        ? primaryColor
                                                        : primaryColor
                                                            .withOpacity(0.6),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : ListTile(
                                      onTap: () {
                                        setState(() {
                                          inspectionCertificateProvider
                                                  .subMenuActive =
                                              subMenu.formCode!;
                                          inspectionCertificateProvider
                                              .titleSubMenu = subMenu.title;
                                        });
                                        Navigator.pop(context);
                                      },
                                      dense: true,
                                      leading: Icon(
                                        inspectionCertificateProvider
                                                    .subMenuActive ==
                                                subMenu.formCode
                                            ? Icons
                                                .radio_button_checked_outlined
                                            : Icons
                                                .radio_button_unchecked_outlined,
                                        color: inspectionCertificateProvider
                                                    .subMenuActive ==
                                                subMenu.formCode
                                            ? secondaryColor
                                            : primaryColor.withOpacity(0.6),
                                      ),
                                      title: Text(
                                        subMenu.title.capitalize(),
                                        style: regularPoppins.copyWith(
                                          fontSize: 14,
                                          color: inspectionCertificateProvider
                                                      .subMenuActive ==
                                                  subMenu.formCode
                                              ? primaryColor
                                              : primaryColor.withOpacity(0.6),
                                        ),
                                      ),
                                    );
                            },
                          ),
                        );
                      }
                    },
                  ),
                ],
              ),
            )
          : const LoadingIndicator(),
      floatingActionButton: !isFormCodeInitialized
          ? Container()
          : _buildFloatingButtonEditWidget(inspectionCertificateProvider),
      backgroundColor: whiteColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.titleMenu.toUpperCase(),
          style: semiboldPoppins,
        ),
        backgroundColor: whiteColor,
        foregroundColor: primaryColor,
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(
              context,
              MaterialPageRoute(
                builder: (context) => DetailSurveyPage(
                  surveyTitle: inspectionCertificateProvider.detailSurvey.title,
                  surveyTitleId: widget.titleId,
                ),
              ),
            );
          },
          icon: const Icon(
            Icons.arrow_back_rounded,
          ),
        ),
      ),
      body: isFormCodeInitialized
          ? FutureBuilder(
              future: inspectionCertificateProvider
                  .getItemBySubMenuInspectionCertifcate(
                titleApi: inspectionCertificateProvider.subMenuActive,
                titleId: widget.titleId,
              ),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const LoadingIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error : ${snapshot.error}');
                } else {
                  if (inspectionCertificateProvider.initializeMenu.isEmpty) {
                    return Center(
                      child: Text(
                        "Menu Belum tersedia",
                        style: regularPoppins,
                      ),
                    );
                  }
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 20,
                        ),
                        child: Center(
                          child: Text(
                            inspectionCertificateProvider.detailSurvey.title,
                            style: semiboldPoppins,
                          ),
                        ),
                      ),
                      _buildSubMenuWidget(inspectionCertificateProvider)
                    ],
                  );
                }
              },
            )
          : const LoadingIndicator(),
    );
  }

  Widget _buildSubMenuWidget(
      InspectionCertificateProvider inspectionCertificateProvider) {
    switch (inspectionCertificateProvider.subMenuActive) {
      case shipLegalities:
        return const DetailShipLegalitiesPage();
      case manualBooks:
        return const DetailManualBookPage();
      case statusStatutorySurvey:
        return const DetailStatusStatutorySurvey();
      case statusClassificationSurvey:
        return const DetailStatusClassificationSurvey();
      // tambahkan kondisi untuk submenu lainnya di sini
      default:
        return Center(
          heightFactor: MediaQuery.of(context).size.height / 200,
          child: const NotFound(),
        );
    }
  }

  Widget _buildFloatingButtonEditWidget(
      InspectionCertificateProvider inspectionCertificateProvider) {
    ListSurvey detailSurvey = inspectionCertificateProvider.detailSurvey;
    switch (inspectionCertificateProvider.subMenuActive) {
      case shipLegalities:
        return FABEditPage(
          onPressed: () async {
            bool status = await Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: ((context) => EditShipLegalitiesPage(
                          titleMenu: inspectionCertificateProvider.titleSubMenu,
                          titleId: widget.titleId,
                          titleShip: detailSurvey.title,
                          formCode: inspectionCertificateProvider.subMenuActive,
                        )),
                  ),
                ) ??
                false;

            if (status) {
              inspectionCertificateProvider
                  .getItemBySubMenuInspectionCertifcate(
                titleApi: inspectionCertificateProvider.subMenuActive,
                titleId: widget.titleId,
              );
              setState(() {});
            }
          },
        );
      case manualBooks:
        return FABEditPage(
          onPressed: () async {
            bool status = await Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: ((context) => EditManualBookPage(
                          titleMenu: inspectionCertificateProvider.titleSubMenu,
                          titleId: widget.titleId,
                          titleShip: detailSurvey.title,
                          formCode: inspectionCertificateProvider.subMenuActive,
                        )),
                  ),
                ) ??
                false;

            if (status) {
              inspectionCertificateProvider
                  .getItemBySubMenuInspectionCertifcate(
                titleApi: inspectionCertificateProvider.subMenuActive,
                titleId: widget.titleId,
              );
              setState(() {});
            }
          },
        );
      case statusClassificationSurvey:
        return FABEditPage(
          onPressed: () async {
            bool status = await Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: ((context) => EditClassificationSurveyPage(
                          titleMenu: inspectionCertificateProvider.titleSubMenu,
                          titleId: widget.titleId,
                          titleShip: detailSurvey.title,
                          formCode: inspectionCertificateProvider.subMenuActive,
                        )),
                  ),
                ) ??
                false;

            if (status) {
              inspectionCertificateProvider
                  .getItemBySubMenuInspectionCertifcate(
                titleApi: inspectionCertificateProvider.subMenuActive,
                titleId: widget.titleId,
              );
              setState(() {});
            }
          },
        );
      case statusStatutorySurvey:
        return FABEditPage(
          onPressed: () async {
            bool status = await Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: ((context) => EditStatutorySurveyPage(
                          titleMenu: inspectionCertificateProvider.titleSubMenu,
                          titleId: widget.titleId,
                          titleShip: detailSurvey.title,
                          formCode: inspectionCertificateProvider.subMenuActive,
                        )),
                  ),
                ) ??
                false;

            if (status) {
              inspectionCertificateProvider
                  .getItemBySubMenuInspectionCertifcate(
                titleApi: inspectionCertificateProvider.subMenuActive,
                titleId: widget.titleId,
              );
              setState(() {});
            }
          },
        );
      default:
        return Container();
    }
  }
}

class FABEditPage extends StatelessWidget {
  final VoidCallback onPressed;
  const FABEditPage({super.key, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onPressed,
      elevation: 4.0,
      backgroundColor: primaryColor,
      child: const Icon(
        Icons.edit,
        color: whiteColor,
      ),
    );
  }
}

class ItemDataWidget extends StatelessWidget {
  final String label;
  final String? data;

  const ItemDataWidget({
    super.key,
    required this.label,
    required this.data,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            width: 100,
          ),
          Flexible(
            child: Text(
              data ?? "-",
              style: lightPoppins.copyWith(
                fontSize: 14,
                color: primaryColor,
              ),
              textAlign: TextAlign.right,
              maxLines: 3,
            ),
          ),
        ],
      ),
    );
  }
}

class HeaderDrawer extends StatelessWidget {
  const HeaderDrawer({
    super.key,
    required this.detailSurvey,
  });

  final ListSurvey detailSurvey;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: primaryColor,
      child: Container(
        margin: const EdgeInsets.only(
          top: 30,
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    detailSurvey.title.toUpperCase(),
                    style: mediumPoppins.copyWith(
                      color: whiteColor,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2.45,
                    child: Text(
                      detailSurvey.location,
                      style: lightPoppins.copyWith(
                        color: whiteColor,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2.45,
                    child: Text(
                      detailSurvey.date!,
                      style: lightPoppins.copyWith(
                        color: whiteColor,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Container(
                        height: 30,
                        width: MediaQuery.of(context).size.width / 4,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                          color: _statusColor(detailSurvey.status),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            _status(detailSurvey.status),
                            style: detailSurvey.status == 1
                                ? mediumPoppins.copyWith(
                                    fontSize: 14,
                                    color: whiteColor,
                                  )
                                : mediumPoppins.copyWith(fontSize: 14),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            SizedBox(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    "${pathImages}ship.png",
                    width: 70,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

String _status(int status) {
  switch (status) {
    case 0:
      return todo;
    case 2:
      return inProgress;
    case 1:
      return done;
    default:
      return 'Unknown';
  }
}

Color _statusColor(int status) {
  switch (status) {
    case 0:
      return backgroundColor1;
    case 2:
      return thirdColor;
    case 1:
      return secondaryColor;
    default:
      return backgroundColor1;
  }
}
