// ignore_for_file: avoid_print

import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/button_custom_widget.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EditClassificationSurveyPage extends StatefulWidget {
  final String titleId;
  final String titleShip;
  final String titleMenu;
  final String formCode;

  const EditClassificationSurveyPage({
    super.key,
    required this.titleId,
    required this.titleShip,
    required this.titleMenu,
    required this.formCode,
  });

  @override
  State<EditClassificationSurveyPage> createState() =>
      _EditClassificationSurveyPageState();
}

class _EditClassificationSurveyPageState
    extends State<EditClassificationSurveyPage> {
  List<TextEditingController> lastDateControllers = [];
  List<TextEditingController> next1DateControllers = [];
  List<TextEditingController> next2DateControllers = [];
  bool isLoading = false;
  String messageAlert = "";

  @override
  void initState() {
    super.initState();
    // Inisialisasi controller sesuai jumlah data yang ada
    final inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    lastDateControllers = List.generate(
      inspectionCertificateProvider.surveyStatus.length,
      (index) => TextEditingController(),
    );

    next1DateControllers = List.generate(
      inspectionCertificateProvider.surveyStatus.length,
      (index) => TextEditingController(),
    );

    next2DateControllers = List.generate(
      inspectionCertificateProvider.surveyStatus.length,
      (index) => TextEditingController(),
    );
  }

  @override
  void dispose() {
    if (lastDateControllers.isNotEmpty) {
      // Dispose controller
      for (int i = 0; i < lastDateControllers.length; i++) {
        lastDateControllers[i].dispose();
        next1DateControllers[i].dispose();
        next2DateControllers[i].dispose();
      }
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    Future<bool> handleUpdateClassificationSurvey() async {
      setState(() {
        isLoading = true;
      });
      List<Map<String, dynamic>> itemsList = [];
      for (var i = 0;
          i < inspectionCertificateProvider.surveyStatus.length;
          i++) {
        Map<String, dynamic> itemMap = {
          'id': inspectionCertificateProvider.surveyStatus[i].id,
          'certificate_type':
              inspectionCertificateProvider.surveyStatus[i].certificateType,
          'last_date': lastDateControllers[i].text,
          'next_1_date': next1DateControllers[i].text,
          'next_2_date': next2DateControllers[i].text,
        };

        itemsList.add(itemMap);
      }

      Map<String, dynamic> requestBody = {'items': itemsList};
      var isSuccess =
          await inspectionCertificateProvider.updateSurveyStatusItem(
        bodyRequest: requestBody,
        titleApi: widget.formCode,
        titleId: widget.titleId,
      );

      if (isSuccess) {
        messageAlert = successMessage;
      } else {
        setState(() {
          isLoading = false;
        });
        messageAlert = failedMessage;
      }

      return isSuccess;
    }

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.titleShip,
          style: semiboldPoppins,
        ),
        backgroundColor: whiteColor,
        foregroundColor: primaryColor,
      ),
      body: Stack(
        children: [
          FutureBuilder(
            future: inspectionCertificateProvider.getSurveyStatusItem(
              titleApi: widget.formCode,
              titleId: widget.titleId,
            ),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const LoadingIndicator();
              } else if (snapshot.hasError) {
                return Text('Error : ${snapshot.error}');
              } else {
                if (inspectionCertificateProvider.surveyStatus.isEmpty) {
                  return Center(
                    child: Text(
                      "Menu Belum tersedia",
                      style: regularPoppins,
                    ),
                  );
                }
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TitleMenu(widget: widget),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          bottom: 20,
                          left: 20,
                          right: 20,
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              ...inspectionCertificateProvider.surveyStatus
                                  .asMap()
                                  .entries
                                  .map(
                                (entry) {
                                  var data = entry.value;
                                  var index = entry.key;

                                  lastDateControllers[index].text =
                                      data.lastDate!;

                                  next1DateControllers[index].text =
                                      data.next1Date!;

                                  next2DateControllers[index].text =
                                      data.next2Date!;

                                  return Column(
                                    children: [
                                      ExpansionTile(
                                        shape: RoundedRectangleBorder(
                                          side: BorderSide(
                                            color:
                                                secondaryColor.withOpacity(0.6),
                                            width: 2,
                                          ),
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        collapsedShape:
                                            const RoundedRectangleBorder(
                                          side: BorderSide(
                                            color: greyColor,
                                          ),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        collapsedIconColor: primaryColor,
                                        collapsedTextColor: primaryColor,
                                        iconColor: greyColor,
                                        textColor: secondaryColor,
                                        tilePadding: const EdgeInsets.symmetric(
                                          horizontal: 10,
                                        ),
                                        title: Text(
                                          data.certificateType,
                                          style: const TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        children: [
                                          Divider(
                                            color:
                                                secondaryColor.withOpacity(0.6),
                                          ),
                                          DateWidget(
                                            label: "Last Date",
                                            textControllers:
                                                lastDateControllers,
                                            index: index,
                                          ),
                                          DateWidget(
                                            label: "Next 1 Date",
                                            textControllers:
                                                next1DateControllers,
                                            index: index,
                                          ),
                                          DateWidget(
                                            label: "Next 2 Date",
                                            textControllers:
                                                next2DateControllers,
                                            index: index,
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  );
                                },
                              ),
                              const SizedBox(
                                height: 50,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              }
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10,
              ),
              // height: 80,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(8),
                ),
                color: whiteColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 10,
                    offset: const Offset(0, -5),
                  ),
                ],
              ),
              child: isLoading
                  ? const LoadingIndicator()
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonCustom(
                          backgroundColor: whiteColor,
                          label: "Cancel",
                          outlineBorderColor: redColor,
                          textColor: redColor,
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                        ),
                        ButtonCustom(
                          backgroundColor: primaryColor,
                          label: "Save",
                          outlineBorderColor: primaryColor,
                          textColor: whiteColor,
                          onPressed: () async {
                            var isSuccess =
                                await handleUpdateClassificationSurvey();
                            // ignore: use_build_context_synchronously
                            SnackbarCustom.alertMessage(
                              context,
                              messageAlert,
                              2,
                              isSuccess,
                            );

                            Future.delayed(
                              const Duration(seconds: 1),
                              () {
                                Navigator.pop(context, isSuccess);
                              },
                            );
                          },
                        ),
                      ],
                    ),
            ),
          ),
        ],
      ),
    );
  }
}

class DateWidget extends StatelessWidget {
  const DateWidget({
    super.key,
    required this.textControllers,
    required this.index,
    required this.label,
  });

  final List<TextEditingController> textControllers;
  final int index;
  final String label;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          Container(
            constraints: const BoxConstraints(
              minHeight: 50,
              minWidth: 200,
              maxHeight: 80,
              maxWidth: 200,
            ),
            child: TextField(
              minLines: 2,
              maxLines: 5,
              controller: textControllers[index],
              style: regularPoppins.copyWith(
                fontSize: 14,
              ),
              cursorColor: primaryColor,
              keyboardType: TextInputType.multiline,
              autofocus: false,
              readOnly: true,
              onTap: () async {
                DateTime? pickedDate = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(
                    2000,
                  ),
                  lastDate: DateTime(2101),
                  builder: (context, child) {
                    return Theme(
                      data: Theme.of(context).copyWith(
                        colorScheme: const ColorScheme.light(
                          primary: secondaryColor,
                          onPrimary: whiteColor,
                          onSurface: primaryColor,
                        ),
                      ),
                      child: child!,
                    );
                  },
                );

                if (pickedDate != null) {
                  DateFormat dateFormat = DateFormat('yyyy/MM/dd');
                  String formattedDate = dateFormat.format(pickedDate);
                  // setState(() {
                  textControllers[index].text = formattedDate;
                  // });
                } else {
                  print("Date is not selected");
                }
              },
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(
                  10.0,
                ),
                labelStyle: regularPoppins,
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                ),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class TitleMenu extends StatelessWidget {
  const TitleMenu({
    super.key,
    required this.widget,
  });

  final EditClassificationSurveyPage widget;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 0,
      ),
      child: ListTile(
        leading: const Icon(
          Icons.radio_button_checked_outlined,
          color: primaryColor,
        ),
        title: Text(
          widget.titleMenu,
          style: semiboldPoppins,
        ),
      ),
    );
  }
}
