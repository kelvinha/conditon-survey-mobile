import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailStatusClassificationSurvey extends StatefulWidget {
  const DetailStatusClassificationSurvey({super.key});

  @override
  State<DetailStatusClassificationSurvey> createState() =>
      DetailStatusClassificationyState();
}

class DetailStatusClassificationyState
    extends State<DetailStatusClassificationSurvey> {
  @override
  Widget build(BuildContext context) {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(
          bottom: 20,
          left: 20,
          right: 20,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ...inspectionCertificateProvider.surveyStatus.map(
                (data) {
                  return Column(
                    children: [
                      ExpansionTile(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                            color: secondaryColor.withOpacity(0.6),
                            width: 2,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        collapsedShape: const RoundedRectangleBorder(
                          side: BorderSide(
                            color: greyColor,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        collapsedIconColor: primaryColor,
                        collapsedTextColor: primaryColor,
                        iconColor: greyColor,
                        textColor: secondaryColor,
                        tilePadding: const EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        title: Text(
                          data.certificateType,
                          style: const TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        children: [
                          Divider(
                            color: secondaryColor.withOpacity(0.6),
                          ),
                          ItemWidget(
                            title: "Last Date",
                            value: data.lastDate != null
                                ? data.lastDate.toString()
                                : "-",
                          ),
                          ItemWidget(
                            title: "Next 1 Date",
                            value: data.next1Date != null
                                ? data.next1Date.toString()
                                : "-",
                          ),
                          ItemWidget(
                            title: "Next 2 Date",
                            value: data.next2Date != null
                                ? data.next2Date.toString()
                                : "-",
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  );
                },
              ),
              const SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ItemWidget extends StatelessWidget {
  const ItemWidget({
    super.key,
    required this.title,
    required this.value,
  });

  final String title;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            width: 100,
          ),
          Flexible(
            child: Text(
              value,
              style: lightPoppins.copyWith(
                fontSize: 14,
                color: primaryColor,
              ),
              textAlign: TextAlign.right,
              maxLines: 3,
            ),
          ),
        ],
      ),
    );
  }
}
