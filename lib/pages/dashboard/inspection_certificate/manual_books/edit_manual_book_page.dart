import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/button_custom_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditManualBookPage extends StatefulWidget {
  final String titleId;
  final String titleShip;
  final String titleMenu;
  final String formCode;

  const EditManualBookPage({
    super.key,
    required this.titleId,
    required this.titleShip,
    required this.titleMenu,
    required this.formCode,
  });

  @override
  State<EditManualBookPage> createState() => _EditManualBookPageState();
}

class _EditManualBookPageState extends State<EditManualBookPage> {
  late List<List<TextEditingController>> subItemControllers;
  String messageAlert = '';

  @override
  void initState() {
    super.initState();
    subItemControllers = [];
    // Initialize subItemControllers
    final inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    for (var i = 0;
        i < inspectionCertificateProvider.detailManualBooks.length;
        i++) {
      var subItemControllersRow = List.generate(
          inspectionCertificateProvider.detailManualBooks[i].subItems.length,
          (index) {
        var subItem =
            inspectionCertificateProvider.detailManualBooks[i].subItems[index];
        return TextEditingController(
            text: subItem.remark != null ? subItem.remark.toString() : '');
      });
      subItemControllers.add(subItemControllersRow);
    }
  }

  @override
  void dispose() {
    // Clean up controllers
    for (var controllers in subItemControllers) {
      for (var controller in controllers) {
        controller.dispose();
      }
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    Future<bool> updateManualBook() async {
      List<Map<String, dynamic>> postData = [];
      for (var i = 0;
          i < inspectionCertificateProvider.detailManualBooks.length;
          i++) {
        var item = inspectionCertificateProvider.detailManualBooks[i];
        List<Map<String, dynamic>> subItems = [];
        for (var j = 0; j < item.subItems.length; j++) {
          var subItem = item.subItems[j];
          subItems.add({
            'id': subItem.id,
            'parent_id': subItem.parentId,
            'item': subItem.item,
            'remark': subItemControllers[i][j].text,
          });
        }
        postData.add({
          'id': item.id,
          'item': item.item,
          'sub_items': subItems,
        });
      }

      Map<String, dynamic> requestBody = {'items': postData};
      var isSuccess = await inspectionCertificateProvider.updateManualBook(
        bodyRequest: requestBody,
        titleId: widget.titleId,
      );

      if (isSuccess) {
        messageAlert = "Berhasil di simpan";
      } else {
        messageAlert = "Data tidak berhasil di simpan";
      }

      return isSuccess;
    }

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.titleShip,
          style: semiboldPoppins,
        ),
        backgroundColor: whiteColor,
        foregroundColor: primaryColor,
      ),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              bottom: 20,
              left: 20,
              right: 20,
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 0,
                    ),
                    child: ListTile(
                      leading: const Icon(
                        Icons.radio_button_checked_outlined,
                        color: primaryColor,
                      ),
                      title: Text(
                        widget.titleMenu,
                        style: semiboldPoppins,
                      ),
                    ),
                  ),
                  ...inspectionCertificateProvider.detailManualBooks
                      .asMap()
                      .entries
                      .map(
                    (entry) {
                      var index = entry.key;
                      var data = entry.value;
                      if (subItemControllers.length <= index) {
                        subItemControllers.add(
                          List.generate(
                            data.subItems.length,
                            (_) => TextEditingController(),
                          ),
                        );
                      }
                      return Column(
                        children: [
                          ExpansionTile(
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                color: secondaryColor.withOpacity(0.6),
                                width: 2,
                              ),
                              borderRadius: const BorderRadius.all(
                                Radius.circular(8),
                              ),
                            ),
                            collapsedShape: const RoundedRectangleBorder(
                              side: BorderSide(
                                color: greyColor,
                              ),
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                            ),
                            collapsedIconColor: primaryColor,
                            collapsedTextColor: primaryColor,
                            iconColor: greyColor,
                            textColor: secondaryColor,
                            tilePadding: const EdgeInsets.symmetric(
                              horizontal: 10,
                            ),
                            title: Text(
                              data.item,
                              style: const TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            children: [
                              Divider(
                                color: secondaryColor.withOpacity(0.6),
                              ),
                              ...data.subItems.asMap().entries.map(
                                (subEntry) {
                                  var subIndex = subEntry.key;
                                  var subData = subEntry.value;
                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2.5,
                                          child: Text(
                                            subData.item,
                                            style: mediumPoppins.copyWith(
                                              fontSize: 14,
                                              color: primaryColor,
                                            ),
                                          ),
                                        ),
                                        Flexible(
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      2.5,
                                                  child: TextField(
                                                    minLines: 2,
                                                    maxLines: 5,
                                                    controller:
                                                        subItemControllers[
                                                            index][subIndex],
                                                    style:
                                                        regularPoppins.copyWith(
                                                      fontSize: 14,
                                                    ),
                                                    cursorColor: primaryColor,
                                                    keyboardType:
                                                        TextInputType.multiline,
                                                    autofocus: false,
                                                    onTap: () async {},
                                                    decoration: InputDecoration(
                                                      contentPadding:
                                                          const EdgeInsets.all(
                                                              10.0),
                                                      labelStyle:
                                                          regularPoppins,
                                                      focusedBorder:
                                                          const OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                          color: primaryColor,
                                                          width: 2,
                                                        ),
                                                      ),
                                                      border:
                                                          const OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                          color: primaryColor,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                        ],
                      );
                    },
                  ),
                  const SizedBox(
                    height: 100,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10,
              ),
              // height: 80,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(8),
                ),
                color: whiteColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 10,
                    offset: const Offset(0, -5),
                  ),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtonCustom(
                    backgroundColor: whiteColor,
                    label: "Cancel",
                    outlineBorderColor: redColor,
                    textColor: redColor,
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                  ),
                  ButtonCustom(
                    backgroundColor: primaryColor,
                    label: "Save",
                    outlineBorderColor: primaryColor,
                    textColor: whiteColor,
                    onPressed: () async {
                      var isSuccess = await updateManualBook();
                      // ignore: use_build_context_synchronously
                      SnackbarCustom.alertMessage(
                        context,
                        messageAlert,
                        2,
                        isSuccess,
                      );

                      Future.delayed(
                        const Duration(seconds: 1),
                        () {
                          Navigator.pop(context, isSuccess);
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
