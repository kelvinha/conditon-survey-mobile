import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailManualBookPage extends StatefulWidget {
  const DetailManualBookPage({super.key});

  @override
  State<DetailManualBookPage> createState() => _DetailManualBookPageState();
}

class _DetailManualBookPageState extends State<DetailManualBookPage> {
  @override
  Widget build(BuildContext context) {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(
          bottom: 20,
          left: 20,
          right: 20,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ...inspectionCertificateProvider.detailManualBooks.map(
                (data) {
                  return Column(
                    children: [
                      ExpansionTile(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                            color: secondaryColor.withOpacity(0.6),
                            width: 2,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        collapsedShape: const RoundedRectangleBorder(
                          side: BorderSide(
                            color: greyColor,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        collapsedIconColor: primaryColor,
                        collapsedTextColor: primaryColor,
                        iconColor: greyColor,
                        textColor: secondaryColor,
                        tilePadding: const EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        title: Text(
                          data.item,
                          style: const TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        children: [
                          Divider(
                            color: secondaryColor.withOpacity(0.6),
                          ),
                          ...List.generate(
                            data.subItems.length,
                            (index) => Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    data.subItems[index].item,
                                    style: mediumPoppins.copyWith(
                                      fontSize: 14,
                                      color: primaryColor,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 100,
                                  ),
                                  Flexible(
                                    child: Text(
                                      data.subItems[index].remark ?? "-",
                                      style: lightPoppins.copyWith(
                                        fontSize: 14,
                                        color: primaryColor,
                                      ),
                                      textAlign: TextAlign.right,
                                      maxLines: 3,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  );
                },
              ),
              const SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
