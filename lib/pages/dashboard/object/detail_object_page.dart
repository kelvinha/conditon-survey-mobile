import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:condition_survey/widgets/table_cell_custom_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'edit_survey_object_page.dart';

class DetailObjectPage extends StatefulWidget {
  final String titleMenu;
  final String titleApi;
  final String titleId;
  final String titleSurvey;

  const DetailObjectPage({
    required this.titleMenu,
    required this.titleApi,
    required this.titleId,
    required this.titleSurvey,
    super.key,
  });

  @override
  State<DetailObjectPage> createState() => _DetailObjectPageState();
}

class _DetailObjectPageState extends State<DetailObjectPage> {
  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          bool status = await Navigator.push(
                context,
                CupertinoPageRoute(
                  builder: ((context) => EditSurveyPage(
                        titleMenu: widget.titleMenu,
                        titleApi: widget.titleApi,
                        titleId: widget.titleId,
                        titleSurvey: widget.titleSurvey,
                      )),
                ),
              ) ??
              false;

          if (status) {
            setState(() {
              dashboardProvider.getSurveyItemObject(
                titleApi: widget.titleApi,
                titleId: widget.titleId,
              );
            });
          }
        },
        elevation: 8.0,
        backgroundColor: primaryColor,
        child: const Icon(
          Icons.mode_edit_outline_outlined,
          color: whiteColor,
        ),
      ),
      backgroundColor: whiteColor,
      appBar: AppBar(
        title: Text(
          widget.titleMenu.toUpperCase(),
          style: semiboldPoppins,
        ),
        centerTitle: true,
        backgroundColor: whiteColor,
        foregroundColor: primaryColor,
      ),
      body: FutureBuilder(
        future: dashboardProvider.getSurveyItemObject(
          titleApi: widget.titleApi,
          titleId: widget.titleId,
        ),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const LoadingIndicator();
          } else if (snapshot.hasError) {
            return Text('Error : ${snapshot.error}');
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                  ),
                  child: Center(
                    child: Text(
                      widget.titleSurvey.toUpperCase(),
                      style: semiboldPoppins,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      bottom: 20,
                      left: 20,
                      right: 20,
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          ...dashboardProvider.surveyItem.map(
                            (data) {
                              return TableCellCustom(
                                firstColumn: data.item.capitalize(),
                                secondColumn: data.value ?? '',
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }
}
