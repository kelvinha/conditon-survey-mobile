import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/button_custom_widget.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditSurveyPage extends StatefulWidget {
  final String titleMenu;
  final String titleApi;
  final String titleId;
  final String titleSurvey;

  const EditSurveyPage({
    required this.titleMenu,
    required this.titleApi,
    required this.titleId,
    required this.titleSurvey,
    super.key,
  });

  @override
  State<EditSurveyPage> createState() => _EditSurveyPageState();
}

class _EditSurveyPageState extends State<EditSurveyPage> {
  Map<String, TextEditingController> controllers = {};
  Map<String, int> listIdItem = {};
  List<TextEditingController> addItemControllers = [];
  List<TextEditingController> labelNameControllers = [];
  TextEditingController labelNameController = TextEditingController();

  String? messageAlert;
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);

    Future<bool> handleUpdateItemSurvey(
      Map<String, TextEditingController> requestBody,
    ) async {
      setState(() {
        isLoading = true;
      });

      List<Map<String, dynamic>> itemsList = [];
      controllers.forEach((key, controller) {
        Map<String, dynamic> itemMap = {
          'id': listIdItem[key],
          'item': key,
          'value': controller.text,
        };
        itemsList.add(itemMap);
      });

      if (addItemControllers.isNotEmpty) {
        for (var i = 0; i < addItemControllers.length; i++) {
          Map<String, dynamic> itemMap = {
            'id': 0,
            'item': labelNameControllers[i].text,
            'value': addItemControllers[i].text,
          };
          itemsList.add(itemMap);
        }
      }

      Map<String, dynamic> requestBody = {'items': itemsList};

      var isSuccess = await dashboardProvider.updateSurvey(
        bodyRequest: requestBody,
        titleApi: widget.titleApi,
        titleId: widget.titleId,
      );

      // ignore: avoid_print
      print(requestBody);

      if (isSuccess) {
        messageAlert = "Survey berhasil di insert";
        return isSuccess;
      } else {
        messageAlert = "Survey gagal di insert";
        return isSuccess;
      }
    }

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        foregroundColor: primaryColor,
        backgroundColor: whiteColor,
        title: Text(
          widget.titleSurvey,
          style: regularPoppins,
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            child: FutureBuilder(
              future: dashboardProvider.getSurveyItemObject(
                titleApi: widget.titleApi,
                titleId: widget.titleId,
              ),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const LoadingIndicator();
                } else if (snapshot.hasError) {
                  return Text("Error : ${snapshot.error}");
                } else {
                  return SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        ...dashboardProvider.surveyItem.map(
                          (dataObject) {
                            TextEditingController controller =
                                TextEditingController(
                                    text: dataObject.value ?? "");
                            controllers[dataObject.item] = controller;
                            // menyimpan list id
                            listIdItem[dataObject.item] = dataObject.id;
                            return FormFieldCustom(
                              labelText: dataObject.item,
                              controller: controller,
                            );
                          },
                        ),
                        Visibility(
                          visible: addItemControllers.isNotEmpty,
                          child: Row(
                            children: [
                              const Expanded(
                                child: Divider(color: secondaryColor),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  "New Items",
                                  style: regularPoppins,
                                ),
                              ),
                              const Expanded(
                                child: Divider(color: secondaryColor),
                              ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: addItemControllers.isNotEmpty,
                          child: Column(
                            children: List.generate(addItemControllers.length,
                                (index) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 10,
                                ),
                                child: TextFormField(
                                  controller: addItemControllers[index],
                                  style: regularPoppins,
                                  cursorColor: primaryColor,
                                  keyboardType: TextInputType.text,
                                  autofocus: false,
                                  decoration: InputDecoration(
                                    labelText: labelNameControllers[index].text,
                                    labelStyle: regularPoppins,
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: primaryColor,
                                        width: 2,
                                      ),
                                    ),
                                    border: const OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: primaryColor),
                                    ),
                                  ),
                                ),
                              );
                            }),
                          ),
                        ),
                        const SizedBox(
                          height: 60,
                        ),
                      ],
                    ),
                  );
                }
              },
            ),
          ),
          isLoading
              ? const LoadingIndicator()
              : Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 10,
                    ),
                    // height: 80,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(8),
                      ),
                      color: whiteColor,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 10,
                          offset: const Offset(0, -5),
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonCustom(
                          backgroundColor: whiteColor,
                          label: "Cancel",
                          outlineBorderColor: redColor,
                          textColor: redColor,
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                        ),
                        ButtonCustom(
                          backgroundColor: primaryColor,
                          label: "Save",
                          outlineBorderColor: primaryColor,
                          textColor: whiteColor,
                          onPressed: () async {
                            bool isSuccess =
                                await handleUpdateItemSurvey(controllers);
                            // ignore: use_build_context_synchronously
                            SnackbarCustom.alertMessage(
                              context,
                              messageAlert!,
                              2,
                              isSuccess,
                            );

                            Future.delayed(
                              const Duration(seconds: 1),
                              () {
                                Navigator.pop(context, isSuccess);
                              },
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
          Positioned(
            right: 5,
            bottom: 80,
            child: FloatingActionButton(
              tooltip: "Tambah Item",
              backgroundColor: primaryColor,
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      actionsAlignment: MainAxisAlignment.center,
                      actions: [
                        ButtonCustom(
                          backgroundColor: primaryColor,
                          label: "Tambahkan Item",
                          outlineBorderColor: primaryColor,
                          textColor: whiteColor,
                          onPressed: () {
                            setState(() {
                              addItemControllers.add(TextEditingController());
                              labelNameControllers.add(
                                TextEditingController(
                                    text: labelNameController.text),
                              );
                            });
                            Navigator.pop(context);
                          },
                          width: 200,
                        ),
                      ],
                      title: Center(
                        child: Text(
                          "Tambah Item",
                          style: mediumPoppins,
                        ),
                      ),
                      elevation: 8.0,
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Divider(),
                          TextFormField(
                            controller: labelNameController,
                            style: regularPoppins,
                            cursorColor: primaryColor,
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            onChanged: (value) {
                              // setState(() {
                              //   labelNameController.text = value;
                              // });
                            },
                            decoration: InputDecoration(
                              labelText: "Nama Label item",
                              labelStyle: regularPoppins,
                              focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: primaryColor,
                                  width: 2,
                                ),
                              ),
                              border: const OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: primaryColor,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                );
              },
              elevation: 8.0,
              child: const Icon(
                Icons.add,
                color: whiteColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FormFieldCustom extends StatefulWidget {
  final String labelText;
  final TextEditingController controller;

  const FormFieldCustom({
    required this.controller,
    required this.labelText,
    super.key,
  });

  @override
  State<FormFieldCustom> createState() => _FormFieldCustomState();
}

class _FormFieldCustomState extends State<FormFieldCustom> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: TextFormField(
        controller: widget.controller,
        style: regularPoppins,
        cursorColor: primaryColor,
        keyboardType: TextInputType.text,
        autofocus: false,
        readOnly: widget.labelText == "TAHUN PEMBUATAN" ||
                widget.labelText == "TGL BOLLARD"
            ? true
            : false,
        onTap: () async {
          if (widget.labelText == "TAHUN PEMBUATAN" ||
              widget.labelText == "TGL BOLLARD") {
            switch (widget.labelText) {
              case "TAHUN PEMBUATAN":
                DateTime? pickedDate = await showDatePicker(
                  context: context,
                  initialDatePickerMode: DatePickerMode.year,
                  firstDate: DateTime(1900),
                  lastDate: DateTime(2101),
                  builder: (context, child) {
                    return Theme(
                      data: Theme.of(context).copyWith(
                        colorScheme: const ColorScheme.light(
                          primary: secondaryColor,
                          onPrimary: whiteColor,
                          onSurface: primaryColor,
                        ),
                      ),
                      child: child!,
                    );
                  },
                );

                if (pickedDate != null) {
                  setState(() {
                    widget.controller.text = pickedDate.year.toString();
                  });
                } else {
                  // ignore: avoid_print
                  print("Date is not selected");
                }
                break;
              default:
                DateTime? pickedDate = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  initialDatePickerMode: DatePickerMode.year,
                  firstDate: DateTime(1900),
                  lastDate: DateTime(2101),
                  builder: (context, child) {
                    return Theme(
                      data: Theme.of(context).copyWith(
                        colorScheme: const ColorScheme.light(
                          primary: secondaryColor,
                          onPrimary: whiteColor,
                          onSurface: primaryColor,
                        ),
                      ),
                      child: child!,
                    );
                  },
                );

                if (pickedDate != null) {
                  var onlyDate = pickedDate.toString().split(" ");
                  setState(() {
                    widget.controller.text = onlyDate[0];
                  });
                } else {
                  // ignore: avoid_print
                  print("Date is not selected");
                }
            }
          }
        },
        decoration: InputDecoration(
          labelText: widget.labelText.capitalize(),
          labelStyle: regularPoppins,
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: primaryColor,
              width: 2,
            ),
          ),
          border: const OutlineInputBorder(
            borderSide: BorderSide(color: primaryColor),
          ),
        ),
      ),
    );
  }
}
