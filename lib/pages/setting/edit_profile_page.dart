import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/auth/users_model.dart';
import '../../providers/auth_provider.dart';
import '../../utils/constant.dart';
import '../../utils/helper.dart';
import '../../utils/theme.dart';
import '../../widgets/button_custom_widget.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confrimPassworController = TextEditingController();

  late String nameDefault;
  late String emailDefault;

  bool isLoading = false;
  bool isLock = false;
  bool isLockCP = false;
  String? messageAlert;

  @override
  void initState() {
    super.initState();
    initData();
  }

  initData() async {
    final AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    Users user = authProvider.user;
    nameController.text = user.name;
    emailController.text = user.email;
  }

  @override
  Widget build(BuildContext context) {
    final AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    Users user = authProvider.user;

    Future<bool> handleUpdate() async {
      // ignore: avoid_print
      if (nameController.text.isEmpty) {
        messageAlert = "Nama tidak boleh kosong";
        return false;
      }

      if (passwordController.text.isNotEmpty &&
          confrimPassworController.text.isEmpty) {
        messageAlert = "Konfirm Password tidak boleh kosong";
        return false;
      }

      if (passwordController.text != confrimPassworController.text) {
        messageAlert = "Konfirmasi password tidak sesuai";
        return false;
      }

      setState(() {
        isLoading = true;
      });

      var isSuccess = await authProvider.update(
        name: nameController.text,
        email: emailController.text,
        password: passwordController.text,
      );

      if (isSuccess) {
        setState(() {
          isLoading = false;
        });
        messageAlert = "Data berhasil diperbarui";
        // ignore: use_build_context_synchronously
        Navigator.pop(context);
        return isSuccess;
      } else {
        messageAlert = "Data gagal diperbarui";
        setState(() {
          isLoading = false;
        });
        return isSuccess;
      }
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: whiteColor,
        foregroundColor: primaryColor,
        centerTitle: true,
        title: Text(
          'Edit Profile',
          style: regularPoppins,
        ),
      ),
      backgroundColor: whiteColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 20,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AvatarImages(user: user),
              const SizedBox(
                height: 20,
              ),
              nameInput(),
              emailInput(),
              passwordInput(),
              confirmPasswordInput(),
              const SizedBox(
                height: 30,
              ),
              isLoading
                  ? const LoadingIndicator()
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        ButtonCustom(
                          backgroundColor: whiteColor,
                          label: "Cancel",
                          outlineBorderColor: redColor,
                          textColor: redColor,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        ButtonCustom(
                          backgroundColor: primaryColor,
                          label: "Save",
                          outlineBorderColor: primaryColor,
                          textColor: whiteColor,
                          onPressed: () async {
                            bool isSuccess = await handleUpdate();
                            // ignore: use_build_context_synchronously
                            SnackbarCustom.alertMessage(
                              context,
                              messageAlert!,
                              2,
                              isSuccess,
                            );
                          },
                        ),
                      ],
                    ),
            ],
          ),
        ),
      ),
    );
  }

  Widget nameInput() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: SizedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Name:",
              style: regularPoppins.copyWith(
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            TextField(
              style: regularPoppins,
              controller: nameController,
              keyboardType: TextInputType.text,
              autofocus: false,
              cursorColor: primaryColor,
              decoration: InputDecoration(
                hintText: "Enter your name",
                contentPadding: const EdgeInsets.only(
                  left: 20,
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: greyColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget emailInput() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: SizedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Email:",
              style: regularPoppins.copyWith(
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            TextField(
              controller: emailController,
              readOnly: true,
              style: regularPoppins.copyWith(
                color: Colors.black.withOpacity(
                  0.5,
                ),
              ),
              autofocus: false,
              cursorColor: primaryColor,
              decoration: InputDecoration(
                filled: true,
                fillColor: greyColor.withOpacity(0.4),
                hintText: "Enter your email",
                contentPadding: const EdgeInsets.only(
                  left: 20,
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: greyColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: greyColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget passwordInput() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: SizedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Password:",
              style: regularPoppins.copyWith(
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            TextField(
              style: regularPoppins,
              controller: passwordController,
              keyboardType: TextInputType.text,
              autofocus: false,
              obscureText: !isLock,
              cursorColor: primaryColor,
              decoration: InputDecoration(
                hintText: "Enter your new password",
                suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      isLock = !isLock;
                    });
                  },
                  child: Icon(
                    isLock ? Icons.lock_open_sharp : Icons.lock_outline_sharp,
                    color: primaryColor,
                  ),
                ),
                contentPadding: const EdgeInsets.only(
                  left: 20,
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: greyColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget confirmPasswordInput() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: SizedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Confirm Password:",
              style: regularPoppins.copyWith(
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            TextField(
              style: regularPoppins,
              controller: confrimPassworController,
              keyboardType: TextInputType.text,
              autofocus: false,
              obscureText: !isLockCP,
              cursorColor: primaryColor,
              decoration: InputDecoration(
                hintText: "Enter your confirm password",
                suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      isLockCP = !isLockCP;
                    });
                  },
                  child: Icon(
                    isLockCP ? Icons.lock_open_sharp : Icons.lock_outline_sharp,
                    color: primaryColor,
                  ),
                ),
                contentPadding: const EdgeInsets.only(
                  left: 20,
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: greyColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AvatarImages extends StatelessWidget {
  const AvatarImages({
    super.key,
    required this.user,
  });

  final Users user;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          const CircleAvatar(
            radius: 50,
            backgroundImage: AssetImage("${pathImages}avatar.png"),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            user.name,
            style: lightPoppins,
          ),
        ],
      ),
    );
  }
}
