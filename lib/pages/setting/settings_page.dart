import 'package:condition_survey/pages/setting/edit_profile_page.dart';
import 'package:condition_survey/widgets/button_custom_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/auth/users_model.dart';
import '../../providers/auth_provider.dart';
import '../../utils/constant.dart';
import '../../utils/theme.dart';
import '../../widgets/list_setting_widget.dart';
import '../auth/login_page.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  void _showAlertDialog(BuildContext context) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(
          'Alert',
          style: regularPoppins.copyWith(
            color: Colors.black,
          ),
        ),
        content: Text(
          'Are you sure?',
          style: regularPoppins.copyWith(
            color: Colors.black,
          ),
        ),
        actions: <CupertinoDialogAction>[
          CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              'No',
              style: regularPoppins.copyWith(
                color: Colors.blue,
              ),
            ),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () async {
              final AuthProvider authProvider =
                  Provider.of<AuthProvider>(context, listen: false);
              var isLogout = await authProvider.logOut();
              try {
                if (isLogout) {
                  // ignore: use_build_context_synchronously
                  Navigator.pushAndRemoveUntil(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const LoginPage(),
                    ),
                    (route) => false,
                  );
                }
              } catch (e) {
                // ignore: avoid_print
                print("errors $e");
              }
            },
            child: Text(
              'Yes',
              style: regularPoppins.copyWith(
                color: redColor,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: true);
    Users user = authProvider.user;
    return Scaffold(
      backgroundColor: whiteColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 60,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Column(
                  children: [
                    const CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage("${pathImages}avatar.png"),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      user.name,
                      style: lightPoppins,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              const ListSetting(
                nameSetting: "About",
                iconSetting: Icons.info_outline_rounded,
                destinationPage: null,
              ),
              const ListSetting(
                nameSetting: "Profile",
                iconSetting: Icons.person,
                destinationPage: ProfilePage(),
              ),
              ButtonCustom(
                backgroundColor: redColor,
                label: "Log Out",
                outlineBorderColor: redColor,
                textColor: whiteColor,
                onPressed: () {
                  _showAlertDialog(context);
                },
                width: MediaQuery.of(context).size.width,
              )
            ],
          ),
        ),
      ),
    );
  }
}
