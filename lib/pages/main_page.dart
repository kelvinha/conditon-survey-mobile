import 'package:condition_survey/pages/setting/settings_page.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:flutter/material.dart';

import 'dashboard/dashboard_page.dart';
import 'list/list_survey_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  // late StreamSubscription internetSub;
  // bool hasConnected = false;

  // @override
  // void initState() async {
  //   super.initState();
  // }

  // Future<void> initialize() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.setBool('mode', false);
  //   internetSub = Connectivity().onConnectivityChanged.listen((status) {
  //     if (status == ConnectivityResult.none) {
  //       prefs.setBool('mode', false);
  //     } else {
  //       prefs.setBool('mode', true);
  //     }
  //   });
  // }

  // @override
  // void dispose() {
  //   super.dispose();
  //   internetSub.cancel();
  // }

  int _selectedIndex = 1;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static const List<Widget> _pages = [
    ListSurveyPage(),
    DashboardPage(),
    SettingsPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: primaryColor,
        selectedItemColor: secondaryColor,
        unselectedItemColor: whiteColor,
        selectedFontSize: 0,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.list_sharp,
              size: 30,
            ),
            label: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              size: 30,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.settings,
              size: 30,
            ),
            label: '',
          ),
        ],
      ),
      backgroundColor: backgroundColor1,
      body: _pages.elementAt(_selectedIndex),
    );
  }
}
