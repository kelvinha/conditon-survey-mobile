import 'dart:async';
import 'package:condition_survey/pages/auth/login_page.dart';
import 'package:condition_survey/pages/main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../providers/auth_provider.dart';
import '../utils/constant.dart';
import '../utils/theme.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({super.key});

  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
    initialize();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> initialize() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');
    bool? usersFirstLoginDone = prefs.getBool('users_first_login_done');
    bool isSuccess;

    if (userToken != null) {
      final AuthProvider authProvider =
          // ignore: use_build_context_synchronously
          Provider.of<AuthProvider>(context, listen: false);
      try {
        if (usersFirstLoginDone!) {
          isSuccess = await authProvider.getUser();
        } else {
          isSuccess = false;
        }

        if (isSuccess) {
          // ignore: use_build_context_synchronously
          Navigator.of(context).pushReplacement(
            CupertinoPageRoute(builder: (context) => const MainPage()),
          );
        } else {
          // ignore: use_build_context_synchronously
          Navigator.of(context).pushReplacement(
            CupertinoPageRoute(builder: (context) => const LoginPage()),
          );
        }
      } catch (e) {
        // Handle any error that occurred during the asynchronous process
        // ignore: avoid_print
        print('Error: $e');
        // ignore: use_build_context_synchronously
        Navigator.of(context).pushReplacement(
          CupertinoPageRoute(builder: (context) => const LoginPage()),
        );
      }
    } else {
      // ignore: use_build_context_synchronously
      Navigator.of(context).pushReplacement(
        CupertinoPageRoute(builder: (context) => const LoginPage()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "${pathImages}logo.png",
              width: 200,
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: const LinearProgressIndicator(
                color: primaryColor,
                backgroundColor: backgroundColor1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
