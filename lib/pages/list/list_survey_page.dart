import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/not_found_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/dashboard_provider.dart';
import '../../widgets/list_survey_widget.dart';
import '../../widgets/loading_indicator_widget.dart';

class ListSurveyPage extends StatefulWidget {
  const ListSurveyPage({super.key});

  @override
  State<ListSurveyPage> createState() => _ListSurveyPageState();
}

class _ListSurveyPageState extends State<ListSurveyPage> {
  TextEditingController titleController = TextEditingController();

  List<String> statusTab = [
    'Todo',
    'In Progress',
    'Done',
  ];

  late int? current;
  @override
  void initState() {
    super.initState();
    current = null;
  }

  @override
  Widget build(BuildContext context) {
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);
    return Scaffold(
      backgroundColor: whiteColor,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 20.0,
                right: 20.0,
                top: 10.0,
              ),
              child: searchInput(),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: SizedBox(
                height: 50,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: statusTab.length,
                  itemBuilder: (context, index) {
                    if (current == index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            current = null;
                          });
                        },
                        child: TabStatusActivated(
                          status: statusTab[index],
                        ),
                      );
                    } else {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            current = index;
                          });
                        },
                        child: TabStatusNonActivated(
                          status: statusTab[index],
                        ),
                      );
                    }
                  },
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Divider(
                color: primaryColor,
              ),
            ),
            Expanded(
              child: FutureBuilder(
                future: dashboardProvider.listGetSurvey(
                  status: current,
                  title: titleController.text,
                ),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const LoadingIndicator();
                  } else if (dashboardProvider.survey.isEmpty) {
                    return const Center(child: NotFound());
                  } else {
                    return ListView.builder(
                      padding: const EdgeInsets.only(
                        top: 20,
                        right: 20,
                        left: 20,
                      ),
                      itemCount: dashboardProvider.survey.length,
                      itemBuilder: (context, index) {
                        return ListSurveyWidget(
                          titleId: dashboardProvider.survey[index].id,
                          titleShip: dashboardProvider.survey[index].title,
                          location: dashboardProvider.survey[index].location,
                          dateRange: dashboardProvider.survey[index].date!,
                          status: dashboardProvider.survey[index].status,
                          inspectorName:
                              dashboardProvider.survey[index].inspectorName,
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget searchInput() {
    return TextField(
      controller: titleController,
      onSubmitted: (value) {
        setState(() {
          titleController.text = value;
        });
      },
      style: regularPoppins,
      autofocus: false,
      cursorColor: primaryColor,
      decoration: InputDecoration(
        hintText: "Search survey",
        contentPadding: const EdgeInsets.only(
          left: 10,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.black.withOpacity(0.50),
            width: 1,
          ),
          borderRadius: BorderRadius.circular(
            8,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: primaryColor,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(
            8,
          ),
        ),
      ),
    );
  }
}

class TabStatusNonActivated extends StatelessWidget {
  final String status;

  const TabStatusNonActivated({
    required this.status,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 10),
      padding: const EdgeInsets.symmetric(
        horizontal: 2,
      ),
      height: 50,
      width: 110,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(
          Radius.circular(8),
        ),
        border: Border.all(
          color: Colors.black.withOpacity(0.50),
        ),
      ),
      child: Center(
        child: Text(
          status,
          style: regularPoppins.copyWith(
            color: Colors.black.withOpacity(0.50),
          ),
        ),
      ),
    );
  }
}

class TabStatusActivated extends StatelessWidget {
  final String status;

  const TabStatusActivated({
    required this.status,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 10),
      padding: const EdgeInsets.symmetric(
        horizontal: 2,
      ),
      height: 50,
      width: 110,
      decoration: const BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
      ),
      child: Center(
        child: Text(
          status,
          style: regularPoppins.copyWith(
            color: whiteColor,
          ),
        ),
      ),
    );
  }
}
