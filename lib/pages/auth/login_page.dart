import 'dart:async';

import 'package:condition_survey/pages/main_page.dart';
import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/button_custom_widget.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/auth_provider.dart';
import '../../utils/constant.dart';
import '../../utils/helper.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLock = false;
  bool isLoading = false;
  String? messageAlert;
  // text controller
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final AuthProvider authProvider = Provider.of<AuthProvider>(context);

    Future<bool> handleLogin() async {
      if (emailController.text.isEmpty) {
        messageAlert = "Email tidak boleh kosong";
        return false;
      } else if (passwordController.text.isEmpty) {
        messageAlert = "Password tidak boleh kosong";
        return false;
      }

      setState(() {
        isLoading = true;
      });

      var isSuccess = await authProvider.login(
        email: emailController.text,
        password: passwordController.text,
      );

      if (isSuccess) {
        messageAlert = "Login Success";
        Timer(
          const Duration(
            seconds: 3,
          ),
          () {
            setState(() {
              isLoading = false;
            });
            // ignore: use_build_context_synchronously
            Navigator.pushReplacement(
              context,
              CupertinoPageRoute(
                builder: (context) => const MainPage(),
              ),
            );
          },
        );
        return isSuccess;
      } else {
        messageAlert = "Email atau password salah";
        setState(() {
          isLoading = false;
        });
        return isSuccess;
      }
    }

    return Scaffold(
      backgroundColor: primaryColor,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 20,
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Welcome",
                  style: semiboldPoppins.copyWith(
                    color: whiteColor,
                    fontSize: 24,
                  ),
                ),
                const SizedBox(
                  height: 6,
                ),
                Text(
                  "Sign In now",
                  style: lightPoppins.copyWith(
                    color: whiteColor,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  // height: 350,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: whiteColor,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 30,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        logoWidget(),
                        emailInput(),
                        passwordInput(),
                        forgotPassword(),
                        Center(
                          child: isLoading
                              ? const LoadingIndicator()
                              : ButtonCustom(
                                  height: 40,
                                  width: 200,
                                  backgroundColor: primaryColor,
                                  label: "Login",
                                  outlineBorderColor: primaryColor,
                                  textColor: whiteColor,
                                  onPressed: () async {
                                    bool isSuccess = await handleLogin();
                                    // ignore: use_build_context_synchronously
                                    SnackbarCustom.alertMessage(
                                      context,
                                      messageAlert!,
                                      2,
                                      isSuccess,
                                    );
                                  },
                                ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget logoWidget() {
    final ConnectivityStatusProvider connStatusProvider =
        Provider.of<ConnectivityStatusProvider>(context, listen: true);
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Visibility(
              visible: !connStatusProvider.hasConnected,
              child: SizedBox(
                height: 30,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 0,
                    vertical: 4,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                // ignore: dead_code
                                color: greyColor..withOpacity(0.7),
                                blurRadius: 8,
                                offset: const Offset(
                                  2,
                                  2,
                                ))
                          ],
                          // ignore: dead_code
                          color: greyColor,
                          shape: BoxShape.circle,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        "You are offline",
                        style: semiboldPoppins.copyWith(
                          fontSize: 12,
                          color: greyColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Image.asset(
              "${pathImages}logo.png",
              width: 50,
              height: 30,
            ),
          ],
        ),
        const SizedBox(
          height: 8,
        ),
      ],
    );
  }

  Widget emailInput() {
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Email",
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          TextField(
            controller: emailController,
            style: regularPoppins,
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            cursorColor: primaryColor,
            decoration: InputDecoration(
              hintText: "Enter your email",
              contentPadding: const EdgeInsets.only(
                left: 20,
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: greyColor,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(
                  10,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: primaryColor,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(
                  10,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget passwordInput() {
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Password",
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          TextField(
            controller: passwordController,
            style: regularPoppins,
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            cursorColor: primaryColor,
            obscureText: !isLock,
            decoration: InputDecoration(
              hintText: "Enter your password",
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    isLock = !isLock;
                  });
                },
                child: Icon(
                  isLock ? Icons.lock_open_sharp : Icons.lock_outline_sharp,
                  color: primaryColor,
                ),
              ),
              contentPadding: const EdgeInsets.only(
                left: 20,
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: greyColor,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(
                  10,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: primaryColor,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(
                  10,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}

Widget forgotPassword() {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            "Forgot Password",
            style: regularPoppins.copyWith(
              color: primaryColor,
              fontSize: 12,
            ),
          ),
        ],
      ),
      const SizedBox(
        height: 10,
      ),
    ],
  );
}
