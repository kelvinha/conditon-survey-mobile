import 'dart:convert';

import 'package:condition_survey/utils/constant.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/auth/users_model.dart';

class AuthServices {
  // users login
  Future<Users> login({
    String? email,
    String? password,
  }) async {
    var headers = {'Content-type': 'application/json'};
    var bodyRequest = jsonEncode({
      'email': email,
      'password': password,
    });

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/user/login',
      ),
      headers: headers,
      body: bodyRequest,
    );

    // ignore: avoid_print
    print("debug : ${response.body}");

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      Users user = Users.fromJson(data);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', user.token!);
      return user;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // users get user
  Future<Users> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');
    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/user',
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      Users user = Users.fromJson(data);
      return user;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // update user
  Future<Users> updateUser({
    String? name,
    String? email,
    String? password,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var bodyRequest = jsonEncode({
      'name': name,
      'email': email,
      'password': password,
    });

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.put(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/user',
      ),
      headers: headers,
      body: bodyRequest,
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      Users user = Users.fromJson(data);
      return user;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // users log out
  Future<bool> logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');
    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/user/logout',
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      prefs.remove('token');
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }
}
