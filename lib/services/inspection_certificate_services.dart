// ignore_for_file: avoid_print

import 'dart:convert';
import 'package:condition_survey/models/dashboard/detail_certificate_model.dart';
import 'package:condition_survey/models/dashboard/inspection_certificate/manual_books/detail_manual_books_model.dart';
import 'package:condition_survey/models/dashboard/inspection_certificate/ship_legalities/ship_legalities_item_model.dart';
import 'package:condition_survey/models/dashboard/inspection_certificate/survey_status/survey_status_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/deck_equipment_condition/deck_equipment_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/deck_machinary_condition/deck_machinary_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/electrical_system_condition/electrical_system_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/hull_and_construction/hull_construction_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/navigation_and_communication_equipment_condition/navigation_communication_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/pipe_and_valve_system_condition/pipe_and_valve_system_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/room_condition/room_condition_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/savety_and_fire_condition/savety_and_fire_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/ship_machinery_system_condition/ship_machinery_system_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/tank_condition/tank_condition_model.dart';
import 'package:condition_survey/models/dashboard/survey_menu_model.dart';
import 'package:condition_survey/models/dashboard/survey_model.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class InspectionCertificateServices {
  // config default get survey
  Map<String, dynamic> queryParamsListSurvey = {
    'status': '0,1,2',
    'limit': '10',
    'order_by': 'status'
  };
  // get ship legalities menu
  Future<List<ShipLegalitiesItem>> getSurveyItemObject(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print(response.body);

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      List<ShipLegalitiesItem> surveyItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          surveyItem.add(ShipLegalitiesItem.fromJson(item));
        }
      }
      return surveyItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // update ship legalities item
  Future<bool> updateSurvey({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(bodyRequest),
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // update status survey item
  Future<bool> updateSurveyShipLegalities({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/ship-legalities/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(bodyRequest),
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<List<SurveyCertificate>> getSurveyItemCertificate(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      List<SurveyCertificate> surveyItemCertificate = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          surveyItemCertificate.add(SurveyCertificate.fromJson(item));
        }
      }
      return surveyItemCertificate;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<List<SurveyMenu>> getAllSurveyMenu({String? formCode}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');
    Map<String, dynamic> queryParamsSurveyMenu = {};

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    if (formCode != null) {
      queryParamsSurveyMenu.addAll({'form_code': formCode});
    }

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/menus',
        queryParamsSurveyMenu,
      ),
      headers: headers,
    );

    print("debug ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      List<SurveyMenu> surveyMenu = [];
      List<SurveyMenu> surveySubMenu = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          surveyMenu.add(SurveyMenu.fromJson(item));
        }
      }
      // remove menu title as "TITLE"
      surveyMenu.removeWhere((item) => item.formCode == "survey-titles");

      // if (formCode != null) {
      //   for (var item in surveyMenu[0].subMenu) {
      //     surveySubMenu.add(item);
      //   }
      //   print("debug ${surveySubMenu[0].formCode}");
      //   return surveySubMenu;
      // }

      return surveyMenu;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // get detail survey by id
  Future<ListSurvey> getDetailSurveyByID({String? id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/titles/$id',
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      ListSurvey survey = ListSurvey.fromJson(data);

      return survey;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // get all survey
  Future<List<ListSurvey>> getSurvey({String? title}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    if (title != null && title != "") {
      queryParamsListSurvey.addAll({'title': title});
    }

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys',
        queryParamsListSurvey,
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      List<ListSurvey> survey = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          survey.add(ListSurvey.fromJson(item));
        }
      }

      return survey;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // get all survey for list page
  Future<List<ListSurvey>> listGetSurvey({int? status, String? title}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    if (status != null) {
      queryParamsListSurvey.addAll({'status': status.toString()});
    }

    if (title != null && title != "") {
      queryParamsListSurvey.addAll({'title': title});
    }

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys',
        queryParamsListSurvey,
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      List<ListSurvey> survey = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          survey.add(ListSurvey.fromJson(item));
        }
      }
      return survey;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // manual books
  Future<List<DetailManualBooks>> getManualBooksItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<DetailManualBooks> manualBooksItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          manualBooksItem.add(DetailManualBooks.fromJson(item));
        }
      }
      return manualBooksItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // update status survey item
  Future<bool> updateManualBook({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/manual-books/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(bodyRequest),
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // manual books
  Future<List<SurveyStatus>> getSurveyStatusItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      List<SurveyStatus> surveyStatusItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          surveyStatusItem.add(SurveyStatus.fromJson(item));
        }
      }
      return surveyStatusItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<bool> updateSurveyStatusItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(
        bodyRequest,
      ),
    );

    print("debug status ${response.body}");

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan lambung dan konstruksi
  Future<List<HullConstruction>> getHullConstructionItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug hull ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<HullConstruction> hullConstructionItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          hullConstructionItem.add(HullConstruction.fromJson(item));
        }
      }

      return hullConstructionItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<bool> updateHullConstructionItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(
        bodyRequest,
      ),
    );

    print("debug status ${response.body}");

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan kondisi tangki
  Future<List<TankCondition>> getTankConditionItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug tank ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<TankCondition> tankConditionItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          tankConditionItem.add(TankCondition.fromJson(item));
        }
      }

      return tankConditionItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<bool> updateTankConditionItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(
        bodyRequest,
      ),
    );

    print("debug status ${response.body}");

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan kondisi sistem pipa dan katup
  Future<List<PipeValveSystem>> getPipeValveSystemItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug pipe ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<PipeValveSystem> pipeValveSystemItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          pipeValveSystemItem.add(PipeValveSystem.fromJson(item));
        }
      }

      return pipeValveSystemItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<bool> updatePipeValveSystemItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(
        bodyRequest,
      ),
    );

    print("debug update pipe ${response.body}");

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan kondisi kondisi ruang
  Future<List<RoomCondition>> getRoomConditionItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug room ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<RoomCondition> roomConditionItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          roomConditionItem.add(RoomCondition.fromJson(item));
        }
      }

      return roomConditionItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan kondisi peralatan geladak
  Future<List<DeckEquipment>> getDeckEquipmentItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug deck equip ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<DeckEquipment> deckEquipmentItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          deckEquipmentItem.add(DeckEquipment.fromJson(item));
        }
      }

      return deckEquipmentItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan kondisi mesin geladak
  Future<List<DeckMachinary>> getDeckMachinaryItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug deck equip ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<DeckMachinary> deckMachinaryItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          deckMachinaryItem.add(DeckMachinary.fromJson(item));
        }
      }

      return deckMachinaryItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan kondisi sistem listrik
  Future<List<ElectricalSystem>> getElectricalSystemItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug electrical ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<ElectricalSystem> electricalSystemItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          electricalSystemItem.add(ElectricalSystem.fromJson(item));
        }
      }

      return electricalSystemItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan kondisi pelaralatan navigasi dan komunikasi
  Future<List<NavigationAndCommunication>> getNavigationAndComunicationItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug navigation ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<NavigationAndCommunication> navigationAndCommunicationItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          navigationAndCommunicationItem
              .add(NavigationAndCommunication.fromJson(item));
        }
      }

      return navigationAndCommunicationItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // pemeriksaan kondisi pelaralatan keselamatan dan pemadam kebakaran
  Future<List<SavetyAndFire>> getSafetyAndFireItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug safety ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<SavetyAndFire> savetyAndFireItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          savetyAndFireItem.add(SavetyAndFire.fromJson(item));
        }
      }

      return savetyAndFireItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<List<ShipMachinerySystem>> getShipMachinerySystemItem(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    print("debug hull ${response.body}");

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data']['items'];
      List<ShipMachinerySystem> shipMachinerySystemItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          shipMachinerySystemItem.add(ShipMachinerySystem.fromJson(item));
        }
      }

      return shipMachinerySystemItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<bool> updateItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(
        bodyRequest,
      ),
    );

    print("debug update room ${response.body}");

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }
}
