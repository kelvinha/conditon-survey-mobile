import 'dart:convert';
import 'dart:io';
import 'package:condition_survey/models/dashboard/file_attachment_model.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:file_picker/file_picker.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:path/path.dart' as path;

class MediaServices {
  // upload file PDF
  Future<List<FileAttachment>> uploadFilePDF({
    String? titleId,
    FilePickerResult? filePdf,
    String? formCode,
    String? name,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var request = http.MultipartRequest(
      'POST',
      Uri.parse('$basePathUrl/file-uploads/$titleId'),
    );

    request.headers.addAll(headers);

    // Data tambahan
    request.fields['name'] = name!;
    request.fields['form_code'] = formCode!;
    request.fields['type'] = 'file';

    for (var file in filePdf!.files) {
      File newFile = File(file.path!);
      request.files.add(http.MultipartFile(
        'files[]',
        newFile.readAsBytes().asStream(),
        file.size,
        filename: file.name,
      ));
    }

    var response = await request.send();
    var responseString = await response.stream.bytesToString();

    if (response.statusCode == 200) {
      // Decode respons JSON menjadi Map
      List data = jsonDecode(responseString)['data']['file_uploads'];
      // Buat instance FileAttachment dari jsonResponse
      List<FileAttachment> filePdf = [];
      if (data.isNotEmpty) {
        for (var item in data) {
          filePdf.add(FileAttachment.fromJson(item));
        }
      }

      // ignore: avoid_print
      print("debug $responseString");

      return filePdf;
    } else {
      throw responseString;
    }
  }

  // upload file Image
  Future<List<FileAttachment>> uploadFileImage({
    String? titleId,
    List<XFile>? listImages,
    String? formCode,
    String? name,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var request = http.MultipartRequest(
      'POST',
      Uri.parse('$basePathUrl/file-uploads/$titleId'),
    );

    request.headers.addAll(headers);

    // Data tambahan
    request.fields['name'] = name!;
    request.fields['form_code'] = formCode!;
    request.fields['type'] = 'image';

    for (var file in listImages!) {
      File newFile = File(file.path);
      request.files.add(http.MultipartFile(
        'files[]',
        newFile.readAsBytes().asStream(),
        newFile.lengthSync(),
        filename: newFile.path.split('/').last,
      ));
    }

    var response = await request.send();
    var responseString = await response.stream.bytesToString();

    if (response.statusCode == 200) {
      // Decode respons JSON menjadi Map
      List data = jsonDecode(responseString)['data']['file_uploads'];
      // Buat instance FileAttachment dari jsonResponse
      List<FileAttachment> fileImage = [];
      if (data.isNotEmpty) {
        for (var item in data) {
          fileImage.add(FileAttachment.fromJson(item));
        }
      }

      // ignore: avoid_print
      print("debug $responseString");

      return fileImage;
    } else {
      throw responseString;
    }
  }

  // upload file Image
  Future<List<FileAttachment>> uploadFileAudio({
    String? titleId,
    File? voiceNote,
    String? formCode,
    String? name,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var request = http.MultipartRequest(
      'POST',
      Uri.parse('$basePathUrl/file-uploads/$titleId'),
    );

    request.headers.addAll(headers);

    // Data tambahan
    request.fields['name'] = name!;
    request.fields['form_code'] = formCode!;
    request.fields['type'] = 'audio';

    File newFile = File(voiceNote!.path);
    // String extension = path.extension(voiceNote.path);
    request.files.add(http.MultipartFile(
      'files[]',
      newFile.readAsBytes().asStream(),
      newFile.lengthSync(),
      filename: newFile.path.split('/').last,
    ));

    var response = await request.send();
    var responseString = await response.stream.bytesToString();

    if (response.statusCode == 200) {
      // Decode respons JSON menjadi Map
      List data = jsonDecode(responseString)['data']['file_uploads'];
      // Buat instance FileAttachment dari jsonResponse
      List<FileAttachment> fileImage = [];
      if (data.isNotEmpty) {
        for (var item in data) {
          fileImage.add(FileAttachment.fromJson(item));
        }
      }

      // ignore: avoid_print
      print("debug $responseString");

      return fileImage;
    } else {
      throw responseString;
    }
  }

  // upload file Image
  Future<bool> deleteFile({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
    String? formCode,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.delete(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/file-uploads/$titleId',
      ),
      headers: headers,
      body: jsonEncode(bodyRequest),
    );

    // ignore: avoid_print
    print("debug $response");

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw (errorMessage);
    }
  }

  Future<bool> checkUrlStorage({String? url}) async {
    var headers = {
      'Content-type': 'application/json',
    };

    var response = await http.get(
      Uri.parse(url!),
      headers: headers,
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      throw false;
    }
  }
}
