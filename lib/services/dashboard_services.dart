import 'dart:convert';
import 'package:condition_survey/models/dashboard/detail_certificate_model.dart';
import 'package:condition_survey/models/dashboard/grading_model.dart';
import 'package:condition_survey/models/dashboard/survey_menu_model.dart';
import 'package:condition_survey/models/dashboard/summary_survey_model.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/dashboard/survey_item_model.dart';
import '../models/dashboard/survey_model.dart';

class DashboardServices {
  // config default get survey
  Map<String, dynamic> queryParamsListSurvey = {
    'status': '0,1,2',
    'limit': '10',
    'order_by': 'status'
  };

  // get summary survey
  Future<SummarySurvey> getSummary() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');
    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/summary',
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      SummarySurvey summarySurvey = SummarySurvey.fromJson(data);
      return summarySurvey;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // get all survey
  Future<List<ListSurvey>> getSurvey({String? title}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    if (title != null && title != "") {
      queryParamsListSurvey.addAll({'title': title});
    }

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys',
        queryParamsListSurvey,
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      List<ListSurvey> survey = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          survey.add(ListSurvey.fromJson(item));
        }
      }

      return survey;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // get all survey for list page
  Future<List<ListSurvey>> listGetSurvey({int? status, String? title}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    if (status != null) {
      queryParamsListSurvey.addAll({'status': status.toString()});
    }

    if (title != null && title != "") {
      queryParamsListSurvey.addAll({'title': title});
    }

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys',
        queryParamsListSurvey,
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      List<ListSurvey> survey = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          survey.add(ListSurvey.fromJson(item));
        }
      }
      return survey;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // get survey menu
  Future<List<SurveyItem>> getSurveyItemObject(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      // ignore: avoid_print
      // print(response.body);
      List data = jsonDecode(response.body)['data'];
      List<SurveyItem> surveyItem = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          surveyItem.add(SurveyItem.fromJson(item));
        }
      }
      return surveyItem;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // update survey item
  Future<bool> updateSurvey({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(bodyRequest),
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // update status survey item
  Future<bool> updateStatusSurvey({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');
    Map<String, dynamic> queryParams = {'status': '2'};

    var headers = {
      'accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.put(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/titles/$titleId',
        queryParams,
      ),
      headers: headers,
      body: jsonEncode(bodyRequest),
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // get detail survey by id
  Future<ListSurvey> getDetailSurveyByID({String? id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/titles/$id',
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      ListSurvey survey = ListSurvey.fromJson(data);

      return survey;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<List<SurveyCertificate>> getSurveyItemCertificate(
      {String? titleApi, String? titleId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/$titleApi/$titleId',
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      // ignore: avoid_print
      List data = jsonDecode(response.body)['data'];
      List<SurveyCertificate> surveyItemCertificate = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          surveyItemCertificate.add(SurveyCertificate.fromJson(item));
        }
      }
      return surveyItemCertificate;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<List<SurveyMenu>> getAllSurveyMenu({String? formCode}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');
    Map<String, dynamic> queryParamsSurveyMenu = {};

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    if (formCode != null) {
      queryParamsSurveyMenu.addAll({'form_code': formCode});
    }

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/menus',
        queryParamsSurveyMenu,
      ),
      headers: headers,
    );

    if (response.statusCode == 200) {
      // ignore: avoid_print
      List data = jsonDecode(response.body)['data'];
      List<SurveyMenu> surveyMenu = [];
      List<SurveyMenu> surveySubMenu = [];

      if (data.isNotEmpty) {
        for (var item in data) {
          surveyMenu.add(SurveyMenu.fromJson(item));
        }
      }
      // remove menu title as "TITLE"
      surveyMenu.removeWhere((item) => item.title == "TITLE");

      if (formCode != null) {
        for (var item in surveyMenu[0].subMenu) {
          surveySubMenu.add(item);
        }
        return surveySubMenu;
      }

      return surveyMenu;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  // update status survey item
  Future<bool> updateSurveyShipLegalities({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');

    var headers = {
      'accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    var response = await http.post(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/ship-legalities/bulk-update/$titleId',
      ),
      headers: headers,
      body: jsonEncode(bodyRequest),
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }

  Future<Grading> getGrading({String? grade, String? idCategoryGrading}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString('token');
    Map<String, dynamic> queryParamsSurveyMenu = {};

    var headers = {
      'Content-type': 'application/json',
      'Authorization': 'Bearer $userToken',
    };

    if (grade != null) {
      queryParamsSurveyMenu.addAll({'grade': grade});
    }

    var response = await http.get(
      Uri.https(
        basePathUrlWithoutHTTPS,
        '/api/surveys/gradings/$idCategoryGrading',
        queryParamsSurveyMenu,
      ),
      headers: headers,
    );

    // ignore: avoid_print
    print("debug grading ${response.request}");

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      Grading grading = Grading.fromJson(data);
      return grading;
    } else {
      var errorMessage = jsonDecode(response.body)['message'];
      throw Exception(errorMessage);
    }
  }
}
