// ignore_for_file: avoid_print, duplicate_ignore

import 'dart:convert';

import 'package:condition_survey/keys.dart';
import 'package:condition_survey/models/dashboard/detail_certificate_model.dart';
import 'package:condition_survey/models/dashboard/grading_model.dart';
import 'package:condition_survey/models/dashboard/survey_menu_model.dart';
import 'package:condition_survey/models/dashboard/summary_survey_model.dart';
import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:condition_survey/services/dashboard_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/dashboard/survey_item_model.dart';
import '../models/dashboard/survey_model.dart';

class DashboardProvider with ChangeNotifier {
  BuildContext context = navigatorKey.currentContext!;
  // set error message
  late String _errorMessage;

  String get errorMessage => _errorMessage;

  set errorMessage(String errorMessage) {
    _errorMessage = errorMessage;
    notifyListeners();
  }

  // get summary survey
  late SummarySurvey _summarySurvey;

  SummarySurvey get summarySurvey => _summarySurvey;

  set summarySurvey(SummarySurvey summarySurvey) {
    _summarySurvey = summarySurvey;
    notifyListeners();
  }

  Future<bool> getSummary() async {
    try {
      bool hasConnected =
          context.read<ConnectivityStatusProvider>().hasConnected;
      if (hasConnected) {
        SummarySurvey summarySurvey = await DashboardServices().getSummary();
        _summarySurvey = summarySurvey;
        notifyListeners();
      } else {
        _summarySurvey =
            SummarySurvey(todo: 0, inProgress: 0, done: 0, total: 0);
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  // get surveys
  List<ListSurvey> _survey = [];
  List<ListSurvey> get survey => _survey;

  set survey(List<ListSurvey> survey) {
    _survey = survey;
    notifyListeners();
  }

  Future<void> getSurvey({
    String? title,
    bool? isDownload,
    ListSurvey? listSurveyDownload,
  }) async {
    try {
      bool hasConnected =
          context.read<ConnectivityStatusProvider>().hasConnected;
      // print("debug title ${_survey[0].title}");
      if (hasConnected) {
        List<ListSurvey> survey =
            await DashboardServices().getSurvey(title: title);
        _survey = survey;

        if (isDownload != null && isDownload) {
          List<ListSurvey> listSurvey = [];
          SharedPreferences prefs = await SharedPreferences.getInstance();
          var checkMenu = prefs.getString('list_menu');

          if (checkMenu != null) {
            // untuk mengambil value sebelumnya
            var listSurveyDecode = jsonDecode(checkMenu);
            ListSurvey mappingListSurvey =
                ListSurvey.fromJson(listSurveyDecode);
            listSurvey.add(mappingListSurvey);
            // untuk menambahkan value yang baru
            listSurvey.add(listSurveyDownload!);

            bool isSuccess = await prefs.setString(
              'list_menu',
              jsonEncode(listSurvey),
            );

            if (isSuccess) {
              print("debug sukses save offline ${listSurveyDownload.title}");
            } else {
              print("debug gagal save offline ${listSurveyDownload.title}");
            }
          } else {
            bool isSuccess = await prefs.setString(
              'list_menu',
              jsonEncode(listSurveyDownload),
            );

            if (isSuccess) {
              print("debug sukses save offline ${listSurveyDownload?.title}");
            } else {
              print("debug gagal save offline ${listSurveyDownload?.title}");
            }
          }
        }
        notifyListeners();
      } else {
        List<ListSurvey> listSurvey = [];
        SharedPreferences prefs = await SharedPreferences.getInstance();
        var checkMenu = prefs.getString('list_menu');
        if (checkMenu != null) {
          try {
            var listSurveyDecode = jsonDecode(checkMenu) as List;
            listSurvey = listSurveyDecode
                .map(
                    (item) => ListSurvey.fromJson(item as Map<String, dynamic>))
                .toList();
            _survey = listSurvey;
          } catch (e) {
            print("debug error $e");
          }
          notifyListeners();
        } else {
          _survey = [];
          notifyListeners();
        }
      }
    } catch (e) {
      print(e);
    }
    notifyListeners();
  }

  Future<void> listGetSurvey({int? status, String? title}) async {
    try {
      // switch status
      switch (status) {
        case 1: // 1 = for done
          status = 2;
          break;
        case 2: // 2 = for in progress
          status = 1;
          break;
      }

      List<ListSurvey> survey = await DashboardServices().listGetSurvey(
        status: status,
        title: title,
      );
      _survey = survey;
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
    notifyListeners();
  }

  // get menu item
  List<SurveyItem> _surveyItem = [];
  List<SurveyItem> get surveyItem => _surveyItem;

  set surveyItem(List<SurveyItem> surveyItem) {
    _surveyItem = surveyItem;
    notifyListeners();
  }

  Future<void> getSurveyItemObject({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<SurveyItem> surveyItem =
          await DashboardServices().getSurveyItemObject(
        titleApi: titleApi,
        titleId: titleId,
      );
      _surveyItem = surveyItem;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<bool> updateSurvey({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool status = await DashboardServices().updateSurvey(
        titleApi: titleApi,
        titleId: titleId,
        bodyRequest: bodyRequest,
      );

      if (status) {
        List<SurveyItem> surveyItem =
            await DashboardServices().getSurveyItemObject(
          titleApi: titleApi,
          titleId: titleId,
        );
        _surveyItem = surveyItem;
        notifyListeners();
      }
      return status;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }

  Future<bool> updateStatusSurvey({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool status = await DashboardServices().updateStatusSurvey(
        titleId: titleId,
        bodyRequest: bodyRequest,
      );

      if (status) {
        getSurvey();
        notifyListeners();
      }
      return status;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }

  late ListSurvey _detailSurvey;

  ListSurvey get detailSurvey => _detailSurvey;

  set getDetailSurvey(ListSurvey detailSurvey) {
    _detailSurvey = detailSurvey;
    notifyListeners();
  }

  Future<bool> getDetailSurveyByID(String? id) async {
    try {
      ListSurvey survey = await DashboardServices().getDetailSurveyByID(id: id);
      _detailSurvey = survey;
      return true;
    } catch (e) {
      return false;
    }
  }

  // get menu item Certificate
  List<SurveyCertificate> _surveyCertificateItem = [];
  List<SurveyCertificate> get surveyCertificate => _surveyCertificateItem;

  set surveyCertificate(List<SurveyCertificate> surveyCertificate) {
    _surveyCertificateItem = surveyCertificate;
    notifyListeners();
  }

  Future<void> getSurveyItemCertificate({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<SurveyCertificate> surveyCertificate =
          await DashboardServices().getSurveyItemCertificate(
        titleApi: titleApi,
        titleId: titleId,
      );
      _surveyCertificateItem = surveyCertificate;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get all survey menu
  List<SurveyMenu> _surveyMenu = [];
  List<SurveyMenu> get surveyMenu => _surveyMenu;

  set surveyMenu(List<SurveyMenu> surveyCertificate) {
    _surveyMenu = surveyMenu;
    notifyListeners();
  }

  Future<void> getSurveyMenu({String? formCode}) async {
    try {
      List<SurveyMenu> listSurveyMenu =
          await DashboardServices().getAllSurveyMenu(formCode: formCode);
      _surveyMenu = listSurveyMenu;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get all survey menu
  List<SurveyMenu> _surveySubMenu = [];
  List<SurveyMenu> get surveySubMenu => _surveySubMenu;

  set surveySubMenu(List<SurveyMenu> surveyCertificate) {
    _surveySubMenu = surveyMenu;
    notifyListeners();
  }

  Future<void> getSurveySubMenu({String? formCode}) async {
    try {
      List<SurveyMenu> listSurveySubMenu =
          await DashboardServices().getAllSurveyMenu(formCode: formCode);
      _surveySubMenu = listSurveySubMenu;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get subMenuActive
  late String _subMenuActive;
  String get subMenuActive => _subMenuActive;
  // get subMenuActive
  late String _titleSubMenu;
  String get titleSubMenu => _titleSubMenu;

  set subMenuActive(String subMenuActive) {
    _subMenuActive = subMenuActive;
    notifyListeners();
  }

  set titleSubMenu(String titleSubMenu) {
    _titleSubMenu = titleSubMenu;
    notifyListeners();
  }

  Future<void> getSubMenuActive({String? formCode}) async {
    try {
      if (formCode != null) {
        _subMenuActive = formCode;
      } else {
        _subMenuActive = "";
      }
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  late Grading _grading;
  Grading get grading => _grading;

  set grading(Grading grading) {
    _grading = grading;
    notifyListeners();
  }

  Future<bool> getGrading({
    String? grade,
    String? idCategoryGrading,
  }) async {
    try {
      Grading grading = await DashboardServices().getGrading(
        grade: grade,
        idCategoryGrading: idCategoryGrading,
      );
      _grading = grading;
      notifyListeners();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> updateSurveyShipLegalities({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool status = await DashboardServices().updateSurveyShipLegalities(
        titleId: titleId,
        bodyRequest: bodyRequest,
      );
      return status;
    } catch (e) {
      // ignore: avoid_print
      print("error $e");
      _errorMessage = e.toString();
      return false;
    }
  }
}
