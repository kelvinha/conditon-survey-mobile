import 'dart:convert';
import 'package:condition_survey/models/dashboard/detail_object_model.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class DetailObjectProvider with ChangeNotifier {
  List<DetailObject> _data = [];

  List<DetailObject> get data => _data;

  Future<void> loadData() async {
    await Future.delayed(const Duration(seconds: 2));
    String jsonString =
        await rootBundle.loadString('$pathStorage/menu_object/object.json');
    Map<String, dynamic> jsonData = jsonDecode(jsonString);
    List<dynamic> jsonData2 = jsonData['data'];
    _data = jsonData2.map((item) => DetailObject.fromJson(item)).toList();

    notifyListeners();
  }
}
