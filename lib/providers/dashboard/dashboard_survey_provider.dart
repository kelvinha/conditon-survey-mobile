import 'dart:convert';
import 'package:condition_survey/models/list/survey_model.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class DashboardSurveyProvider with ChangeNotifier {
  List<Survey> _data = [];

  List<Survey> get data => _data;

  Future<void> loadData() async {
    await Future.delayed(const Duration(seconds: 2));
    String jsonString =
        await rootBundle.loadString('$pathStorage/dashboard/list_survey.json');
    Map<String, dynamic> jsonData = jsonDecode(jsonString);
    List<dynamic> jsonData2 = jsonData['data'];
    _data = jsonData2.map((item) => Survey.fromJson(item)).toList();

    notifyListeners();
  }
}
