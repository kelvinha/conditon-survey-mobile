import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

// ignore: constant_identifier_names
enum ConnectivityStatus { Online, Offline }

class ConnectivityStatusProvider with ChangeNotifier {
  bool _hasConnected = false;

  bool get hasConnected => _hasConnected;

  ConnectivityStatusProvider() {
    connectivityService();
  }

  Future<void> connectivityService() async {
    Connectivity().onConnectivityChanged.listen((status) {
      if (status == ConnectivityResult.none) {
        _hasConnected = false;
      } else {
        _hasConnected = true;
      }
      notifyListeners();
    });
  }
}
