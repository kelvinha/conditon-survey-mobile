import 'dart:io';

import 'package:condition_survey/models/dashboard/file_attachment_model.dart';
import 'package:condition_survey/services/media_services.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';

class MediaProvider with ChangeNotifier {
  // set error message
  late String _errorMessage;

  String get errorMessage => _errorMessage;

  set errorMessage(String errorMessage) {
    _errorMessage = errorMessage;
    notifyListeners();
  }

  List<FileAttachment> _filePdfs = [];
  List<FileAttachment> get filePdfs => _filePdfs;

  set filePdfs(List<FileAttachment> filePdfs) {
    _filePdfs = filePdfs;
    notifyListeners();
  }

  Future<bool> uploadFilePDF({
    String? titleId,
    FilePickerResult? filePdf,
    String? formCode,
    String? name,
  }) async {
    try {
      List<FileAttachment> filePdfs = await MediaServices().uploadFilePDF(
        titleId: titleId,
        filePdf: filePdf,
        formCode: formCode,
        name: name,
      );

      _filePdfs = filePdfs;
      notifyListeners();
      return true;
    } catch (e) {
      // ignore: avoid_print
      print("error $e");
      _errorMessage = e.toString();
      return false;
    }
  }

  List<FileAttachment> _images = [];
  List<FileAttachment> get images => _images;

  set images(List<FileAttachment> images) {
    _images = images;
    notifyListeners();
  }

  Future<bool> uploadFileImage({
    String? titleId,
    List<XFile>? listImages,
    String? formCode,
    String? name,
  }) async {
    try {
      List<FileAttachment> images = await MediaServices().uploadFileImage(
        titleId: titleId,
        listImages: listImages,
        formCode: formCode,
        name: name,
      );

      _images = images;
      notifyListeners();
      return true;
    } catch (e) {
      // ignore: avoid_print
      print("error $e");
      _errorMessage = e.toString();
      return false;
    }
  }

  String _voiceNote = "";
  String get voiceNote => _voiceNote;

  set voiceNote(String voiceNote) {
    _voiceNote = voiceNote;
    notifyListeners();
  }

  Future<bool> uploadFileAudio({
    String? titleId,
    File? audio,
    String? formCode,
    String? name,
  }) async {
    try {
      List<FileAttachment> vn = await MediaServices().uploadFileAudio(
        titleId: titleId,
        voiceNote: audio,
        formCode: formCode,
        name: name,
      );

      _voiceNote = vn[0].path;
      notifyListeners();
      return true;
    } catch (e) {
      // ignore: avoid_print
      print("error $e");
      _errorMessage = e.toString();
      return false;
    }
  }

  Future<bool> deleteFile({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
    String? formCode,
  }) async {
    try {
      var isSuccess = await MediaServices().deleteFile(
        titleId: titleId,
        bodyRequest: bodyRequest,
        formCode: formCode,
      );
      notifyListeners();
      return isSuccess;
    } catch (e) {
      // ignore: avoid_print
      print("error $e");
      _errorMessage = e.toString();
      return false;
    }
  }

  Future<bool> checkUrlStorage({
    String? url,
  }) async {
    try {
      bool status = await MediaServices().checkUrlStorage(url: url);
      return status;
    } catch (e) {
      return false;
    }
  }
}
