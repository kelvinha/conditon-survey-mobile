import 'dart:convert';

import 'package:condition_survey/keys.dart';
import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:condition_survey/services/auth_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/auth/users_model.dart';

class AuthProvider with ChangeNotifier {
  BuildContext context = navigatorKey.currentContext!;
  late Users _user;

  Users get user => _user;

  set user(Users user) {
    _user = user;
    notifyListeners();
  }

  Future<bool> login({
    String? email,
    String? password,
  }) async {
    try {
      bool hasConnected =
          context.read<ConnectivityStatusProvider>().hasConnected;
      if (hasConnected) {
        Users user = await AuthServices().login(
          email: email,
          password: password,
        );
        _user = user;
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('users', jsonEncode(user));
        prefs.setString('users_pass', password!);
        prefs.setString('users_email', email!);
        prefs.setBool('users_first_login_done', true);
        return true;
      } else {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        var user = prefs.getString('users');
        var userPass = prefs.getString('users_pass');
        var userEmail = prefs.getString('users_email');
        if (userEmail == email && userPass == password) {
          var data = jsonDecode(user!);
          Users userData = Users.fromJson(data);
          _user = userData;
          return true;
        } else {
          return false;
        }
      }
    } catch (e) {
      return false;
    }
  }

  Future<bool> getUser() async {
    try {
      Users user = await AuthServices().getUser();
      _user = user;
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> logOut() async {
    try {
      bool hasConnected =
          context.read<ConnectivityStatusProvider>().hasConnected;
      if (hasConnected) {
        var isLogOut = await AuthServices().logOut();
        return isLogOut;
      } else {
        return true;
      }
    } catch (e) {
      return false;
    }
  }

  Future<bool> update({
    String? name,
    String? email,
    String? password,
  }) async {
    try {
      Users user = await AuthServices().updateUser(
        name: name,
        email: email,
        password: password,
      );
      _user = user;
      notifyListeners();
      return true;
    } catch (e) {
      return false;
    }
  }
}
