import 'dart:convert';

import 'package:condition_survey/keys.dart';
import 'package:condition_survey/models/dashboard/detail_certificate_model.dart';
import 'package:condition_survey/models/dashboard/inspection_certificate/manual_books/detail_manual_books_model.dart';
import 'package:condition_survey/models/dashboard/inspection_certificate/ship_legalities/ship_legalities_item_model.dart';
import 'package:condition_survey/models/dashboard/inspection_certificate/survey_status/survey_status_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/deck_equipment_condition/deck_equipment_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/deck_machinary_condition/deck_machinary_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/electrical_system_condition/electrical_system_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/hull_and_construction/hull_construction_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/navigation_and_communication_equipment_condition/navigation_communication_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/pipe_and_valve_system_condition/pipe_and_valve_system_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/room_condition/room_condition_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/savety_and_fire_condition/savety_and_fire_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/ship_machinery_system_condition/ship_machinery_system_model.dart';
import 'package:condition_survey/models/dashboard/inspection_physicals/tank_condition/tank_condition_model.dart';
import 'package:condition_survey/models/dashboard/survey_menu_model.dart';
import 'package:condition_survey/models/dashboard/survey_model.dart';
import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:condition_survey/services/inspection_certificate_services.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InspectionCertificateProvider with ChangeNotifier {
  BuildContext context = navigatorKey.currentContext!;
  // set initialize menu
  List<dynamic> _initializeMenu = [];
  List<dynamic> get initializeMenu => _initializeMenu;

  set initializeMenu(List<dynamic> initializeMenu) {
    _initializeMenu = initializeMenu;
    notifyListeners();
  }

  // set error message
  late String _errorMessage;

  String get errorMessage => _errorMessage;

  set errorMessage(String errorMessage) {
    _errorMessage = errorMessage;
    notifyListeners();
  }

  // get menu item
  List<ShipLegalitiesItem> _surveyItem = [];
  List<ShipLegalitiesItem> get surveyItem => _surveyItem;

  set surveyItem(List<ShipLegalitiesItem> surveyItem) {
    _surveyItem = surveyItem;
    notifyListeners();
  }

  Future<void> getSurveyItemObject({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<ShipLegalitiesItem> surveyItem =
          await InspectionCertificateServices().getSurveyItemObject(
        titleApi: titleApi,
        titleId: titleId,
      );
      _surveyItem = surveyItem;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<bool> updateSurveyShipLegalities({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool status =
          await InspectionCertificateServices().updateSurveyShipLegalities(
        titleId: titleId,
        bodyRequest: bodyRequest,
      );
      return status;
    } catch (e) {
      // ignore: avoid_print
      print("error $e");
      _errorMessage = e.toString();
      return false;
    }
  }

  // get menu item Certificate
  List<SurveyCertificate> _surveyCertificateItem = [];
  List<SurveyCertificate> get surveyCertificate => _surveyCertificateItem;

  set surveyCertificate(List<SurveyCertificate> surveyCertificate) {
    _surveyCertificateItem = surveyCertificate;
    notifyListeners();
  }

  Future<void> getSurveyItemCertificate({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<SurveyCertificate> surveyCertificate =
          await InspectionCertificateServices().getSurveyItemCertificate(
        titleApi: titleApi,
        titleId: titleId,
      );
      _surveyCertificateItem = surveyCertificate;
      _initializeMenu = surveyCertificate;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<void> getItemBySubMenuInspectionCertifcate({
    String? titleApi,
    String? titleId,
  }) async {
    switch (titleApi) {
      case shipLegalities:
        await getSurveyItemCertificate(titleApi: titleApi, titleId: titleId);
        break;
      case manualBooks:
        await getManualBooksItem(titleApi: titleApi, titleId: titleId);
        break;
      default:
        await getSurveyStatusItem(titleApi: titleApi, titleId: titleId);
        break;
    }
  }

  // get all survey menu
  List<SurveyMenu> _surveyMenu = [];
  List<SurveyMenu> get surveyMenu => _surveyMenu;

  set surveyMenu(List<SurveyMenu> surveyCertificate) {
    _surveyMenu = surveyMenu;
    notifyListeners();
  }

  Future<void> getSurveyMenu({String? formCode, bool? isDownload}) async {
    try {
      bool hasConnected =
          context.read<ConnectivityStatusProvider>().hasConnected;
      if (hasConnected) {
        List<SurveyMenu> listSurveyMenu = await InspectionCertificateServices()
            .getAllSurveyMenu(formCode: formCode);
        _surveyMenu = listSurveyMenu;

        if (isDownload != null && isDownload) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('survey_menu',
              jsonEncode(listSurveyMenu.map((e) => e.toJson()).toList()));
        }
        notifyListeners();
      } else {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        var checkMenu = prefs.getString('survey_menu');
        List<SurveyMenu> listSurveyMenu = jsonDecode(checkMenu!);
        _surveyMenu = listSurveyMenu;
        notifyListeners();
      }
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get all survey menu
  List<SurveyMenu> _surveySubMenu = [];
  List<SurveyMenu> get surveySubMenu => _surveySubMenu;

  set surveySubMenu(List<SurveyMenu> surveyCertificate) {
    _surveySubMenu = surveyMenu;
    notifyListeners();
  }

  Future<void> getSurveySubMenu({String? formCode}) async {
    try {
      List<SurveyMenu> listSurveySubMenu = await InspectionCertificateServices()
          .getAllSurveyMenu(formCode: formCode);
      _surveySubMenu = listSurveySubMenu;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get subMenuActive
  late String _subMenuActive;
  String get subMenuActive => _subMenuActive;
  // get subMenuActive
  late String _titleSubMenu;
  String get titleSubMenu => _titleSubMenu;

  set subMenuActive(String subMenuActive) {
    _subMenuActive = subMenuActive;
    notifyListeners();
  }

  set titleSubMenu(String titleSubMenu) {
    _titleSubMenu = titleSubMenu;
    notifyListeners();
  }

  Future<void> getSubMenuActive({String? formCode}) async {
    try {
      if (formCode != null) {
        _subMenuActive = formCode;
      } else {
        _subMenuActive = "";
      }
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  late ListSurvey _detailSurvey;

  ListSurvey get detailSurvey => _detailSurvey;

  set getDetailSurvey(ListSurvey detailSurvey) {
    _detailSurvey = detailSurvey;
    notifyListeners();
  }

  Future<bool> getDetailSurveyByID({String? id, bool? isDownload}) async {
    try {
      ListSurvey survey =
          await InspectionCertificateServices().getDetailSurveyByID(id: id);
      _detailSurvey = survey;
      notifyListeners();
      return true;
    } catch (e) {
      return false;
    }
  }

  // get surveys
  List<ListSurvey> _survey = [];
  List<ListSurvey> get survey => _survey;

  set survey(List<ListSurvey> survey) {
    _survey = survey;
    notifyListeners();
  }

  Future<void> getSurvey({String? title}) async {
    try {
      List<ListSurvey> survey =
          await InspectionCertificateServices().getSurvey(title: title);
      _survey = survey;
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
    notifyListeners();
  }

  Future<void> listGetSurvey({int? status, String? title}) async {
    try {
      // switch status
      switch (status) {
        case 1: // 1 = for done
          status = 2;
          break;
        case 2: // 2 = for in progress
          status = 1;
          break;
      }

      List<ListSurvey> survey =
          await InspectionCertificateServices().listGetSurvey(
        status: status,
        title: title,
      );
      _survey = survey;
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
    notifyListeners();
  }

  // get menu item manual book
  List<DetailManualBooks> _detailManualBooksItem = [];
  List<DetailManualBooks> get detailManualBooks => _detailManualBooksItem;

  set detailManualBooks(List<DetailManualBooks> detailManualBooks) {
    _detailManualBooksItem = detailManualBooks;
    notifyListeners();
  }

  Future<void> getManualBooksItem({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<DetailManualBooks> detailManualBooks =
          await InspectionCertificateServices().getManualBooksItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _detailManualBooksItem = detailManualBooks;
      _initializeMenu = detailManualBooks;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<bool> updateManualBook({
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool status = await InspectionCertificateServices().updateManualBook(
        titleId: titleId,
        bodyRequest: bodyRequest,
      );
      return status;
    } catch (e) {
      // ignore: avoid_print
      print("error $e");
      _errorMessage = e.toString();
      return false;
    }
  }

  // get menu item manual book
  List<SurveyStatus> _surveyStatusItem = [];
  List<SurveyStatus> get surveyStatus => _surveyStatusItem;

  set surveyStatus(List<SurveyStatus> surveyStatus) {
    _surveyStatusItem = surveyStatus;
    notifyListeners();
  }

  Future<void> getSurveyStatusItem({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<SurveyStatus> surveyStatus =
          await InspectionCertificateServices().getSurveyStatusItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _surveyStatusItem = surveyStatus;
      _initializeMenu = surveyStatus;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<bool> updateSurveyStatusItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool isSuccess =
          await InspectionCertificateServices().updateSurveyStatusItem(
        titleApi: titleApi,
        titleId: titleId,
        bodyRequest: bodyRequest,
      );
      return isSuccess;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }

  Future<void> getItemBySubMenuInspectionPhysics({
    String? titleApi,
    String? titleId,
  }) async {
    switch (titleApi) {
      case hullConstruction:
        await hullConstructionExam(titleApi: titleApi, titleId: titleId);
      case tankCondition:
        await tankConditionExam(titleApi: titleApi, titleId: titleId);
      case pipeAndValveSystemCondition:
        await pipeValveSystemExam(titleApi: titleApi, titleId: titleId);
      case roomConditions:
        await roomConditionExam(titleApi: titleApi, titleId: titleId);
      case deckEquipmentCondition:
        await deckEquipmentExam(titleApi: titleApi, titleId: titleId);
      case deckMachineryCondition:
        await deckMachinaryExam(titleApi: titleApi, titleId: titleId);
      case electricalSystemCondition:
        await electricalSystemExam(titleApi: titleApi, titleId: titleId);
      case navigationAndCommunicationEquipmentCondition:
        await navigationAndCommunicationExam(
            titleApi: titleApi, titleId: titleId);
      case safetyAndFireEquipmentCondition:
        await savetyAndFireExam(titleApi: titleApi, titleId: titleId);
      case shipMachinerySystemCondition:
        await shipMachinerySystemExam(titleApi: titleApi, titleId: titleId);
        break;
    }
  }

  // get menu item manual book
  List<HullConstruction> _hullConstructionCertificate = [];
  List<HullConstruction> get hullConstructionCertificate =>
      _hullConstructionCertificate;

  set hullConstructionCertificate(
      List<HullConstruction> hullConstructionCertificate) {
    _hullConstructionCertificate = hullConstructionCertificate;
    notifyListeners();
  }

  Future<void> hullConstructionExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<HullConstruction> hullConstructionCertificate =
          await InspectionCertificateServices().getHullConstructionItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _hullConstructionCertificate = hullConstructionCertificate;
      _initializeMenu = hullConstructionCertificate;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<bool> updateHullConstructionItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool isSuccess =
          await InspectionCertificateServices().updateHullConstructionItem(
        titleApi: titleApi,
        titleId: titleId,
        bodyRequest: bodyRequest,
      );
      return isSuccess;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }

  // get menu item manual book
  List<TankCondition> _tankConditionCertificate = [];
  List<TankCondition> get tankConditionCertificate => _tankConditionCertificate;

  set tankConditionCertificate(List<TankCondition> tankConditionCertificate) {
    _tankConditionCertificate = tankConditionCertificate;
    notifyListeners();
  }

  Future<void> tankConditionExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<TankCondition> tankConditionCertificate =
          await InspectionCertificateServices().getTankConditionItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _tankConditionCertificate = tankConditionCertificate;
      _initializeMenu = tankConditionCertificate;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<bool> updateTankConditionItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool isSuccess =
          await InspectionCertificateServices().updateTankConditionItem(
        titleApi: titleApi,
        titleId: titleId,
        bodyRequest: bodyRequest,
      );
      return isSuccess;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }

  // get menu item manual book
  List<PipeValveSystem> _pipeValveSystemCertificate = [];
  List<PipeValveSystem> get pipeValveSystemCertificate =>
      _pipeValveSystemCertificate;

  set pipeValveSystemCertificate(
      List<PipeValveSystem> pipeValveSystemCertificate) {
    _pipeValveSystemCertificate = pipeValveSystemCertificate;
    notifyListeners();
  }

  Future<void> pipeValveSystemExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<PipeValveSystem> pipeValveSystemCertificate =
          await InspectionCertificateServices().getPipeValveSystemItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _pipeValveSystemCertificate = pipeValveSystemCertificate;
      _initializeMenu = pipeValveSystemCertificate;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<bool> updatePipeValveSystemItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool isSuccess =
          await InspectionCertificateServices().updatePipeValveSystemItem(
        titleApi: titleApi,
        titleId: titleId,
        bodyRequest: bodyRequest,
      );
      return isSuccess;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }

  // get menu item kondisi ruangan
  List<RoomCondition> _roomCondition = [];
  List<RoomCondition> get roomCondition => _roomCondition;

  set roomCondition(List<RoomCondition> roomCondition) {
    _roomCondition = roomCondition;
    notifyListeners();
  }

  Future<void> roomConditionExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<RoomCondition> roomCondition =
          await InspectionCertificateServices().getRoomConditionItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _roomCondition = roomCondition;
      _initializeMenu = roomCondition;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  Future<bool> updateItem({
    String? titleApi,
    String? titleId,
    Map<String, dynamic>? bodyRequest,
  }) async {
    try {
      bool isSuccess = await InspectionCertificateServices().updateItem(
        titleApi: titleApi,
        titleId: titleId,
        bodyRequest: bodyRequest,
      );
      return isSuccess;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }

  // get menu item kondisi geladak
  List<DeckEquipment> _deckEquipment = [];
  List<DeckEquipment> get deckEquipment => _deckEquipment;

  set deckEquipment(List<DeckEquipment> deckEquipment) {
    _deckEquipment = deckEquipment;
    notifyListeners();
  }

  Future<void> deckEquipmentExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<DeckEquipment> deckEquipment =
          await InspectionCertificateServices().getDeckEquipmentItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _deckEquipment = deckEquipment;
      _initializeMenu = deckEquipment;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get menu item kondisi mesin geladak
  List<DeckMachinary> _deckMachinary = [];
  List<DeckMachinary> get deckMachinary => _deckMachinary;

  set deckMachinary(List<DeckMachinary> deckMachinary) {
    _deckMachinary = deckMachinary;
    notifyListeners();
  }

  Future<void> deckMachinaryExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<DeckMachinary> deckMachinary =
          await InspectionCertificateServices().getDeckMachinaryItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _deckMachinary = deckMachinary;
      _initializeMenu = deckMachinary;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get menu item kondisi sistem listrik
  List<ElectricalSystem> _electricalSystem = [];
  List<ElectricalSystem> get electricalSystem => _electricalSystem;

  set electricalSystem(List<ElectricalSystem> electricalSystem) {
    _electricalSystem = electricalSystem;
    notifyListeners();
  }

  Future<void> electricalSystemExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<ElectricalSystem> electricalSystem =
          await InspectionCertificateServices().getElectricalSystemItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _electricalSystem = electricalSystem;
      _initializeMenu = electricalSystem;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get menu item kondisi peralatan navigasi dan komunikasi
  List<NavigationAndCommunication> _navigationAndCommunication = [];
  List<NavigationAndCommunication> get navigationAndCommunication =>
      _navigationAndCommunication;

  set navigationAndCommunication(
      List<NavigationAndCommunication> navigationAndCommunication) {
    _navigationAndCommunication = navigationAndCommunication;
    notifyListeners();
  }

  Future<void> navigationAndCommunicationExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<NavigationAndCommunication> navigationAndCommunication =
          await InspectionCertificateServices()
              .getNavigationAndComunicationItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _navigationAndCommunication = navigationAndCommunication;
      _initializeMenu = navigationAndCommunication;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get menu item kondisi peralatan keselamatan dan pemadam kebarkaran
  List<SavetyAndFire> _savetyAndFire = [];
  List<SavetyAndFire> get savetyAndFire => _savetyAndFire;

  set savetyAndFire(List<SavetyAndFire> savetyAndFire) {
    _savetyAndFire = savetyAndFire;
    notifyListeners();
  }

  Future<void> savetyAndFireExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<SavetyAndFire> savetyAndFire =
          await InspectionCertificateServices().getSafetyAndFireItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _savetyAndFire = savetyAndFire;
      _initializeMenu = savetyAndFire;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  // get menu item manual book
  List<ShipMachinerySystem> _shipMachinerySystemCertificate = [];
  List<ShipMachinerySystem> get shipMachinerySystemCertificate =>
      _shipMachinerySystemCertificate;

  set shipMachinerySystemCertificate(
      List<ShipMachinerySystem> shipMachinerySystemCertificate) {
    _shipMachinerySystemCertificate = shipMachinerySystemCertificate;
    notifyListeners();
  }

  Future<void> shipMachinerySystemExam({
    String? titleApi,
    String? titleId,
  }) async {
    try {
      List<ShipMachinerySystem> shipMachinerySystemCertificate =
          await InspectionCertificateServices().getShipMachinerySystemItem(
        titleApi: titleApi,
        titleId: titleId,
      );
      _shipMachinerySystemCertificate = shipMachinerySystemCertificate;
      _initializeMenu = shipMachinerySystemCertificate;
      notifyListeners();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }
}
