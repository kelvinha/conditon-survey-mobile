// constant path file
const String pathImages = "assets/images/";
const String pathStorage = "assets/storage";

// status
const String inProgress = "In Progress";
const String todo = "Todo";
const String done = "Done";

// endpoint
const String basePathUrl = "https://mvcs.marines.id/api";
const String basePathUrlWithoutHTTPS = "mvcs.marines.id";
const String baseDomain = "https://mvcs.marines.id";
const String basePathUrlStorage = "https://mvcs.marines.id/storage";

// list form code
const String inspectionObjects = "inspection-objects";
const String inspectionCertificate = "inspection-certificates";
const String inspectionPhysics = "inspection-physicals";

// list menu inspection certificate
const String shipLegalities = "ship-legalities";
const String manualBooks = "manual-books";
const String surveyStatus = "survey-status";
const String statusStatutorySurvey = "status-statutory-surveys";
const String statusClassificationSurvey = "status-classification-surveys";

// list menu inspection physics
const String hullConstruction = "hull-and-construction-examinations";
const String roomConditions = "room-condition-examinations";
const String tankCondition = "tank-condition-examinations";
const String deckEquipmentCondition = "deck-equipment-condition-examinations";
const String deckMachineryCondition = "deck-machinery-condition-examinations";
const String pipeAndValveSystemCondition =
    "pipe-and-valve-system-condition-examinations";
const String electricalSystemCondition =
    "electrical-system-condition-examinations";
const String navigationAndCommunicationEquipmentCondition =
    "navigation-and-communication-equipment-condition-examinations";
const String safetyAndFireEquipmentCondition =
    "safety-and-fire-equipment-condition-examinations";
const String shipMachinerySystemCondition =
    "ship-machinery-system-condition-examinations";

// const message
const String successMessage = "Berhasil di simpan";
const String failedMessage = "Data tidak berhasil di simpan";
