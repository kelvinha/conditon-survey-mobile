import 'package:flutter/material.dart';

// color for regular
const Color primaryColor = Color(0xFF184980);
const Color secondaryColor = Color(0xFF0DB0AB);
const Color thirdColor = Color(0xFFF4E869);
const Color backgroundColor1 = Color(0xFFE2E2EA);
const Color backgroundColor2 = Color(0xFFFFFFFF);
// color for button
const Color greenColor = Color(0xFF09E83A);
const Color redColor = Color(0xFFCB1616);
const Color greyColor = Color(0xFF828282);
const Color whiteColor = Color(0xFFFFFFFF);

// text style
TextStyle regularPoppins = const TextStyle(
  fontFamily: 'Poppins',
  fontSize: 16,
  color: primaryColor,
);

TextStyle semiboldPoppins = const TextStyle(
  fontFamily: 'Poppins',
  fontSize: 16,
  color: primaryColor,
  fontWeight: FontWeight.w600,
);

TextStyle mediumPoppins = const TextStyle(
  fontFamily: 'Poppins',
  fontSize: 16,
  color: primaryColor,
  fontWeight: FontWeight.w500,
);

TextStyle lightPoppins = const TextStyle(
  fontFamily: 'Poppins',
  fontSize: 16,
  color: primaryColor,
  fontWeight: FontWeight.w300,
);
