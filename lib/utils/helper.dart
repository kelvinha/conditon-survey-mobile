import 'package:condition_survey/utils/theme.dart';
import 'package:flutter/material.dart';

class SnackbarCustom {
  static void alertMessage(
    BuildContext context,
    String message,
    int durationSeconds,
    bool isSuccess,
  ) {
    final snackBar = SnackBar(
      duration: Duration(seconds: durationSeconds),
      backgroundColor: isSuccess ? greenColor : redColor,
      content: Text(
        message,
        style: regularPoppins.copyWith(
          color: whiteColor,
        ),
        textAlign: TextAlign.center,
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}

extension StringCapitalization on String {
  String capitalize() {
    // ignore: unnecessary_this
    if (this.isEmpty) {
      return this;
    }
    // ignore: unnecessary_this
    return this[0].toUpperCase() + this.substring(1).toLowerCase();
  }
}
