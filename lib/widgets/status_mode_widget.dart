import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../utils/theme.dart';

class StatusMode extends StatelessWidget {
  const StatusMode({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final ConnectivityStatusProvider connStatusProvider =
        Provider.of<ConnectivityStatusProvider>(context, listen: true);
    return Container(
      width: 100,
      height: 30,
      decoration: const BoxDecoration(
        color: whiteColor,
        borderRadius: BorderRadius.all(
          Radius.circular(
            30,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 4,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              width: 10,
              height: 10,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      // ignore: dead_code
                      color: connStatusProvider.hasConnected
                          ? greenColor.withOpacity(0.7)
                          : greyColor
                        ..withOpacity(0.7),
                      blurRadius: 8,
                      offset: const Offset(
                        2,
                        2,
                      ))
                ],
                // ignore: dead_code
                color: connStatusProvider.hasConnected ? greenColor : greyColor,
                shape: BoxShape.circle,
              ),
            ),
            const SizedBox(
              width: 2,
            ),
            Text(
              connStatusProvider.hasConnected
                  ?
                  // ignore: dead_code
                  "Online Mode"
                  // ignore: dead_code
                  : "Offline Mode",
              style: regularPoppins.copyWith(
                fontSize: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
