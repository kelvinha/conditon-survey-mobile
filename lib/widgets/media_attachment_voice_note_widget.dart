import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:condition_survey/providers/media_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/list_audio_preview_widget.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sound/public/flutter_sound_recorder.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class MediaAttachmentVoiceNote extends StatefulWidget {
  final Function(List<String>?) onMediaSelected;
  final String titleId;
  final String formCode;
  final List<String>? currentAudio;

  const MediaAttachmentVoiceNote({
    super.key,
    required this.onMediaSelected,
    required this.titleId,
    required this.formCode,
    required this.currentAudio,
  });

  @override
  State<MediaAttachmentVoiceNote> createState() =>
      _MediaAttachmentVoiceNoteState();
}

class _MediaAttachmentVoiceNoteState extends State<MediaAttachmentVoiceNote> {
  final recorder = FlutterSoundRecorder();
  late File audioFile;
  late AudioPlayer audioPlayer;
  bool isRecorderReady = false;
  bool isLoading = false;
  List<String> pathAudio = [];
  List<String>? pathAudiotemp = [];

  @override
  void initState() {
    super.initState();
    initRecorder();
    pathAudiotemp = widget.currentAudio;
  }

  @override
  void dispose() {
    recorder.closeRecorder();
    super.dispose();
  }

  Future initRecorder() async {
    final status = await Permission.microphone.status;
    if (status != PermissionStatus.granted) {
      final result = await Permission.microphone.request();
      if (result != PermissionStatus.granted) {
        print('debug Microphone permission not granted');
        return;
      }
    }

    await recorder.openRecorder();
    isRecorderReady = true;
    recorder.setSubscriptionDuration(const Duration(milliseconds: 500));
  }

  // Future initRecorder() async {
  //   final status = await Permission.microphone.status;
  //   if (status != PermissionStatus.granted) {
  //     final result = await Permission.microphone.request();
  //     if (result != PermissionStatus.granted) {
  //       print('debug Microphone permission not granted');
  //       return;
  //     }
  //   }

  //   await recorder.openRecorder();
  //   isRecorderReady = true;
  //   recorder.setSubscriptionDuration(const Duration(milliseconds: 500));
  // }

  Future startRecording() async {
    if (!isRecorderReady) return !isRecorderReady;
    await recorder.startRecorder(toFile: 'audio.aac');
  }

  Future<bool> stopRecording() async {
    if (!isRecorderReady) return !isRecorderReady;
    final pathRecorder = await recorder.stopRecorder();
    audioFile = File(pathRecorder!);
    // play(audioFile.path);

    return true;
  }

  Future play(String path) async {
    audioPlayer = AudioPlayer();
    await audioPlayer.play(UrlSource(path));
  }

  @override
  Widget build(BuildContext context) {
    final MediaProvider mediaProvider =
        Provider.of<MediaProvider>(context, listen: false);

    Future<bool> uploadVoiceNote() async {
      setState(() {
        isLoading = true;
      });
      var isSuccess = await mediaProvider.uploadFileAudio(
        audio: audioFile,
        formCode: widget.formCode,
        name: "audio-${widget.formCode}",
        titleId: widget.titleId,
      );

      if (isSuccess) {
        setState(() {
          isLoading = false;
        });
      } else {
        setState(() {
          isLoading = false;
        });
      }
      return isSuccess;
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Attachment Voice Note:",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          isLoading
              ? const LoadingIndicator()
              : GestureDetector(
                  // record voice note
                  onTap: () async {
                    if (recorder.isRecording) {
                      var isStopped = await stopRecording();
                      if (isStopped) {
                        var isSuccess = await uploadVoiceNote();
                        if (isSuccess) {
                          // ignore: use_build_context_synchronously
                          SnackbarCustom.alertMessage(
                            context,
                            "Sukses Uploaded Audio",
                            4,
                            isSuccess,
                          );

                          pathAudio.add(mediaProvider.voiceNote);
                          widget.onMediaSelected(pathAudio);
                        } else {
                          // ignore: use_build_context_synchronously
                          SnackbarCustom.alertMessage(
                            context,
                            mediaProvider.errorMessage,
                            4,
                            isSuccess,
                          );
                        }
                      }
                    } else {
                      await startRecording();
                    }
                    setState(() {});
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 14),
                    child: Container(
                      padding: const EdgeInsets.all(8),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: whiteColor,
                        border: Border.all(
                          color: primaryColor.withOpacity(0.7),
                          width: 2,
                        ),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(8),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            recorder.isRecording
                                ? Icons.stop
                                : Icons.mic_none_sharp,
                            size: 35,
                            color: primaryColor,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          recorder.isRecording
                              ? StreamBuilder<RecordingDisposition>(
                                  stream: recorder.onProgress,
                                  builder: (context, snapshot) {
                                    final duration = snapshot.hasData
                                        ? snapshot.data!.duration
                                        : Duration.zero;
                                    final minutes = duration.inMinutes
                                        .remainder(60)
                                        .toString()
                                        .padLeft(2, '0');
                                    final seconds = duration.inSeconds
                                        .remainder(60)
                                        .toString()
                                        .padLeft(2, '0');
                                    return Text(
                                      '$minutes:$seconds',
                                      style: regularPoppins,
                                    );
                                  },
                                )
                              : Text(
                                  "Record",
                                  style: regularPoppins.copyWith(
                                    color: primaryColor,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                        ],
                      ),
                    ),
                  ),
                ),
          const SizedBox(
            height: 10,
          ),
          Visibility(
            visible: pathAudio.isNotEmpty,
            child: InkWell(
              onTap: () {
                showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return ListAudioPreview(
                      formCode: widget.formCode,
                      audioPath: pathAudio,
                    );
                  },
                );
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 14.0),
                padding: const EdgeInsets.all(8),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: secondaryColor,
                  border: Border.all(
                    color: primaryColor.withOpacity(0.7),
                    width: 2,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "Total Audio Yang terekam (${pathAudio.length})",
                      style: regularPoppins.copyWith(
                        color: whiteColor,
                        fontSize: 14,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
