import 'package:flutter/material.dart';

import '../utils/theme.dart';

class NotFound extends StatelessWidget {
  const NotFound({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.search_off,
              color: redColor,
              size: 80,
            ),
            Text(
              "Kami tidak memiliki informasi yang sesuai dengan permintaan Anda.",
              style: regularPoppins,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ],
    );
  }
}
