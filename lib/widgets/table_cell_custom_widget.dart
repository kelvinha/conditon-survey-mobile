import 'package:flutter/material.dart';

import '../utils/theme.dart';

class TableCellCustom extends StatelessWidget {
  final String firstColumn;
  final String secondColumn;

  const TableCellCustom({
    required this.firstColumn,
    required this.secondColumn,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 50,
          width: (MediaQuery.of(context).size.width * 0.5) - 20,
          decoration: BoxDecoration(
            color: backgroundColor1,
            border: Border.all(
              color: greyColor.withOpacity(0.4),
              width: 1,
            ),
          ),
          child: Center(
            child: Text(
              firstColumn,
              style: semiboldPoppins.copyWith(
                color: primaryColor,
                fontSize: 12,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Container(
          height: 50,
          width: (MediaQuery.of(context).size.width * 0.5) - 20,
          decoration: BoxDecoration(
            color: whiteColor,
            border: Border.all(
              color: greyColor.withOpacity(0.4),
              width: 1,
            ),
          ),
          child: Center(
            child: Text(
              secondColumn,
              style: semiboldPoppins.copyWith(
                color: primaryColor,
                fontSize: 12,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }
}
