import 'package:condition_survey/providers/connectivity_provider.dart';
import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../pages/dashboard/detail_survey_page.dart';
import '../utils/constant.dart';
import '../utils/theme.dart';

class ListSurveyWidget extends StatefulWidget {
  final String titleShip;
  final String location;
  final String dateRange;
  final int status;
  final int titleId;
  final String inspectorName;

  const ListSurveyWidget({
    required this.titleShip,
    required this.location,
    required this.dateRange,
    required this.status,
    required this.titleId,
    required this.inspectorName,
    super.key,
  });

  @override
  State<ListSurveyWidget> createState() => _ListSurveyWidgetState();
}

class _ListSurveyWidgetState extends State<ListSurveyWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          CupertinoPageRoute(
            builder: (context) => DetailSurveyPage(
              surveyTitle: widget.titleShip,
              surveyTitleId: widget.titleId.toString(),
            ),
          ),
        );
      },
      child: CardSurveyWidget(widget: widget),
    );
  }
}

class CardSurveyWidget extends StatelessWidget {
  const CardSurveyWidget({
    super.key,
    required this.widget,
  });

  final ListSurveyWidget widget;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        bottom: 15,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: greyColor.withOpacity(0.5),
            offset: const Offset(
              5,
              8,
            ),
            blurRadius: 2.0,
          )
        ],
        borderRadius: const BorderRadius.all(
          Radius.circular(8),
        ),
        color: whiteColor,
      ),
      child: Row(
        children: [
          const ShipLogo(),
          ContentSurveyWidget(widget: widget),
        ],
      ),
    );
  }
}

class ContentSurveyWidget extends StatelessWidget {
  const ContentSurveyWidget({
    super.key,
    required this.widget,
  });

  final ListSurveyWidget widget;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.titleShip.toUpperCase(),
          style: mediumPoppins.copyWith(
            fontSize: 18,
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width / 1.75,
          child: Text(
            widget.location,
            style: lightPoppins.copyWith(
              fontSize: 12,
            ),
          ),
        ),
        Text(
          widget.dateRange,
          style: lightPoppins.copyWith(
            fontSize: 12,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        StatusSurveyWidget(widget: widget),
      ],
    );
  }
}

class StatusSurveyWidget extends StatefulWidget {
  const StatusSurveyWidget({
    super.key,
    required this.widget,
  });

  final ListSurveyWidget widget;

  @override
  State<StatusSurveyWidget> createState() => _StatusSurveyWidgetState();
}

class _StatusSurveyWidgetState extends State<StatusSurveyWidget> {
  @override
  Widget build(BuildContext context) {
    String? messageAlert;
    bool isSuccess = false;
    final DashboardProvider survey =
        Provider.of<DashboardProvider>(context, listen: false);
    final ConnectivityStatusProvider connStatusProvider =
        Provider.of<ConnectivityStatusProvider>(context, listen: true);

    Future<bool> handleUpdateStatusSurvey(
      Map<String, dynamic> requestBody,
    ) async {
      isSuccess = await survey.updateStatusSurvey(
        bodyRequest: requestBody,
        titleId: widget.widget.titleId.toString(),
      );

      if (isSuccess) {
        messageAlert = 'Selamat mengerjakan!';
      } else {
        messageAlert = 'Oops, terjadi kesalahan';
      }

      // ignore: avoid_print
      print(requestBody);

      return isSuccess;
    }

    void showAlertDialog(
        BuildContext context, Map<String, dynamic> requestBody) {
      showCupertinoModalPopup<bool>(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(
            'Alert',
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          content: Text(
            'Apakah kamu yakin ?',
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          actions: <CupertinoDialogAction>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'No',
                style: regularPoppins.copyWith(
                  color: redColor,
                ),
              ),
            ),
            CupertinoDialogAction(
              isDestructiveAction: true,
              onPressed: () async {
                bool isSuccess = await handleUpdateStatusSurvey(requestBody);
                // ignore: use_build_context_synchronously
                SnackbarCustom.alertMessage(
                  context,
                  messageAlert!,
                  2,
                  isSuccess,
                );
                // ignore: use_build_context_synchronously
                Navigator.pop(context);
              },
              child: Text(
                'Yes',
                style: regularPoppins.copyWith(
                  color: Colors.blue,
                ),
              ),
            ),
          ],
        ),
      );
    }

    void showAlertDialogDownload(BuildContext context) {
      showCupertinoModalPopup<bool>(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(
            'Alert',
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          content: Text(
            'Apakah kamu yakin ingin mengunduh survey ini?',
            style: regularPoppins.copyWith(
              color: Colors.black,
            ),
          ),
          actions: <CupertinoDialogAction>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'No',
                style: regularPoppins.copyWith(
                  color: redColor,
                ),
              ),
            ),
            CupertinoDialogAction(
              isDestructiveAction: true,
              onPressed: () async {
                // ignore: use_build_context_synchronously
                Navigator.pop(context);
                showDialog(
                  context: context,
                  builder: (context) {
                    return ContentDownload(widget: widget.widget);
                  },
                );
              },
              child: Text(
                'Yes',
                style: regularPoppins.copyWith(
                  color: Colors.blue,
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Row(
      children: [
        Container(
          height: 30,
          width: widget.widget.status == 0
              ? MediaQuery.of(context).size.width / 5.0
              : connStatusProvider.hasConnected
                  ? MediaQuery.of(context).size.width / 2.1
                  : MediaQuery.of(context).size.width / 1.75,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(8),
            ),
            color: _statusColor(widget.widget.status),
          ),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              _status(widget.widget.status),
              style: widget.widget.status == 1
                  ? mediumPoppins.copyWith(
                      fontSize: 14,
                      color: whiteColor,
                    )
                  : mediumPoppins.copyWith(fontSize: 14),
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Visibility(
          visible: widget.widget.status == 0,
          child: GestureDetector(
            onTap: () {
              Map<String, dynamic> bodyRequest = {
                'title': widget.widget.titleShip,
                'location': widget.widget.location,
                'date': widget.widget.dateRange,
                'inspector_name': widget.widget.inspectorName,
              };

              showAlertDialog(context, bodyRequest);

              setState(() {});
            },
            child: Container(
              height: 30,
              width: connStatusProvider.hasConnected
                  ? MediaQuery.of(context).size.width / 3.5
                  : MediaQuery.of(context).size.width / 2.6,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.5),
                    blurRadius: 6,
                    offset: const Offset(
                      2,
                      4,
                    ),
                  )
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(8),
                ),
                color: primaryColor,
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  "Start Survey",
                  style: mediumPoppins.copyWith(
                    fontSize: 14,
                    color: whiteColor,
                  ),
                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: connStatusProvider.hasConnected,
          child: Row(
            children: [
              const SizedBox(
                width: 10,
              ),
              InkWell(
                onTap: () {
                  showAlertDialogDownload(context);
                },
                child: Container(
                  padding: const EdgeInsets.all(6.0),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        blurRadius: 6,
                        offset: const Offset(
                          2,
                          4,
                        ),
                      )
                    ],
                    borderRadius: const BorderRadius.all(
                      Radius.circular(6),
                    ),
                    color: secondaryColor,
                  ),
                  child: const Icon(
                    Icons.download,
                    color: whiteColor,
                    size: 18,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class ContentDownload extends StatefulWidget {
  final ListSurveyWidget widget;
  const ContentDownload({
    super.key,
    required this.widget,
  });

  @override
  State<ContentDownload> createState() => _ContentDownloadState();
}

class _ContentDownloadState extends State<ContentDownload> {
  bool isSuccessDetailMenu = false;
  bool isSuccessListMenu = false;

  @override
  void initState() {
    super.initState();
    downloadDetailMenu();
    // downloadListMenu();
  }

  Future<void> downloadDetailMenu() async {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    await inspectionCertificateProvider.getSurveyMenu(isDownload: true);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? checkMenu = prefs.getString('survey_menu');

    if (checkMenu!.isNotEmpty) {
      setState(() {
        isSuccessDetailMenu = true;
      });
    } else {
      setState(() {
        isSuccessDetailMenu = false;
      });
    }
  }

  Future<void> downloadListMenu() async {
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);
    await inspectionCertificateProvider.getDetailSurveyByID(
        id: widget.widget.titleId.toString(), isDownload: true);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? checkMenu = prefs.getString('list_menu');

    if (checkMenu!.isNotEmpty) {
      setState(() {
        isSuccessListMenu = true;
      });
    } else {
      setState(() {
        isSuccessListMenu = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Download | ${widget.widget.titleShip}",
            style: regularPoppins,
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "List Menu",
                style: regularPoppins,
              ),
              isSuccessDetailMenu
                  ? const Icon(
                      Icons.verified,
                      color: greenColor,
                    )
                  : const Icon(
                      Icons.verified,
                      color: greyColor,
                    ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Detail Menu",
                style: regularPoppins,
              ),
              isSuccessDetailMenu
                  ? const Icon(
                      Icons.verified,
                      color: greenColor,
                    )
                  : const Icon(
                      Icons.verified,
                      color: greyColor,
                    ),
            ],
          ),
        ],
      ),
    );
  }
}

class ShipLogo extends StatelessWidget {
  const ShipLogo({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Row(
        children: [
          Image.asset(
            "${pathImages}ship.png",
            width: 60,
          ),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }
}

String _status(int status) {
  switch (status) {
    case 0:
      return todo;
    case 2:
      return inProgress;
    case 1:
      return done;
    default:
      return 'Unknown';
  }
}

Color _statusColor(int status) {
  switch (status) {
    case 0:
      return backgroundColor1;
    case 2:
      return thirdColor;
    case 1:
      return secondaryColor;
    default:
      return backgroundColor1;
  }
}
