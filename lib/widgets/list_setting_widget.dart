import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utils/theme.dart';

class ListSetting extends StatefulWidget {
  final String nameSetting;
  final IconData iconSetting;
  final Widget? destinationPage;

  const ListSetting({
    required this.nameSetting,
    required this.iconSetting,
    this.destinationPage,
    super.key,
  });

  @override
  State<ListSetting> createState() => _ListSettingState();
}

class _ListSettingState extends State<ListSetting> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: GestureDetector(
        onTap: () {
          if (widget.destinationPage != null) {
            Navigator.push(
              context,
              CupertinoPageRoute(builder: (context) => widget.destinationPage!),
            );
          }
        },
        child: Column(
          children: [
            Container(
              color: Colors.transparent,
              child: Row(
                children: [
                  Icon(
                    widget.iconSetting,
                    size: 30,
                    color: primaryColor,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(
                    widget.nameSetting,
                    style: lightPoppins,
                  ),
                  const Spacer(),
                  const Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: primaryColor,
                    size: 20,
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 42,
                  ),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width -
                        90, // Set the width of the Divider
                    child: Divider(
                      height: 20,
                      color: greyColor.withOpacity(0.5),
                      thickness: 2,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
