import 'package:flutter/material.dart';

import '../utils/theme.dart';

class CountData extends StatefulWidget {
  final String? title;
  final int? total;

  const CountData({
    super.key,
    this.title,
    this.total,
  });

  @override
  State<CountData> createState() => _CountDataState();
}

class _CountDataState extends State<CountData> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: [
          Text(
            widget.title!,
            style: lightPoppins.copyWith(
              color: whiteColor,
              fontSize: 12,
            ),
          ),
          const SizedBox(
            height: 2,
          ),
          Text(
            widget.total.toString(),
            style: lightPoppins.copyWith(
              color: secondaryColor,
            ),
          ),
        ],
      ),
    );
  }
}
