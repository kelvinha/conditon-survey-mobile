// ignore_for_file: avoid_print
import 'package:condition_survey/providers/media_provider.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/not_found_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:provider/provider.dart';

class PdfViewerPage extends StatefulWidget {
  final String urlPdf;
  final String formCode;

  const PdfViewerPage({
    super.key,
    required this.urlPdf,
    required this.formCode,
  });

  @override
  State<PdfViewerPage> createState() => _PdfViewerPageState();
}

class _PdfViewerPageState extends State<PdfViewerPage> {
  late Future<bool> _statusUrl;
  @override
  void initState() {
    super.initState();
    _statusUrl = checkUrlStorage();
  }

  Future<bool> checkUrlStorage() async {
    String url = "$basePathUrlStorage/${widget.formCode}/file/${widget.urlPdf}";
    final MediaProvider mediaProvider =
        Provider.of<MediaProvider>(context, listen: false);
    var isSuccess = await mediaProvider.checkUrlStorage(url: url);
    return isSuccess;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        foregroundColor: primaryColor,
        title: Text('PDF Viewer', style: semiboldPoppins),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: _statusUrl,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else {
            bool statusUrl = snapshot.data!;
            return statusUrl
                ? Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: PDF(
                      enableSwipe: true,
                      swipeHorizontal: false,
                      autoSpacing: false,
                      pageFling: false,
                      fitEachPage: true,
                      nightMode: false,
                      onError: (error) {
                        print("debug ${error.toString()}");
                      },
                      onPageError: (page, error) {
                        print("debug page ${error.toString()}");
                      },
                      onPageChanged: (int? page, int? total) {
                        print('page change: $page/$total');
                      },
                    ).cachedFromUrl(
                        "$basePathUrlStorage/${widget.formCode}/file/${widget.urlPdf}"),
                  )
                : const Center(
                    child: NotFound(),
                  );
          }
        },
      ),
    );
  }
}
