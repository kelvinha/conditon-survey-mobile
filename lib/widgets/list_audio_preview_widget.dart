import 'package:audioplayers/audioplayers.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:flutter/material.dart';

class ListAudioPreview extends StatefulWidget {
  const ListAudioPreview({
    super.key,
    required this.formCode,
    required this.audioPath,
  });

  final String formCode;
  final List<String> audioPath;

  @override
  State<ListAudioPreview> createState() => _ListAudioPreviewState();
}

class _ListAudioPreviewState extends State<ListAudioPreview> {
  final audioPlayer = AudioPlayer();
  int? currentIndex; // index dari item yang sedang diputar

  @override
  void dispose() {
    super.dispose();
    audioPlayer.stop();
  }

  Future play(String path, int index) async {
    await audioPlayer.play(UrlSource(path));
    setState(() {
      currentIndex = index; // set index dari item yang sedang diputar
    });
    audioPlayer.onPlayerComplete.listen((event) {
      setState(() {
        currentIndex = null;
      });
    });
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      currentIndex = null; // reset index ketika audio dihentikan
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: List.generate(
          widget.audioPath.length,
          (index) => ListTile(
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                currentIndex == index
                    ? const Icon(
                        Icons.pause_circle_outline_outlined,
                        color: secondaryColor,
                        size: 28,
                      )
                    : const Icon(
                        Icons.play_circle_outline_outlined,
                        color: secondaryColor,
                        size: 28,
                      ),
                IconButton(
                  icon: const Icon(
                    Icons.delete_outline,
                    color: redColor,
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            leading: const Icon(
              Icons.label_important_outline_sharp,
              color: secondaryColor,
            ),
            title: Text(
              "Rekaman audio ke-${index + 1}",
              style: regularPoppins,
            ),
            onTap: () async {
              if (currentIndex != index) {
                if (currentIndex != null) {
                  await stop(); // hentikan audio yang sedang diputar sebelumnya
                }
                play(
                  "$basePathUrlStorage/${widget.formCode}/audio/${widget.audioPath[index]}",
                  index,
                );
              } else {
                await stop(); // hentikan audio jika item diklik lagi
              }
            },
          ),
        ),
      ),
    );
  }
}
