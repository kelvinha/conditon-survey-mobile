import 'package:condition_survey/providers/media_provider.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/not_found_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ImageFullScreenViewer extends StatefulWidget {
  final String imageUrl;
  final String formCode;

  const ImageFullScreenViewer({
    Key? key,
    required this.imageUrl,
    required this.formCode,
  }) : super(key: key);

  @override
  State<ImageFullScreenViewer> createState() => _ImageFullScreenViewerState();
}

class _ImageFullScreenViewerState extends State<ImageFullScreenViewer> {
  late Future<bool> _statusUrl;
  @override
  void initState() {
    super.initState();
    _statusUrl = checkUrlStorage();
  }

  Future<bool> checkUrlStorage() async {
    String url =
        "$basePathUrlStorage/${widget.formCode}/image/${widget.imageUrl}";
    final MediaProvider mediaProvider =
        Provider.of<MediaProvider>(context, listen: false);
    var isSuccess = await mediaProvider.checkUrlStorage(url: url);
    return isSuccess;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Image Viewer',
          style: semiboldPoppins.copyWith(
            fontSize: 20,
          ),
        ),
        foregroundColor: primaryColor,
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: _statusUrl,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else {
            bool statusUrl = snapshot.data!;
            return !statusUrl
                ? const Center(
                    child: NotFound(),
                  )
                : GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Center(
                      child: Image.network(
                        "$basePathUrlStorage/${widget.formCode}/image/${widget.imageUrl}",
                        fit: BoxFit.contain,
                      ),
                    ),
                  );
          }
        },
      ),
    );
  }
}
