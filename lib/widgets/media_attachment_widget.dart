import 'package:audioplayers/audioplayers.dart';
import 'package:condition_survey/utils/constant.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/image_viewer_widget.dart';
import 'package:condition_survey/widgets/pdf_viewer_widget.dart';
import 'package:flutter/material.dart';

class MediaAttachment extends StatefulWidget {
  final dynamic data;
  final String formCode;
  const MediaAttachment({
    super.key,
    required this.data,
    required this.formCode,
  });

  @override
  State<MediaAttachment> createState() => _MediaAttachmentState();
}

class _MediaAttachmentState extends State<MediaAttachment> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        InkWell(
          // file pdf
          onTap: () {
            if (widget.data.filePath!.isNotEmpty) {
              showModalBottomSheet(
                context: context,
                builder: (BuildContext context) {
                  return Container(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: List.generate(
                        widget.data.filePath!.length,
                        (index) => ListTile(
                          leading: const Icon(
                            Icons.label_important_outline_sharp,
                            color: secondaryColor,
                          ),
                          title: Text(
                            "Buka berkas PDF ke-${index + 1}",
                            style: regularPoppins,
                          ),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PdfViewerPage(
                                  urlPdf: widget.data.filePath![index],
                                  formCode: widget.formCode,
                                ),
                                fullscreenDialog: true,
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  );
                },
              );
            } else {
              // ignore: use_build_context_synchronously
              SnackbarCustom.alertMessage(
                context,
                "Berkas tidak tersedia",
                1,
                false,
              );
            }
          },
          child: Container(
            padding: const EdgeInsets.all(8),
            width: MediaQuery.of(context).size.width / 4,
            decoration: BoxDecoration(
              border: Border.all(
                color: primaryColor.withOpacity(0.7),
                width: 2,
              ),
              borderRadius: const BorderRadius.all(
                Radius.circular(8),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                const Icon(
                  Icons.picture_as_pdf_outlined,
                  size: 35,
                  color: primaryColor,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "${widget.data.filePath!.length} Files",
                  style: regularPoppins.copyWith(),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
        InkWell(
          // file image
          onTap: () {
            if (widget.data.imagesPath!.isNotEmpty) {
              showModalBottomSheet(
                context: context,
                builder: (BuildContext context) {
                  return Container(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: List.generate(
                        widget.data.imagesPath!.length,
                        (index) => ListTile(
                          leading: const Icon(
                            Icons.label_important_outline_sharp,
                            color: secondaryColor,
                          ),
                          title: Text(
                            "Buka berkas gambar ke-${index + 1}",
                            style: regularPoppins,
                          ),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ImageFullScreenViewer(
                                  imageUrl: widget.data.imagesPath![index],
                                  formCode: widget.formCode,
                                ),
                                fullscreenDialog: true,
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  );
                },
              );
            } else {
              // ignore: use_build_context_synchronously
              SnackbarCustom.alertMessage(
                context,
                "Gambar tidak tersedia",
                1,
                false,
              );
            }
          },
          child: Container(
            padding: const EdgeInsets.all(8),
            width: MediaQuery.of(context).size.width / 4,
            decoration: BoxDecoration(
              border: Border.all(
                color: primaryColor.withOpacity(0.7),
                width: 2,
              ),
              borderRadius: const BorderRadius.all(
                Radius.circular(8),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                const Icon(
                  Icons.image,
                  size: 35,
                  color: primaryColor,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "${widget.data.imagesPath!.length} Files",
                  style: regularPoppins.copyWith(),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
        InkWell(
          // file voice note
          onTap: () {
            if (widget.data.voiceNotePath!.isNotEmpty) {
              showModalBottomSheet(
                context: context,
                builder: (BuildContext context) {
                  return ListAudioTile(widget: widget);
                },
              );
            } else {
              // ignore: use_build_context_synchronously
              SnackbarCustom.alertMessage(
                context,
                "Audio tidak tersedia",
                1,
                false,
              );
            }
          },
          child: Container(
            padding: const EdgeInsets.all(8),
            width: MediaQuery.of(context).size.width / 4,
            decoration: BoxDecoration(
              border: Border.all(
                color: primaryColor.withOpacity(0.7),
                width: 2,
              ),
              borderRadius: const BorderRadius.all(
                Radius.circular(8),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                const Icon(
                  Icons.play_circle_outline_sharp,
                  size: 35,
                  color: primaryColor,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "${widget.data.voiceNotePath!.length} Files",
                  style: regularPoppins.copyWith(),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class ListAudioTile extends StatefulWidget {
  const ListAudioTile({
    super.key,
    required this.widget,
  });

  final MediaAttachment widget;

  @override
  State<ListAudioTile> createState() => _ListAudioTileState();
}

class _ListAudioTileState extends State<ListAudioTile> {
  final audioPlayer = AudioPlayer();
  int? currentIndex; // index dari item yang sedang diputar

  @override
  void dispose() {
    super.dispose();
    audioPlayer.stop();
  }

  Future play(String path, int index) async {
    await audioPlayer.play(UrlSource(path));
    setState(() {
      currentIndex = index; // set index dari item yang sedang diputar
    });
    audioPlayer.onPlayerComplete.listen((event) {
      setState(() {
        currentIndex = null;
      });
    });
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      currentIndex = null; // reset index ketika audio dihentikan
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: List.generate(
          widget.widget.data.voiceNotePath!.length,
          (index) => ListTile(
            trailing: currentIndex == index // ubah ikon hanya jika index sesuai
                ? const Icon(
                    Icons.pause_circle_outline_outlined,
                    color: secondaryColor,
                    size: 28,
                  )
                : const Icon(
                    Icons.play_circle_outline_outlined,
                    color: secondaryColor,
                    size: 28,
                  ),
            leading: const Icon(
              Icons.label_important_outline_sharp,
              color: secondaryColor,
            ),
            title: Text(
              "Rekaman audio ke-${index + 1}",
              style: regularPoppins,
            ),
            onTap: () async {
              if (currentIndex != index) {
                if (currentIndex != null) {
                  await stop(); // hentikan audio yang sedang diputar sebelumnya
                }
                play(
                  "$basePathUrlStorage/${widget.widget.formCode}/audio/${widget.widget.data.voiceNotePath![index]}",
                  index,
                );
              } else {
                await stop(); // hentikan audio jika item diklik lagi
              }
            },
          ),
        ),
      ),
    );
  }
}
