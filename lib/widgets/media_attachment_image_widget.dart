import 'package:condition_survey/providers/media_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/image_viewer_widget.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class MediaAttachmentImage extends StatefulWidget {
  final Function(List<String>?) onMediaSelected;
  final String titleId;
  final String formCode;
  final int totalCurrentImage;
  final List<String> currentImage;

  const MediaAttachmentImage({
    super.key,
    required this.onMediaSelected,
    required this.titleId,
    required this.formCode,
    required this.totalCurrentImage,
    required this.currentImage,
  });

  @override
  State<MediaAttachmentImage> createState() => _MediaAttachmentImageState();
}

class _MediaAttachmentImageState extends State<MediaAttachmentImage> {
  bool isLoading = false;
  FilePickerResult? resultPDF;
  List<XFile>? resultImage = [];
  XFile? resultImageCamera;
  List<String> listPathImages = [];
  List<String> totalImage = [];

  Future pickImage() async {
    resultImage = await ImagePicker().pickMultiImage();
    if (resultImage!.isEmpty) return;
  }

  Future pickImageFromCamera() async {
    resultImageCamera =
        await ImagePicker().pickImage(source: ImageSource.camera);
    if (resultImageCamera == null) return;
  }

  @override
  Widget build(BuildContext context) {
    final MediaProvider mediaProvider =
        Provider.of<MediaProvider>(context, listen: false);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Attachment Image:",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              isLoading
                  ? const LoadingIndicator()
                  : GestureDetector(
                      // add images
                      onTap: () async {
                        showModalBottomSheet(
                          isDismissible: isLoading,
                          context: context,
                          builder: (BuildContext context) {
                            return Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 8.0,
                                vertical: 20.0,
                              ),
                              width: double.infinity,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      IconButton(
                                        onPressed: () async {
                                          await pickImage();
                                          setState(() {
                                            isLoading = true;
                                          });
                                          listPathImages = [];
                                          // upload file pdf ke API
                                          var isSuccess = await mediaProvider
                                              .uploadFileImage(
                                            titleId: widget.titleId,
                                            listImages: resultImage,
                                            formCode: widget.formCode,
                                            name: 'image',
                                          );

                                          if (isSuccess) {
                                            for (var item
                                                in mediaProvider.images) {
                                              listPathImages.add(item.path);
                                              totalImage.add(item.path);
                                            }
                                            // ignore: use_build_context_synchronously
                                            SnackbarCustom.alertMessage(
                                              context,
                                              "Success Uploaded Image",
                                              4,
                                              isSuccess,
                                            );
                                          } else {
                                            // ignore: use_build_context_synchronously
                                            SnackbarCustom.alertMessage(
                                              context,
                                              mediaProvider.errorMessage,
                                              4,
                                              isSuccess,
                                            );

                                            listPathImages = [];

                                            widget.onMediaSelected(
                                                listPathImages);
                                          }

                                          widget
                                              .onMediaSelected(listPathImages);

                                          setState(() {
                                            isLoading = false;
                                          });

                                          // ignore: use_build_context_synchronously
                                          Navigator.pop(context);
                                        },
                                        icon: const Icon(
                                          Icons.image_outlined,
                                          color: primaryColor,
                                          size: 60,
                                        ),
                                      ),
                                      Text(
                                        "Gallery",
                                        style: regularPoppins,
                                      )
                                    ],
                                  ),
                                  Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      IconButton(
                                        onPressed: () async {
                                          await pickImageFromCamera();
                                          setState(() {
                                            isLoading = true;
                                          });
                                          listPathImages = [];
                                          // upload photo pdf ke API
                                          var isSuccess = await mediaProvider
                                              .uploadFileImage(
                                            titleId: widget.titleId,
                                            listImages: [resultImageCamera!],
                                            formCode: widget.formCode,
                                            name: 'camera',
                                          );

                                          if (isSuccess) {
                                            for (var item
                                                in mediaProvider.images) {
                                              listPathImages.add(item.path);
                                              totalImage.add(item.path);
                                            }
                                            // ignore: use_build_context_synchronously
                                            SnackbarCustom.alertMessage(
                                              context,
                                              "Success Uploaded Image",
                                              2,
                                              isSuccess,
                                            );

                                            widget.onMediaSelected(
                                                listPathImages);
                                          } else {
                                            // ignore: use_build_context_synchronously
                                            SnackbarCustom.alertMessage(
                                              context,
                                              mediaProvider.errorMessage,
                                              4,
                                              isSuccess,
                                            );
                                            listPathImages = [];
                                          }

                                          setState(() {
                                            isLoading = false;
                                          });
                                          // ignore: use_build_context_synchronously
                                          Navigator.pop(context);
                                        },
                                        icon: const Icon(
                                          Icons.photo_camera_outlined,
                                          color: primaryColor,
                                          size: 60,
                                        ),
                                      ),
                                      Text(
                                        "Camera",
                                        style: regularPoppins,
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        width: MediaQuery.of(context).size.width / 2,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: primaryColor.withOpacity(0.7),
                            width: 2,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Icon(
                              Icons.image,
                              size: 35,
                              color: primaryColor,
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              "Add",
                              style: regularPoppins,
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ),
              GestureDetector(
                // view file pdf
                onTap: () async {
                  if (widget.totalCurrentImage != 0) {
                    showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          padding: const EdgeInsets.all(16),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: List.generate(
                              widget.totalCurrentImage,
                              (index) => ListTile(
                                leading: const Icon(
                                  Icons.label_important_outline_sharp,
                                  color: secondaryColor,
                                ),
                                title: Text(
                                  "Gambar ke-${index + 1}",
                                  style: regularPoppins,
                                ),
                                trailing: IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.delete_outline,
                                    color: redColor,
                                  ),
                                ),
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          ImageFullScreenViewer(
                                        imageUrl: widget.currentImage[index],
                                        formCode: widget.formCode,
                                      ),
                                      fullscreenDialog: true,
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                },
                child: Container(
                  padding: const EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width / 5,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: widget.totalCurrentImage != 0
                          ? primaryColor.withOpacity(0.7)
                          : greyColor.withOpacity(0.5),
                      width: 2,
                    ),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(8),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.image,
                        size: 35,
                        color: widget.totalCurrentImage != 0
                            ? primaryColor
                            : greyColor.withOpacity(0.5),
                      ),
                      Text(
                        "View",
                        style: regularPoppins.copyWith(
                          color: widget.totalCurrentImage != 0
                              ? primaryColor
                              : greyColor.withOpacity(0.5),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Visibility(
            visible: totalImage.isNotEmpty,
            child: InkWell(
              onTap: () {
                showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return Container(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: List.generate(
                          totalImage.length,
                          (index) => ListTile(
                            leading: const Icon(
                              Icons.label_important_outline_sharp,
                              color: secondaryColor,
                            ),
                            title: Text(
                              "Preview gambar ke-${index + 1}",
                              style: regularPoppins,
                            ),
                            trailing: IconButton(
                              onPressed: () {},
                              icon: const Icon(
                                Icons.delete_outline,
                                color: redColor,
                              ),
                            ),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ImageFullScreenViewer(
                                    imageUrl: listPathImages[index],
                                    formCode: widget.formCode,
                                  ),
                                  fullscreenDialog: true,
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 10.0),
                padding: const EdgeInsets.all(8),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: secondaryColor,
                  border: Border.all(
                    color: primaryColor.withOpacity(0.7),
                    width: 2,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "Preview Gambar Yang Terunggah (${totalImage.length})",
                      style: regularPoppins.copyWith(
                        color: whiteColor,
                        fontSize: 14,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
