import 'package:condition_survey/providers/inspection_certificate_provider.dart';
import 'package:condition_survey/providers/media_provider.dart';
import 'package:condition_survey/utils/helper.dart';
import 'package:condition_survey/utils/theme.dart';
import 'package:condition_survey/widgets/loading_indicator_widget.dart';
import 'package:condition_survey/widgets/pdf_viewer_widget.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MediaAttachmentPDF extends StatefulWidget {
  final Function(List<String>?) onMediaSelected;
  final Function(List<String>?) onMediaSelectedForDeleted;
  final String titleId;
  final String formCode;
  final int totalCurrentFile;
  final List<String>? currentFile;
  final int itemId;

  const MediaAttachmentPDF({
    super.key,
    required this.onMediaSelected,
    required this.onMediaSelectedForDeleted,
    required this.titleId,
    required this.formCode,
    required this.totalCurrentFile,
    required this.itemId,
    this.currentFile,
  });

  @override
  State<MediaAttachmentPDF> createState() => _MediaAttachmentPDFState();
}

class _MediaAttachmentPDFState extends State<MediaAttachmentPDF> {
  bool isLoading = false;
  FilePickerResult? resultPDF;
  List<String> listPdf = [];
  List<String> listPdfDeleted = [];
  List<String> listPdfTemp = [];
  String messageAlert = "";

  @override
  void initState() {
    super.initState();
    listPdfTemp = widget.currentFile!;
  }

  @override
  Widget build(BuildContext context) {
    final MediaProvider mediaProvider =
        Provider.of<MediaProvider>(context, listen: false);
    final InspectionCertificateProvider inspectionCertificateProvider =
        Provider.of<InspectionCertificateProvider>(context, listen: false);

    Future<bool> deleteFilePdf() async {
      Map<String, dynamic> bodyRequest = {
        'form_code': widget.formCode,
        'item_id': widget.itemId,
        'type': 'file',
        'files': listPdfDeleted
      };

      var isSuccess = await mediaProvider.deleteFile(
        bodyRequest: bodyRequest,
        titleId: widget.titleId,
      );

      if (isSuccess) {
        messageAlert = "Berhasil di simpan";
      } else {
        messageAlert = "Data tidak berhasil di simpan";
      }

      return isSuccess;
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Attachment PDF:",
            style: mediumPoppins.copyWith(
              fontSize: 14,
              color: primaryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              isLoading
                  ? const LoadingIndicator()
                  : GestureDetector(
                      // add file pdf
                      onTap: () async {
                        resultPDF = await FilePicker.platform.pickFiles(
                          allowMultiple: true,
                          allowedExtensions: ['pdf'],
                          type: FileType.custom,
                        );
                        if (resultPDF == null) return;
                        // var total = resultPDF?.files.length;
                        // if (total! > 10) {
                        //   resultPDF = null;
                        //   // ignore: use_build_context_synchronously
                        //   SnackbarCustom.alertMessage(
                        //     context,
                        //     "Maks. hanya 10 File",
                        //     2,
                        //     false,
                        //   );
                        // }

                        setState(() {
                          isLoading = true;
                        });
                        // upload file pdf ke API
                        var isSuccess = await mediaProvider.uploadFilePDF(
                          titleId: widget.titleId,
                          filePdf: resultPDF,
                          formCode: widget.formCode,
                          name: 'test',
                        );

                        if (isSuccess) {
                          for (var item in mediaProvider.filePdfs) {
                            listPdf.add(item.path);
                          }
                          widget.onMediaSelected(listPdf);
                          // ignore: use_build_context_synchronously
                          SnackbarCustom.alertMessage(
                            context,
                            "Success Uploaded PDF",
                            2,
                            isSuccess,
                          );
                        } else {
                          resultPDF = null;
                          widget.onMediaSelected(listPdf);
                          // ignore: use_build_context_synchronously
                          SnackbarCustom.alertMessage(
                            context,
                            inspectionCertificateProvider.errorMessage,
                            4,
                            isSuccess,
                          );
                        }

                        setState(() {
                          isLoading = false;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        width: MediaQuery.of(context).size.width / 2,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: primaryColor.withOpacity(0.7),
                            width: 2,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Icon(
                              Icons.picture_as_pdf_outlined,
                              size: 35,
                              color: primaryColor,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Add",
                              style: regularPoppins,
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ),
              GestureDetector(
                // view file pdf
                onTap: () async {
                  if (listPdfTemp.length != 0) {
                    showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          padding: const EdgeInsets.all(16),
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: List.generate(
                                listPdfTemp.length,
                                (index) => ListTile(
                                  leading: const Icon(
                                    Icons.label_important_outline_sharp,
                                    color: secondaryColor,
                                  ),
                                  title: Text(
                                    "Buka berkas PDF ke-${index + 1}",
                                    style: regularPoppins,
                                  ),
                                  trailing: IconButton(
                                    onPressed: () async {
                                      listPdfDeleted = [];
                                      listPdfDeleted.add(listPdfTemp[index]);
                                      print("list pdf : $listPdfDeleted");
                                      var isSuccess = await deleteFilePdf();
                                      if (isSuccess) {
                                        setState(() {
                                          widget.onMediaSelectedForDeleted(
                                              listPdfDeleted);
                                          listPdfTemp
                                              .remove(listPdfDeleted[index]);
                                        });
                                        // ignore: use_build_context_synchronously
                                        SnackbarCustom.alertMessage(
                                          context,
                                          "Success Deleted File PDF",
                                          2,
                                          isSuccess,
                                        );
                                      }
                                      // ignore: use_build_context_synchronously
                                      Navigator.pop(context);
                                    },
                                    icon: const Icon(
                                      Icons.delete_outline,
                                      color: redColor,
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.pop(context);
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => PdfViewerPage(
                                          urlPdf: listPdfTemp[index],
                                          formCode: widget.formCode,
                                        ),
                                        fullscreenDialog: true,
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                },
                child: Container(
                  padding: const EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width / 5,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: listPdfTemp.length != 0
                          ? primaryColor.withOpacity(0.7)
                          : greyColor.withOpacity(0.5),
                      width: 2,
                    ),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(8),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.picture_as_pdf_outlined,
                        size: 35,
                        color: listPdfTemp.length != 0
                            ? primaryColor
                            : greyColor.withOpacity(0.5),
                      ),
                      Text(
                        "View",
                        style: regularPoppins.copyWith(
                          color: listPdfTemp.length != 0
                              ? primaryColor
                              : greyColor.withOpacity(0.5),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Visibility(
            visible: listPdf.isNotEmpty,
            child: InkWell(
              onTap: () {
                showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return Container(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: List.generate(
                          listPdf.length,
                          (index) => ListTile(
                            trailing: IconButton(
                              icon: const Icon(
                                Icons.delete_outline,
                                color: redColor,
                              ),
                              onPressed: () async {
                                listPdfDeleted = [];
                                listPdfDeleted.add(listPdf[index]);
                                var isSuccess = await deleteFilePdf();
                                if (isSuccess) {
                                  setState(() {
                                    widget.onMediaSelectedForDeleted(listPdf);
                                    listPdf.removeAt(index);
                                  });
                                  // ignore: use_build_context_synchronously
                                  SnackbarCustom.alertMessage(
                                    context,
                                    "Success Deleted File PDF",
                                    2,
                                    isSuccess,
                                  );
                                }
                                // ignore: use_build_context_synchronously
                                Navigator.pop(context);
                              },
                            ),
                            leading: const Icon(
                              Icons.label_important_outline_sharp,
                              color: secondaryColor,
                            ),
                            title: Text(
                              "Buka berkas PDF ke-${index + 1}",
                              style: regularPoppins,
                            ),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PdfViewerPage(
                                    urlPdf: listPdf[index],
                                    formCode: widget.formCode,
                                  ),
                                  fullscreenDialog: true,
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 14.0),
                    padding: const EdgeInsets.all(8),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: secondaryColor,
                      border: Border.all(
                        color: primaryColor.withOpacity(0.7),
                        width: 2,
                      ),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(8),
                      ),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "Preview PDF yang terpilih (${listPdf.length})",
                          style: regularPoppins.copyWith(
                            color: whiteColor,
                            fontSize: 14,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
