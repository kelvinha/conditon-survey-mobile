import 'package:flutter/material.dart';

import '../utils/theme.dart';

class ButtonCustom extends StatelessWidget {
  final Color? outlineBorderColor;
  final Color textColor;
  final Color backgroundColor;
  final String label;
  final VoidCallback onPressed;
  final double? width;
  final double? labelSize;
  final double? elevation;
  final double? height;

  const ButtonCustom({
    this.outlineBorderColor,
    required this.textColor,
    required this.backgroundColor,
    required this.label,
    required this.onPressed,
    this.width,
    this.labelSize,
    this.elevation,
    this.height,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        side: BorderSide(
          width: 1,
          color: outlineBorderColor ?? Colors.transparent,
        ),
        elevation: elevation ?? 8,
        backgroundColor: backgroundColor,
        fixedSize: Size(
          width ?? MediaQuery.of(context).size.width * 0.43,
          height ?? 40,
        ),
      ),
      onPressed: onPressed,
      child: Text(
        label,
        style: regularPoppins.copyWith(
          color: textColor,
          fontSize: labelSize,
        ),
      ),
    );
  }
}
