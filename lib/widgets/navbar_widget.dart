import 'package:condition_survey/providers/dashboard_provider.dart';
import 'package:condition_survey/widgets/status_mode_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/auth/users_model.dart';
import '../providers/auth_provider.dart';
import '../utils/constant.dart';
import '../utils/theme.dart';
import 'count_data_widget.dart';
import 'loading_indicator_widget.dart';

class Navbar extends StatelessWidget {
  const Navbar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    // auth user
    final AuthProvider authProvider = Provider.of<AuthProvider>(context);
    Users user = authProvider.user;
    // auth dashboard
    final DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context, listen: false);
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 230,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: greyColor.withOpacity(0.5),
                offset: const Offset(
                  5,
                  8,
                ),
                blurRadius: 2.0,
              )
            ],
            color: primaryColor,
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 60.0, 20.0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome,",
                        style: semiboldPoppins.copyWith(
                          fontSize: 26,
                          color: whiteColor,
                        ),
                      ),
                      Text(
                        user.name,
                        style: lightPoppins.copyWith(
                          color: whiteColor,
                        ),
                      ),
                    ],
                  ),
                  const Spacer(),
                  const CircleAvatar(
                    backgroundImage: AssetImage("${pathImages}avatar.png"),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              const Divider(
                color: secondaryColor,
                height: 10,
                thickness: 2,
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 10,
                ),
                child: FutureBuilder(
                  future: dashboardProvider.getSummary(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.60,
                            child: const LoadingIndicator(
                              color: secondaryColor,
                            ),
                          ),
                          const StatusMode(),
                        ],
                      );
                    } else {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CountData(
                            title: "In Progress",
                            total: dashboardProvider.summarySurvey.inProgress,
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          CountData(
                            title: "Done",
                            total: dashboardProvider.summarySurvey.done,
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          CountData(
                            title: "This Month",
                            total: dashboardProvider.summarySurvey.total,
                          ),
                          const SizedBox(
                            width: 40,
                          ),
                          const StatusMode(),
                        ],
                      );
                    }
                  },
                ),
              ),
              // const SizedBox(
              //   height: 13,
              // ),
            ],
          ),
        )
      ],
    );
  }
}
