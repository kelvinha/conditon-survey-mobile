class SummarySurvey {
  int todo;
  int inProgress;
  int done;
  int total;

  SummarySurvey({
    required this.todo,
    required this.inProgress,
    required this.done,
    required this.total,
  });

  factory SummarySurvey.fromJson(Map<String, dynamic> json) {
    return SummarySurvey(
      todo: json['todo'] ?? 0,
      inProgress: json['inprogress'] ?? 0,
      done: json['done'] ?? 0,
      total: json['total'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'todo': todo,
      'inProgress': inProgress,
      'done': done,
      'total': total,
    };
  }
}
