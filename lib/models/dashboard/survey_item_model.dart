class SurveyItem {
  int id;
  int surveyTitleId;
  String item;
  dynamic value; // Jika value bisa berupa tipe data lain selain null
  final String itemCode;

  DateTime createdAt;
  DateTime updatedAt;

  SurveyItem({
    required this.id,
    required this.surveyTitleId,
    required this.item,
    required this.value,
    required this.createdAt,
    required this.updatedAt,
    required this.itemCode,
  });

  factory SurveyItem.fromJson(Map<String, dynamic> json) {
    return SurveyItem(
        id: json['id'],
        surveyTitleId: json['survey_title_id'],
        item: json['item'],
        value: json['value'],
        createdAt: DateTime.parse(json['created_at']),
        updatedAt: DateTime.parse(json['updated_at']),
        itemCode: json['item_code']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'survey_title_id': surveyTitleId,
      'item': item,
      'value': value,
      'created_at': createdAt.toIso8601String(),
      'updated_at': updatedAt.toIso8601String(),
      'item_code': itemCode
    };
  }
}
