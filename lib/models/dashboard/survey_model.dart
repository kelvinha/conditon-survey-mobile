class ListSurvey {
  final int id;
  final int inspectorId;
  final String title;
  final String location;
  final String? date;
  final String inspectorName;
  final int status;
  final String createdAt;
  final String updatedAt;

  ListSurvey({
    required this.id,
    required this.inspectorId,
    required this.title,
    required this.location,
    required this.date,
    required this.inspectorName,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  factory ListSurvey.fromJson(Map<String, dynamic> json) {
    return ListSurvey(
      id: json['id'],
      inspectorId: json['inspector_id'],
      title: json['title'],
      location: json['location'],
      date: json['date'] ?? '',
      inspectorName: json['inspector_name'],
      status: json['status'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'inspector_id': inspectorId,
      'title': title,
      'location': location,
      'date': date,
      'inspector_name': inspectorName,
      'status': status,
      'created_at': createdAt,
      'updated_at': updatedAt,
    };
  }
}
