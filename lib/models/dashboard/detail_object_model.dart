class DetailObject {
  final String field;
  final String value;

  DetailObject({
    required this.field,
    required this.value,
  });

  factory DetailObject.fromJson(Map<String, dynamic> json) {
    return DetailObject(
      field: json['field'] ?? '',
      value: json['value'] ?? '',
    );
  }
}
