class FileAttachment {
  final String name;
  final String formCode;
  final String type;
  final String referenceId;
  final String path;
  final DateTime updatedAt;
  final DateTime createdAt;
  final int id;

  FileAttachment({
    required this.name,
    required this.formCode,
    required this.type,
    required this.referenceId,
    required this.path,
    required this.updatedAt,
    required this.createdAt,
    required this.id,
  });

  factory FileAttachment.fromJson(Map<String, dynamic> json) {
    return FileAttachment(
      name: json['name'] ?? '',
      formCode: json['form_code'] ?? '',
      type: json['type'] ?? '',
      referenceId: json['reference_id'] ?? '',
      path: json['path'] ?? '',
      updatedAt: DateTime.parse(json['updated_at'] ?? ''),
      createdAt: DateTime.parse(json['created_at'] ?? ''),
      id: json['id'] ?? 0,
    );
  }
}
