class SurveyCertificate {
  final int id;
  final int surveyTitleId;
  final int gradeCatL;
  final int gradeL;
  final String certificateName;
  final String? certificateNumber;
  final String? expiryDate;
  final String? finding;
  final String? remark;
  final List<String>? voiceNotePath;
  final List<String>? imagesPath;
  final List<String>? filePath;
  final String createdAt;
  final String updatedAt;

  SurveyCertificate({
    required this.id,
    required this.surveyTitleId,
    required this.gradeCatL,
    required this.gradeL,
    required this.certificateName,
    this.certificateNumber,
    this.expiryDate,
    this.finding,
    this.remark,
    this.voiceNotePath,
    this.imagesPath,
    this.filePath,
    required this.createdAt,
    required this.updatedAt,
  });

  factory SurveyCertificate.fromJson(Map<String, dynamic> json) {
    return SurveyCertificate(
      id: json['id'],
      surveyTitleId: json['survey_title_id'],
      gradeCatL: json['grade_cat_l'],
      gradeL: json['grade_l'],
      certificateName: json['certificate_name'],
      certificateNumber: json['certificate_number'],
      expiryDate: json['expiry_date'],
      finding: json['finding'],
      remark: json['remark'],
      voiceNotePath: json['voice_note_path'] != null
          ? (json['voice_note_path'] as List<dynamic>)
              .map((path) => path.toString())
              .toList()
          : [],
      imagesPath: json['images_path'] != null
          ? (json['images_path'] as List<dynamic>)
              .map((path) => path.toString())
              .toList()
          : [],
      filePath: json['file_path'] != null
          ? (json['file_path'] as List<dynamic>)
              .map((path) => path.toString())
              .toList()
          : [],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}
