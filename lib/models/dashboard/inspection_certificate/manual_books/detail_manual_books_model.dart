class DetailManualBooks {
  int id;
  int? parentId;
  String item;
  List<SubItem> subItems;

  DetailManualBooks({
    required this.id,
    this.parentId,
    required this.item,
    required this.subItems,
  });

  factory DetailManualBooks.fromJson(Map<String, dynamic> json) {
    List<SubItem> subItems = [];
    if (json['sub_items'] != null) {
      subItems =
          List<SubItem>.from(json['sub_items'].map((x) => SubItem.fromJson(x)));
    }
    return DetailManualBooks(
      id: json['id'],
      parentId: json['parent_id'],
      item: json['item'],
      subItems: subItems,
    );
  }
}

class SubItem {
  int id;
  int surveyTitleId;
  int parentId;
  String item;
  dynamic remark;
  String createdAt;
  String updatedAt;

  SubItem({
    required this.id,
    required this.surveyTitleId,
    required this.parentId,
    required this.item,
    this.remark,
    required this.createdAt,
    required this.updatedAt,
  });

  factory SubItem.fromJson(Map<String, dynamic> json) {
    return SubItem(
      id: json['id'],
      surveyTitleId: json['survey_title_id'],
      parentId: json['parent_id'],
      item: json['item'],
      remark: json['remark'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}
