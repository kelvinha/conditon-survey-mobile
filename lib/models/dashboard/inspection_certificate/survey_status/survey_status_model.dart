class SurveyStatus {
  final int id;
  final int surveyTitleId;
  final String certificateType;
  final String? lastDate;
  final String? next1Date;
  final String? next2Date;
  final DateTime createdAt;
  final DateTime updatedAt;

  SurveyStatus({
    required this.id,
    required this.surveyTitleId,
    required this.certificateType,
    this.lastDate,
    this.next1Date,
    this.next2Date,
    required this.createdAt,
    required this.updatedAt,
  });

  factory SurveyStatus.fromJson(Map<String, dynamic> json) {
    return SurveyStatus(
      id: json['id'] as int,
      surveyTitleId: json['survey_title_id'] as int,
      certificateType: json['certificate_type'] as String,
      lastDate: json['last_date'],
      next1Date: json['next_1_date'],
      next2Date: json['next_2_date'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['survey_title_id'] = surveyTitleId;
    data['certificate_type'] = certificateType;
    data['last_date'] = lastDate;
    data['next_1_date'] = next1Date;
    data['next_2_date'] = next2Date;
    data['created_at'] = createdAt.toIso8601String();
    data['updated_at'] = updatedAt.toIso8601String();
    return data;
  }
}
