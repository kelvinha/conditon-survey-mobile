class SurveyMenu {
  final int id;
  final int? parentMenuId;
  final String title;
  final String? desc;
  final bool isFormMenu;
  final String? formCode;
  final List<SurveyMenu> subMenu;

  SurveyMenu({
    required this.id,
    required this.parentMenuId,
    required this.title,
    required this.desc,
    required this.isFormMenu,
    required this.formCode,
    required this.subMenu,
  });

  factory SurveyMenu.fromJson(Map<String, dynamic> json) {
    return SurveyMenu(
      id: json['id'],
      parentMenuId: json['parent_menu_id'],
      title: json['title'],
      desc: json['desc'],
      isFormMenu: json['is_form_menu'],
      formCode: json['form_code'],
      subMenu: (json['sub_menu'] as List<dynamic>)
          .map((subMenuJson) => SurveyMenu.fromJson(subMenuJson))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    List<Map<String, dynamic>> subMenuJson =
        subMenu.map((subMenu) => subMenu.toJson()).toList();

    return {
      'id': id,
      'parent_menu_id': parentMenuId,
      'title': title,
      'desc': desc,
      'is_form_menu': isFormMenu,
      'form_code': formCode,
      'sub_menu': subMenuJson,
    };
  }
}
