class PipeValveSystem {
  final int id;
  final int surveyTitleId;
  final String item;
  final int gradeCatVp;
  final int gradeCatVv;
  final int gradeVp;
  final int gradeVv;
  String? finding;
  String? remark;
  List<String> voiceNotePath;
  List<String> imagesPath;
  List<String> filePath;
  DateTime createdAt;
  DateTime updatedAt;

  PipeValveSystem({
    required this.id,
    required this.surveyTitleId,
    required this.item,
    required this.gradeCatVp,
    required this.gradeCatVv,
    required this.gradeVp,
    required this.gradeVv,
    required this.finding,
    required this.remark,
    required this.voiceNotePath,
    required this.imagesPath,
    required this.filePath,
    required this.createdAt,
    required this.updatedAt,
  });

  factory PipeValveSystem.fromJson(Map<String, dynamic> json) {
    return PipeValveSystem(
      id: json['id'],
      surveyTitleId: json['survey_title_id'],
      item: json['item'],
      gradeCatVp: json['grade_cat_vp'],
      gradeCatVv: json['grade_cat_vv'],
      gradeVp: json['grade_vp'],
      gradeVv: json['grade_vv'],
      finding: json['finding'],
      remark: json['remark'],
      voiceNotePath: List<String>.from(json['voice_note_path'] ?? []),
      imagesPath: List<String>.from(json['images_path'] ?? []),
      filePath: List<String>.from(json['file_path'] ?? []),
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }
}
