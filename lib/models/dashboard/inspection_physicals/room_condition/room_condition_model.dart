class RoomCondition {
  final int id;
  final int? parentId;
  final String item;
  final List<SubItem> subItems;

  RoomCondition({
    required this.id,
    this.parentId,
    required this.item,
    required this.subItems,
  });

  factory RoomCondition.fromJson(Map<String, dynamic> json) {
    List<dynamic> subItemsList = json['sub_items'] ?? [];
    List<SubItem> subItems =
        subItemsList.map((e) => SubItem.fromJson(e)).toList();

    return RoomCondition(
      id: json['id'] as int,
      parentId: json['parent_id'] as int?,
      item: json['item'] as String,
      subItems: subItems,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['parent_id'] = parentId;
    data['item'] = item;
    data['sub_items'] = subItems.map((e) => e.toJson()).toList();
    return data;
  }
}

class SubItem {
  final int id;
  final int parentId;
  final int surveyTitleId;
  final String item;
  final int gradeCatL;
  final int gradeCatC;
  final int gradeCatFl;
  final int gradeCatFu;
  final int gradeL;
  final int gradeC;
  final int gradeFl;
  final int gradeFu;
  final String finding;
  final String remark;
  final List<String> voiceNotePath;
  final List<String> imagesPath;
  final List<String> filePath;
  final DateTime createdAt;
  final DateTime updatedAt;

  SubItem({
    required this.id,
    required this.parentId,
    required this.surveyTitleId,
    required this.item,
    required this.gradeCatL,
    required this.gradeCatC,
    required this.gradeCatFl,
    required this.gradeCatFu,
    required this.gradeL,
    required this.gradeC,
    required this.gradeFl,
    required this.gradeFu,
    required this.finding,
    required this.remark,
    required this.voiceNotePath,
    required this.imagesPath,
    required this.filePath,
    required this.createdAt,
    required this.updatedAt,
  });

  factory SubItem.fromJson(Map<String, dynamic> json) {
    return SubItem(
      id: json['id'] as int,
      parentId: json['parent_id'] as int,
      surveyTitleId: json['survey_title_id'] as int,
      item: json['item'] as String,
      gradeCatL: json['grade_cat_l'] as int,
      gradeCatC: json['grade_cat_c'] as int,
      gradeCatFl: json['grade_cat_fl'] as int,
      gradeCatFu: json['grade_cat_fu'] as int,
      gradeL: json['grade_l'] as int,
      gradeC: json['grade_c'] as int,
      gradeFl: json['grade_fl'] as int,
      gradeFu: json['grade_fu'] as int,
      finding: json['finding'] as String,
      remark: json['remark'] as String,
      voiceNotePath: List<String>.from(json['voice_note_path'] ?? []),
      imagesPath: List<String>.from(json['images_path'] ?? []),
      filePath: List<String>.from(json['file_path'] ?? []),
      createdAt: DateTime.parse(json['created_at'] as String),
      updatedAt: DateTime.parse(json['updated_at'] as String),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['parent_id'] = parentId;
    data['survey_title_id'] = surveyTitleId;
    data['item'] = item;
    data['grade_cat_l'] = gradeCatL;
    data['grade_cat_c'] = gradeCatC;
    data['grade_cat_fl'] = gradeCatFl;
    data['grade_cat_fu'] = gradeCatFu;
    data['grade_l'] = gradeL;
    data['grade_c'] = gradeC;
    data['grade_fl'] = gradeFl;
    data['grade_fu'] = gradeFu;
    data['finding'] = finding;
    data['remark'] = remark;
    data['voice_note_path'] = voiceNotePath;
    data['images_path'] = imagesPath;
    data['file_path'] = filePath;
    data['created_at'] = createdAt.toIso8601String();
    data['updated_at'] = updatedAt.toIso8601String();
    return data;
  }
}
