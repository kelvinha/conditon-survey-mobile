class HullConstruction {
  int id;
  int surveyTitleId;
  int? parentId;
  String item;
  int gradeCatC;
  int gradeCatV;
  int gradeCatM;
  int gradeC;
  int gradeV;
  int gradeM;
  String? finding;
  String? remark;
  List<String> voiceNotePath;
  List<String> imagesPath;
  List<String> filePath;
  DateTime createdAt;
  DateTime updatedAt;

  HullConstruction({
    required this.id,
    required this.surveyTitleId,
    this.parentId,
    required this.item,
    required this.gradeCatC,
    required this.gradeCatV,
    required this.gradeCatM,
    required this.gradeC,
    required this.gradeV,
    required this.gradeM,
    required this.finding,
    required this.remark,
    required this.voiceNotePath,
    required this.imagesPath,
    required this.filePath,
    required this.createdAt,
    required this.updatedAt,
  });

  factory HullConstruction.fromJson(Map<String, dynamic> json) {
    return HullConstruction(
      id: json['id'],
      surveyTitleId: json['survey_title_id'],
      parentId: json['parent_id'],
      item: json['item'],
      gradeCatC: json['grade_cat_c'],
      gradeCatV: json['grade_cat_v'],
      gradeCatM: json['grade_cat_m'],
      gradeC: json['grade_c'],
      gradeV: json['grade_v'],
      gradeM: json['grade_m'],
      finding: json['finding'],
      remark: json['remark'],
      voiceNotePath: List<String>.from(json['voice_note_path'] ?? []),
      imagesPath: List<String>.from(json['images_path'] ?? []),
      filePath: List<String>.from(json['file_path'] ?? []),
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'survey_title_id': surveyTitleId,
      'parent_id': parentId,
      'item': item,
      'grade_cat_c': gradeCatC,
      'grade_cat_v': gradeCatV,
      'grade_cat_m': gradeCatM,
      'grade_c': gradeC,
      'grade_v': gradeV,
      'grade_m': gradeM,
      'finding': finding,
      'remark': remark,
      'voice_note_path': voiceNotePath,
      'images_path': imagesPath,
      'file_path': filePath,
      'created_at': createdAt.toIso8601String(),
      'updated_at': updatedAt.toIso8601String(),
    };
  }
}
