class ShipMachinerySystem {
  int id;
  int surveyTitleId;
  int? parentId;
  String item;
  String itemDetail;
  int gradeCatV;
  int gradeCatF;
  int? gradeCatP;
  int? gradeCatM;
  int gradeV;
  int gradeF;
  int? gradeP;
  int? gradeM;
  String? finding;
  String? remark;
  List<String> voiceNotePath;
  List<String> imagesPath;
  List<String> filePath;
  DateTime createdAt;
  DateTime updatedAt;

  ShipMachinerySystem({
    required this.id,
    required this.surveyTitleId,
    this.parentId,
    required this.item,
    required this.itemDetail,
    required this.gradeCatV,
    required this.gradeCatF,
    this.gradeCatP,
    this.gradeCatM,
    required this.gradeV,
    required this.gradeF,
    this.gradeP,
    this.gradeM,
    required this.finding,
    required this.remark,
    required this.voiceNotePath,
    required this.imagesPath,
    required this.filePath,
    required this.createdAt,
    required this.updatedAt,
  });

  factory ShipMachinerySystem.fromJson(Map<String, dynamic> json) {
    return ShipMachinerySystem(
      id: json['id'],
      surveyTitleId: json['survey_title_id'],
      parentId: json['parent_id'],
      item: json['item'],
      itemDetail: json['item_detail'],
      gradeCatV: json['grade_cat_v'],
      gradeCatF: json['grade_cat_f'],
      gradeCatP: json['grade_cat_p'],
      gradeCatM: json['grade_cat_m'],
      gradeV: json['grade_v'],
      gradeF: json['grade_f'],
      gradeP: json['grade_p'],
      gradeM: json['grade_m'],
      finding: json['finding'],
      remark: json['remark'],
      voiceNotePath: List<String>.from(json['voice_note_path'] ?? []),
      imagesPath: List<String>.from(json['images_path'] ?? []),
      filePath: List<String>.from(json['file_path'] ?? []),
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'survey_title_id': surveyTitleId,
      'parent_id': parentId,
      'item': item,
      'item_detail': itemDetail,
      'grade_cat_v': gradeCatV,
      'grade_cat_f': gradeCatF,
      'grade_cat_p': gradeCatP,
      'grade_cat_m': gradeCatM,
      'grade_v': gradeV,
      'grade_f': gradeF,
      'grade_p': gradeP,
      'grade_m': gradeM,
      'finding': finding,
      'remark': remark,
      'voice_note_path': voiceNotePath,
      'images_path': imagesPath,
      'file_path': filePath,
      'created_at': createdAt.toIso8601String(),
      'updated_at': updatedAt.toIso8601String(),
    };
  }
}
