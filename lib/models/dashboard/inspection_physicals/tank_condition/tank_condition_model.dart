class TankCondition {
  int id;
  int surveyTitleId;
  String item;
  int gradeCatV;
  int gradeCatM;
  int gradeV;
  int gradeM;
  String? finding;
  String? remark;
  List<String> voiceNotePath;
  List<String> imagesPath;
  List<String> filePath;
  DateTime createdAt;
  DateTime updatedAt;

  TankCondition({
    required this.id,
    required this.surveyTitleId,
    required this.item,
    required this.gradeCatV,
    required this.gradeCatM,
    required this.gradeV,
    required this.gradeM,
    required this.finding,
    required this.remark,
    required this.voiceNotePath,
    required this.imagesPath,
    required this.filePath,
    required this.createdAt,
    required this.updatedAt,
  });

  factory TankCondition.fromJson(Map<String, dynamic> json) {
    return TankCondition(
      id: json['id'],
      surveyTitleId: json['survey_title_id'],
      item: json['item'],
      gradeCatV: json['grade_cat_v'],
      gradeCatM: json['grade_cat_m'],
      gradeV: json['grade_v'],
      gradeM: json['grade_m'],
      finding: json['finding'],
      remark: json['remark'],
      voiceNotePath: List<String>.from(json['voice_note_path'] ?? []),
      imagesPath: List<String>.from(json['images_path'] ?? []),
      filePath: List<String>.from(json['file_path'] ?? []),
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }
}
