class NavigationAndCommunication {
  final int id;
  final int? parentId;
  final String item;
  final List<SubItem> subItems;

  NavigationAndCommunication({
    required this.id,
    required this.parentId,
    required this.item,
    required this.subItems,
  });

  factory NavigationAndCommunication.fromJson(Map<String, dynamic> json) {
    var subItemsList = json['sub_items'] as List;
    List<SubItem> subItems =
        subItemsList.map((item) => SubItem.fromJson(item)).toList();

    return NavigationAndCommunication(
      id: json['id'],
      parentId: json['parent_id'],
      item: json['item'],
      subItems: subItems,
    );
  }
}

class SubItem {
  final int id;
  final int parentId;
  final int surveyTitleId;
  final String item;
  final int gradeCatV;
  final int gradeCatF;
  final int gradeCatM;
  final int gradeV;
  final int gradeF;
  final int gradeM;
  final String finding;
  final String remark;
  final List<String> voiceNotePath;
  final List<String> imagesPath;
  final List<String> filePath;
  final String createdAt;
  final String updatedAt;

  SubItem({
    required this.id,
    required this.parentId,
    required this.surveyTitleId,
    required this.item,
    required this.gradeCatV,
    required this.gradeCatF,
    required this.gradeCatM,
    required this.gradeV,
    required this.gradeF,
    required this.gradeM,
    required this.finding,
    required this.remark,
    required this.voiceNotePath,
    required this.imagesPath,
    required this.filePath,
    required this.createdAt,
    required this.updatedAt,
  });

  factory SubItem.fromJson(Map<String, dynamic> json) {
    return SubItem(
      id: json['id'],
      parentId: json['parent_id'],
      surveyTitleId: json['survey_title_id'],
      item: json['item'],
      gradeCatV: json['grade_cat_v'],
      gradeCatF: json['grade_cat_f'],
      gradeCatM: json['grade_cat_m'],
      gradeV: json['grade_v'],
      gradeF: json['grade_f'],
      gradeM: json['grade_m'],
      finding: json['finding'],
      remark: json['remark'],
      voiceNotePath: List<String>.from(json['voice_note_path'] ?? []),
      imagesPath: List<String>.from(json['images_path'] ?? []),
      filePath: List<String>.from(json['file_path'] ?? []),
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}
