class Grading {
  final int id;
  final int parentMenuId;
  final int categoryId;
  final int grade;
  final String description;
  final DateTime createdAt;
  final DateTime updatedAt;

  Grading({
    required this.id,
    required this.parentMenuId,
    required this.categoryId,
    required this.grade,
    required this.description,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Grading.fromJson(Map<String, dynamic> json) {
    return Grading(
      id: json['id'],
      parentMenuId: json['parent_menu_id'],
      categoryId: json['category_id'],
      grade: json['grade'],
      description: json['description'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }
}
