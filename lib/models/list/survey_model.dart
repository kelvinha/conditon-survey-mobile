class Survey {
  final String titleShip;
  final String dateRange;
  final String location;
  final int status;

  Survey({
    required this.titleShip,
    required this.dateRange,
    required this.location,
    required this.status,
  });

  factory Survey.fromJson(Map<String, dynamic> json) {
    return Survey(
      titleShip: json['title'],
      dateRange: json['date'],
      location: json['location'],
      status: json['status'],
    );
  }
}
