class Users {
  int id;
  String email;
  String name;
  String? token = '';

  Users({
    required this.id,
    required this.email,
    required this.name,
    required this.token,
  });

  factory Users.fromJson(Map<String, dynamic> json) {
    return Users(
      id: json['id'],
      email: json['email'],
      name: json['name'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'email': email,
      'name': name,
      'token': token,
    };
  }
}
